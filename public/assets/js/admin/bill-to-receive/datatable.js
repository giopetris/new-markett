var TableBillToReceive = function () {

    var initTable = function () {

        var table = $('#table-bill-to-receive');

        table.dataTable({

            "language": {
                url: url + '/assets/lib/datatable/i18n/Portuguese-Brasil.json'
            },

            "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.

            "bSort" : false,
            "lengthMenu": [
                [25, 50, 100],
                [25, 50, 100] // change per page values here
            ],
            // set the initial value
            "pageLength": 25,
            "pagingType": "bootstrap_full_number"
        });
    }

    return {

        init: function () {
            if ( ! jQuery().dataTable) {
                return;
            }

            initTable();
        }

    };

}();
