$(function() {
    $.reject({
        reject: { msie: true },
        imagePath: './assets/lib/jquery-jreject/images/',
        display: ['chrome'],
        header: 'Nosso sistema não é suportado pelo Internet Explorer',
        paragraph1: 'Por favor, instale algum navegador mais moderno.',
        paragraph2: 'Recomendamos instalar o Google Chrome. Clique no botão abaixo para fazer o download.',
        closeMessage: ''
    });
});