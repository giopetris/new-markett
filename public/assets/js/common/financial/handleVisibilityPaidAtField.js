$(function() {
    var transactionStatusField = $('#transaction_status_id');

    handleVisibility();

    transactionStatusField.change(function() {
        handleVisibility();
    });

    function handleVisibility() {
        var transactionStatusValue = transactionStatusField.val(),
            formGroupPaidAt = $('.form-group-paid-at');

        if (transactionStatusValue == transactionStatusPaid) {
            formGroupPaidAt.show();
            return;
        }

        formGroupPaidAt.hide();
    }
});
