$(function() {
    $('.mask-money').maskMoney({thousands:'.', decimal:',', affixesStay: true, allowNegative: true, allowZero: true, prefix: 'R$ '});

    $('.mask-decimal').maskMoney({thousands:'.', decimal:',', affixesStay: false});

    $('.mask-numeric').keydown(function(event) {
        // Allow special chars + arrows
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9
            || event.keyCode == 27 || event.keyCode == 13
            || (event.keyCode == 65 && event.ctrlKey === true)
            || (event.keyCode >= 35 && event.keyCode <= 39)){
            return;
        } else {
            // If it's not a number stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault();
            }
        }
    });

    $('.mask-cnpj').mask('99.999.999/9999-99');

    $('.mask-cpf').mask('999.999.999-99');

    $('.mask-zip-code').mask('99999-999');

    $('.mask-phone-number').mask('(99) 9999-9999?9');

    $('.mask-date').mask('99/99/9999');

    $('.mask-month-year').mask('99/9999');
});
