$(function() {
    var checkboxHasSponsor = $('#checkbox-has-sponsor'),
        inputSponsorId = $('#input-sponsor-id');

    checkboxHasSponsor.click(function() {
        if ( ! $(this).is(':checked')) {
            inputSponsorId.attr('readonly', false);
            inputSponsorId.val('');

            return;
        }

        inputSponsorId.attr('readonly', true);
        inputSponsorId.val(1);
    });
});
