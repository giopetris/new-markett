$(function() {
    var checkboxNotAccountHolder = $('#checkbox-not-account-holder');

    handleHolderFields();

    checkboxNotAccountHolder.change(function () {
        handleHolderFields();
    });

    function handleHolderFields() {
        var showAccountHolder = $('.show-account-holder');

        if (checkboxNotAccountHolder.is(':checked')) {
            showAccountHolder.show();

            return;
        }

        showAccountHolder.hide();
    }
});
