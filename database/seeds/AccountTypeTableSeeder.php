<?php

use Illuminate\Database\Seeder;
use NewMarkett\Repositories\Bank\AccountTypeRepository;
use NewMarkett\PickLists\Bank\AccountTypeNamePickList;

class AccountTypeTableSeeder extends Seeder
{
    /**
     * @var AccountTypeRepository
     */
    private $accountTypeRepository;

    /**
     * @param AccountTypeRepository $accountTypeRepository
     */
    public function __construct(AccountTypeRepository $accountTypeRepository)
    {
        $this->accountTypeRepository = $accountTypeRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $this->accountTypeRepository->deleteAll();

        $accountTypes = AccountTypeNamePickList::getData();

        foreach ($accountTypes as $id => $name) {
            $this->accountTypeRepository->create([
                'id' => $id,
                'name' => $name
            ]);
        }
    }
}
