<?php

use Illuminate\Database\Seeder;
use NewMarkett\PickLists\Classified\ClassifiedStatusNamePickList;
use NewMarkett\Repositories\Classified\ClassifiedRepository;
use NewMarkett\Repositories\Address\AddressRepository;

class ClassifiedTableSeeder extends Seeder
{
    /**
     * @var ClassifiedRepository
     */
    private $classifiedRepository;

    /**
     * @var AddressRepository
     */
    private $addressRepository;

    /**
     * @param ClassifiedRepository $classifiedRepository
     * @param AddressRepository $addressRepository
     */
    public function __construct(ClassifiedRepository $classifiedRepository, AddressRepository $addressRepository)
    {
        $this->classifiedRepository = $classifiedRepository;
        $this->addressRepository = $addressRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $this->classifiedRepository->deleteAll();

        $address = $this->addressRepository->create([
            'cep' => '89036240',
            'street' => 'Rua Doutor Artur Balsini',
            'number' => 107,
            'district' => 'Velha',
            'complement' => 'Sala 22',
            'city_id' => 8377
        ]);

        $this->classifiedRepository->create([
            'classified_status_id' => ClassifiedStatusNamePickList::ACTIVE,
            'logo_path' => '/uploads/app/user/3/',
            'file_name' => 'my_classified_logo.jpg',
            'title' => 'Evolumed',
            'phone' => '4732375763',
            'site' => 'http://www.evolumed.com.br/',
            'email' => 'contato.evolumed@gmail.com',
            'link_facebook' => 'https://www.facebook.com/evolumedconsultasmedicas',
            'address_id' => $address->id,
            'user_id' => 3
        ]);
    }
}
