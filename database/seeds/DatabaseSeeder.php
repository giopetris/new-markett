<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(RoleTableSeeder::class);
        $this->call(BankTableSeeder::class);
        $this->call(PlanTableSeeder::class);
        $this->call(AccountTypeTableSeeder::class);
        $this->call(TransactionScopeTableSeeder::class);
        $this->call(TransactionStatusTableSeeder::class);
        $this->call(TransactionTypeTableSeeder::class);
        $this->call(TransactionSubTypeTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(CidadesSeeder::class);
        $this->call(ClassifiedStatusTableSeeder::class);
        $this->call(ClassifiedTableSeeder::class);
        $this->call(PaymentFormTableSeeder::class);

        Model::reguard();
    }
}
