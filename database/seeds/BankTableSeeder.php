<?php

use Illuminate\Database\Seeder;
use NewMarkett\Repositories\Bank\BankRepository;
use NewMarkett\PickLists\Bank\BankNamePickList;

class BankTableSeeder extends Seeder
{
    /**
     * @var BankRepository
     */
    private $bankRepository;

    /**
     * @param BankRepository $bankRepository
     */
    public function __construct(BankRepository $bankRepository)
    {
        $this->bankRepository = $bankRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $this->bankRepository->deleteAll();

        $banks = BankNamePickList::getData();

        foreach ($banks as $id => $name) {
            $this->bankRepository->create([
                'id' => $id,
                'name' => $name
            ]);
        }
    }
}
