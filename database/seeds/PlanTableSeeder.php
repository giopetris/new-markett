<?php

use Illuminate\Database\Seeder;
use NewMarkett\PickLists\User\PlanNamePickList;
use NewMarkett\Repositories\User\PlanRepository;

class PlanTableSeeder extends Seeder
{
    /**
     * @var PlanRepository
     */
    private $planRepository;

    /**
     * @param PlanRepository $planRepository
     */
    public function __construct(PlanRepository $planRepository)
    {
        $this->planRepository = $planRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $this->planRepository->deleteAll();

        $this->planRepository->create([
            'id' => PlanNamePickList::BRONZE,
            'name' => PlanNamePickList::get(PlanNamePickList::BRONZE),
            'value' => 50,
            'shares_number' => 1,
            'percentage_comission_direct_indication' => 10,
            'percentage_comission_global_value' => 1,
        ]);

        $this->planRepository->create([
            'id' => PlanNamePickList::SILVER,
            'name' => PlanNamePickList::get(PlanNamePickList::SILVER),
            'value' => 250,
            'shares_number' => 5,
            'percentage_comission_direct_indication' => 10,
            'percentage_comission_global_value' => 4,
        ]);

        $this->planRepository->create([
            'id' => PlanNamePickList::GOLD,
            'name' => PlanNamePickList::get(PlanNamePickList::GOLD),
            'value' => 500,
            'shares_number' => 10,
            'percentage_comission_direct_indication' => 10,
            'percentage_comission_global_value' => 8,
        ]);

        $this->planRepository->create([
            'id' => PlanNamePickList::RUBY,
            'name' => PlanNamePickList::get(PlanNamePickList::RUBY),
            'value' => 750,
            'shares_number' => 15,
            'percentage_comission_direct_indication' => 10,
            'percentage_comission_global_value' => 12,
        ]);

        $this->planRepository->create([
            'id' => PlanNamePickList::DIAMOND,
            'name' => PlanNamePickList::get(PlanNamePickList::DIAMOND),
            'value' => 1200,
            'shares_number' => 20,
            'percentage_comission_direct_indication' => 10,
            'percentage_comission_global_value' => 21,
        ]);
    }
}
