<?php

use Illuminate\Database\Seeder;
use NewMarkett\PickLists\Classified\ClassifiedStatusNamePickList;
use NewMarkett\Repositories\Classified\ClassifiedStatusRepository;

class ClassifiedStatusTableSeeder extends Seeder
{
    /**
     * @var ClassifiedStatusRepository
     */
    private $classifiedStatusRepository;

    /**
     * @param ClassifiedStatusRepository $classifiedStatusRepository
     */
    public function __construct(ClassifiedStatusRepository $classifiedStatusRepository)
    {
        $this->classifiedStatusRepository = $classifiedStatusRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $this->classifiedStatusRepository->deleteAll();

        $classifiedsStatus = ClassifiedStatusNamePickList::getData();

        foreach ($classifiedsStatus as $id => $name) {
            $this->classifiedStatusRepository->create([
                'id' => $id,
                'name' => $name
            ]);
        }
    }
}
