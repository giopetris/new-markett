<?php

use Illuminate\Database\Seeder;
use NewMarkett\Repositories\Financial\TransactionScopeRepository;
use NewMarkett\PickLists\Financial\TransactionScopeNamePickList;

class TransactionScopeTableSeeder extends Seeder
{
    /**
     * @var TransactionScopeRepository
     */
    private $transactionScopeRepository;

    /**
     * @param TransactionScopeRepository $transactionScopeRepository
     */
    public function __construct(TransactionScopeRepository $transactionScopeRepository)
    {
        $this->transactionScopeRepository = $transactionScopeRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $this->transactionScopeRepository->deleteAll();

        $transactionScopes = TransactionScopeNamePickList::getData();

        foreach ($transactionScopes as $id => $name) {
            $this->transactionScopeRepository->create([
                'id' => $id,
                'name' => $name
            ]);
        }
    }
}
