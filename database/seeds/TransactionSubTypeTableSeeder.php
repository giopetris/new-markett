<?php

use Illuminate\Database\Seeder;
use NewMarkett\PickLists\Financial\TransactionSubTypeNamePickList;
use NewMarkett\Repositories\Financial\TransactionSubTypeRepository;
use NewMarkett\PickLists\Financial\TransactionTypeNamePickList;

class TransactionSubTypeTableSeeder extends Seeder
{
    /**
     * @var TransactionSubTypeRepository
     */
    private $transactionSubTypeRepository;

    /**
     * @param TransactionSubTypeRepository $transactionSubTypeRepository
     */
    public function __construct(TransactionSubTypeRepository $transactionSubTypeRepository)
    {
        $this->transactionSubTypeRepository = $transactionSubTypeRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $this->transactionSubTypeRepository->deleteAll();

        $this->transactionSubTypeRepository->create([
            'id' => TransactionSubTypeNamePickList::DIRECT_COMMISSION,
            'name' => TransactionSubTypeNamePickList::get(TransactionSubTypeNamePickList::DIRECT_COMMISSION),
            'transaction_type_id' => TransactionTypeNamePickList::INCOME
        ]);

        $this->transactionSubTypeRepository->create([
            'id' => TransactionSubTypeNamePickList::GLOBAL_SALES,
            'name' => TransactionSubTypeNamePickList::get(TransactionSubTypeNamePickList::GLOBAL_SALES),
            'transaction_type_id' => TransactionTypeNamePickList::INCOME
        ]);

        $this->transactionSubTypeRepository->create([
            'id' => TransactionSubTypeNamePickList::AFFILIATE_MEMBERSHIP,
            'name' => TransactionSubTypeNamePickList::get(TransactionSubTypeNamePickList::AFFILIATE_MEMBERSHIP),
            'transaction_type_id' => TransactionTypeNamePickList::INCOME
        ]);

        $this->transactionSubTypeRepository->create([
            'id' => TransactionSubTypeNamePickList::PLAN_RENOVATION,
            'name' => TransactionSubTypeNamePickList::get(TransactionSubTypeNamePickList::PLAN_RENOVATION),
            'transaction_type_id' => TransactionTypeNamePickList::INCOME
        ]);

        $this->transactionSubTypeRepository->create([
            'id' => TransactionSubTypeNamePickList::LOOT,
            'name' => TransactionSubTypeNamePickList::get(TransactionSubTypeNamePickList::LOOT),
            'transaction_type_id' => TransactionTypeNamePickList::SPENT
        ]);

        $this->transactionSubTypeRepository->create([
            'id' => TransactionSubTypeNamePickList::PLAN_CHANGE,
            'name' => TransactionSubTypeNamePickList::get(TransactionSubTypeNamePickList::PLAN_CHANGE),
            'transaction_type_id' => TransactionTypeNamePickList::INCOME
        ]);

        $this->transactionSubTypeRepository->create([
            'id' => TransactionSubTypeNamePickList::MONTHLY_FEE_COLLECT,
            'name' => TransactionSubTypeNamePickList::get(TransactionSubTypeNamePickList::MONTHLY_FEE_COLLECT),
            'transaction_type_id' => TransactionTypeNamePickList::INCOME
        ]);

        $this->transactionSubTypeRepository->create([
            'id' => TransactionSubTypeNamePickList::MONTHLY_FEE_PAYMENT,
            'name' => TransactionSubTypeNamePickList::get(TransactionSubTypeNamePickList::MONTHLY_FEE_PAYMENT),
            'transaction_type_id' => TransactionTypeNamePickList::SPENT
        ]);

        $this->transactionSubTypeRepository->create([
            'id' => TransactionSubTypeNamePickList::MONTHLY_FEE_COMMISSION,
            'name' => TransactionSubTypeNamePickList::get(TransactionSubTypeNamePickList::MONTHLY_FEE_COMMISSION),
            'transaction_type_id' => TransactionTypeNamePickList::INCOME
        ]);
    }
}
