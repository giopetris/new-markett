<?php

use Illuminate\Database\Seeder;
use NewMarkett\Repositories\User\UserRepository;
use NewMarkett\PickLists\User\RoleNamePickList;
use NewMarkett\PickLists\User\PlanNamePickList;

class UserTableSeeder extends Seeder
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $planExpirationDateToNewMarkettUsers = new \Carbon\Carbon();
        $planExpirationDateToNewMarkettUsers->addYears(100);

        $this->userRepository->create([
            'id' => 1,
            'role_id' => RoleNamePickList::ADMIN,
            'name' => 'Admin',
            'email' => 'admin@newmarkett.com.br',
            'password' => 'admin123'
        ]);

        $this->userRepository->create([
            'id' => 2,
            'role_id' => RoleNamePickList::ADMIN,
            'name' => 'Admin - Giovanni Petris',
            'email' => 'giopetris@gmail.com',
            'password' => 'gio#2015'
        ]);

        $this->userRepository->create([
            'id' => 3,
            'sponsor_id' => 1,
            'role_id' => RoleNamePickList::AFFILIATED,
            'name' => 'New Markett',
            'email' => 'adelino.moreira28@gmail.com',
            'password' => 'newmarkett#2015',
            'plan_id' => PlanNamePickList::BRONZE,
            'plan_expiration_date' => $planExpirationDateToNewMarkettUsers->toDateString()
        ]);

        $this->userRepository->create([
            'id' => 4,
            'sponsor_id' => 1,
            'role_id' => RoleNamePickList::AFFILIATED,
            'name' => 'New Markett',
            'email' => 'adelino.moreira28@gmail.com',
            'password' => 'newmarkett#2015',
            'plan_id' => PlanNamePickList::SILVER,
            'plan_expiration_date' => $planExpirationDateToNewMarkettUsers->toDateString()
        ]);

        $this->userRepository->create([
            'id' => 5,
            'sponsor_id' => 1,
            'role_id' => RoleNamePickList::AFFILIATED,
            'name' => 'New Markett',
            'email' => 'adelino.moreira28@gmail.com',
            'password' => 'newmarkett#2015',
            'plan_id' => PlanNamePickList::GOLD,
            'plan_expiration_date' => $planExpirationDateToNewMarkettUsers->toDateString()
        ]);

        $this->userRepository->create([
            'id' => 6,
            'sponsor_id' => 1,
            'role_id' => RoleNamePickList::AFFILIATED,
            'name' => 'New Markett',
            'email' => 'adelino.moreira28@gmail.com',
            'password' => 'newmarkett#2015',
            'plan_id' => PlanNamePickList::RUBY,
            'plan_expiration_date' => $planExpirationDateToNewMarkettUsers->toDateString()
        ]);

        $this->userRepository->create([
            'id' => 7,
            'sponsor_id' => 1,
            'role_id' => RoleNamePickList::AFFILIATED,
            'name' => 'New Markett',
            'email' => 'adelino.moreira28@gmail.com',
            'password' => 'newmarkett#2015',
            'plan_id' => PlanNamePickList::DIAMOND,
            'plan_expiration_date' => $planExpirationDateToNewMarkettUsers->toDateString()
        ]);

        $this->userRepository->create([
            'id' => 8,
            'sponsor_id' => 2,
            'role_id' => RoleNamePickList::AFFILIATED,
            'name' => 'Giovanni Petris',
            'email' => 'giopetris@gmail.com',
            'password' => 'gio123',
            'plan_id' => PlanNamePickList::BRONZE,
            'plan_expiration_date' => $planExpirationDateToNewMarkettUsers->toDateString()
        ]);
    }
}
