<?php

use Illuminate\Database\Seeder;
use NewMarkett\Repositories\User\RoleRepository;
use NewMarkett\PickLists\User\RoleNamePickList;

class RoleTableSeeder extends Seeder
{
    /**
     * @var RoleRepository
     */
    private $roleRepository;

    /**
     * @param RoleRepository $roleRepository
     */
    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $this->roleRepository->deleteAll();

        $this->roleRepository->create([
            'id' => RoleNamePickList::ADMIN,
            'name' => RoleNamePickList::get(RoleNamePickList::ADMIN)
        ]);

        $this->roleRepository->create([
            'id' => RoleNamePickList::AFFILIATED,
            'name' => RoleNamePickList::get(RoleNamePickList::AFFILIATED)
        ]);
    }
}
