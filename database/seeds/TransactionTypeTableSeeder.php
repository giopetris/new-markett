<?php

use Illuminate\Database\Seeder;
use NewMarkett\PickLists\Financial\TransactionTypeNamePickList;
use NewMarkett\Repositories\Financial\TransactionTypeRepository;

class TransactionTypeTableSeeder extends Seeder
{
    /**
     * @var TransactionTypeRepository
     */
    private $transactionTypeRepository;

    /**
     * @param TransactionTypeRepository $transactionTypeRepository
     */
    public function __construct(TransactionTypeRepository $transactionTypeRepository)
    {
        $this->transactionTypeRepository = $transactionTypeRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $this->transactionTypeRepository->deleteAll();

        $transactionTypes = TransactionTypeNamePickList::getData();

        foreach ($transactionTypes as $id => $name) {
            $this->transactionTypeRepository->create([
                'id' => $id,
                'name' => $name
            ]);
        }
    }
}
