<?php

use Illuminate\Database\Seeder;
use NewMarkett\Repositories\Financial\TransactionStatusRepository;
use NewMarkett\PickLists\Financial\TransactionStatusNamePickList;

class TransactionStatusTableSeeder extends Seeder
{
    /**
     * @var TransactionStatusRepository
     */
    private $transactionStatusRepository;

    /**
     * @param TransactionStatusRepository $transactionStatusRepository
     */
    public function __construct(TransactionStatusRepository $transactionStatusRepository)
    {
        $this->transactionStatusRepository = $transactionStatusRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $this->transactionStatusRepository->deleteAll();

        $transactionStatus = TransactionStatusNamePickList::getData();

        foreach ($transactionStatus as $id => $name) {
            $this->transactionStatusRepository->create([
                'id' => $id,
                'name' => $name
            ]);
        }
    }
}
