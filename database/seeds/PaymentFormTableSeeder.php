<?php

use Illuminate\Database\Seeder;
use NewMarkett\Repositories\Financial\PaymentFormRepository;
use NewMarkett\PickLists\Financial\PaymentFormNamePickList;

class PaymentFormTableSeeder extends Seeder
{
    /**
     * @var PaymentFormRepository
     */
    private $paymentFormRepository;

    /**
     * @param PaymentFormRepository $paymentFormRepository
     */
    public function __construct(PaymentFormRepository $paymentFormRepository)
    {
        $this->paymentFormRepository = $paymentFormRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $this->paymentFormRepository->deleteAll();

        $paymentForms = PaymentFormNamePickList::getData();

        foreach ($paymentForms as $id => $name) {
            $this->paymentFormRepository->create([
                'id' => $id,
                'name' => $name
            ]);
        }
    }
}
