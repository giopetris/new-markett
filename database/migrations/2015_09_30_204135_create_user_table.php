<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('role_id')->unsigned();
            $table->string('name');
            $table->string('email');
            $table->string('password', 60);
            $table->char('cpf', 11);
            $table->char('phone', 11);
            $table->char('cellular', 11);
            $table->integer('address_id')->unsigned()->nullable();
            $table->integer('sponsor_id')->unsigned()->nullable();
            $table->integer('plan_id')->unsigned()->nullable();
            $table->date('plan_expiration_date');

            $table->string('token_password_reset')->nullable();

            $table->foreign('address_id')
                ->references('id')->on('address')
                ->onDelete('cascade');
            $table->foreign('sponsor_id')
                ->references('id')->on('user')
                ->onDelete('cascade');
            $table->foreign('plan_id')
                ->references('id')->on('plan')
                ->onDelete('cascade');

            $table->rememberToken();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user');
    }
}
