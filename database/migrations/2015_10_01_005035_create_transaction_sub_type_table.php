<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionSubTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_sub_type', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('transaction_type_id')->unsigned();

            $table->foreign('transaction_type_id')
                ->references('id')->on('transaction_type')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transaction_sub_type');
    }
}
