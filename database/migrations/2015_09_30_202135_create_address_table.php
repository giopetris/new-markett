<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('address', function (Blueprint $table) {
            $table->increments('id');

            $table->char('cep', 8)->nullable();
            $table->string('street')->nullable();
            $table->integer('number')->nullable()->default(null);
            $table->string('district')->nullable();
            $table->string('complement')->nullable();
            $table->integer('city_id')->unsigned()->nullable()->default(null);

            $table->foreign('city_id')
                ->references('id')->on('cidades')
                ->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('address');
    }
}
