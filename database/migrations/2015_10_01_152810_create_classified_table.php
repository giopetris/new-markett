<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassifiedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classified', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('classified_status_id')->unsigned();
            $table->string('logo_path');
            $table->string('file_name');
            $table->string('title');
            $table->char('phone', 11);
            $table->char('cellular', 11)->nullable();
            $table->string('site')->nullable();
            $table->string('email')->nullable();
            $table->string('link_facebook')->nullable();
            $table->integer('address_id')->unsigned();
            $table->integer('user_id')->unsigned();

            $table->foreign('classified_status_id')
                ->references('id')->on('classified_status')
                ->onDelete('cascade');
            $table->foreign('address_id')
                ->references('id')->on('address')
                ->onDelete('cascade');
            $table->foreign('user_id')
                ->references('id')->on('user')
                ->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('classified');
    }
}
