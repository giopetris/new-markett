<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGlobalSaleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('global_sale', function (Blueprint $table) {
            $table->increments('id');

            $table->date('date');
            $table->double('total_sale');

            $table->integer('number_users_with_bronze_plan')->nullable();
            $table->double('total_value_to_users_with_bronze_plan')->nullable();

            $table->integer('number_users_with_silver_plan')->nullable();
            $table->double('total_value_to_users_with_silver_plan')->nullable();

            $table->integer('number_users_with_gold_plan')->nullable();
            $table->double('total_value_to_users_with_gold_plan')->nullable();

            $table->integer('number_users_with_ruby_plan')->nullable();
            $table->double('total_value_to_users_with_ruby_plan')->nullable();

            $table->integer('number_users_with_diamond_plan')->nullable();
            $table->double('total_value_to_users_with_diamond_plan')->nullable();

            $table->dateTime('divided_at')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('global_sale');
    }
}
