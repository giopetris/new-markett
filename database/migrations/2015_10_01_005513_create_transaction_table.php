<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->unsigned();
            $table->integer('transaction_subtype_id')->unsigned();
            $table->integer('transaction_status_id')->unsigned();
            $table->integer('transaction_scope_id')->unsigned();
            $table->string('description');
            $table->float('value');
            $table->date('paid_at')->nullable();
            $table->integer('track_status_transaction_id')->unsigned()->nullable();
            $table->integer('payment_form_id')->unsigned()->nullable();

            $table->foreign('transaction_subtype_id')
                ->references('id')->on('transaction_sub_type')
                ->onDelete('cascade');
            $table->foreign('user_id')
                ->references('id')->on('user')
                ->onDelete('cascade');
            $table->foreign('transaction_status_id')
                ->references('id')->on('transaction_status')
                ->onDelete('cascade');
            $table->foreign('transaction_scope_id')
                ->references('id')->on('transaction_scope')
                ->onDelete('cascade');
            $table->foreign('track_status_transaction_id')
                ->references('id')->on('transaction')
                ->onDelete('cascade');
            $table->foreign('payment_form_id')
                ->references('id')->on('payment_form')
                ->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transaction');
    }
}
