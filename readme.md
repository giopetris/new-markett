# Running the app

1. Run `composer install`

2. Copy `.env.example` to `.env` configuring the variables according with your environment. 
For development purposes just change database settings  

3. Run `php artisan key:generate`

4. Create a database in your server `CREATE DATABASE new_markett`

5. Run `php artisan migrate:refresh --seed`

6. Run `php artisan serve`

7. Enjoy the application (:

# Access data

1. To access affiliated area:
URL: http://app.new-markett.localhost:8000/login
ID: 8
Password: gio123

2. To access admin area:
URL: http://admin.new-markett.localhost:8000/login
ID: 1
Password: admin123