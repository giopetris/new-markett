<?php

namespace NewMarkett\ValueObjects\Financial;

class MonthlyFee
{
    const VALUE = 60;
    const COMMISSION_LEVEL_1 = 5;
    const COMMISSION_LEVEL_2 = 3;
    const COMMISSION_LEVEL_3 = 1;
    const TOTAL_LEVELS = 3;
}