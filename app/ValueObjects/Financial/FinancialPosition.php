<?php

namespace NewMarkett\ValueObjects\Financial;

use NewMarkett\Entities\User\User;
use NewMarkett\Presenters\ValueObject\Financial\FinancialPositionPresenter;
use Laracasts\Presenter\PresentableTrait;

class FinancialPosition
{
    use PresentableTrait;

    protected $presenter = FinancialPositionPresenter::class;

    /**
     * @var float
     */
    private $totalEntriesPaid;

    /**
     * @var float
     */
    private $totalOutputsPaid;

    /**
     * @var float
     */
    private $totalEntriesPaidOrWaitingPayment;

    /**
     * @var float
     */
    private $totalOutputsPaidOrWaitingPayment;

    /**
     * @var User
     */
    private $user;

    /**
     * @param float $totalEntriesPaid
     * @param float $totalOutputsPaid
     * @param float $totalEntriesPaidOrWaitingPayment
     * @param float $totalOutputsPaidOrWaitingPayment
     * @param User $user
     */
    public function __construct(
        $totalEntriesPaid,
        $totalOutputsPaid,
        $totalEntriesPaidOrWaitingPayment,
        $totalOutputsPaidOrWaitingPayment,
        User $user
    ) {
        $this->totalEntriesPaid = $totalEntriesPaid;
        $this->totalOutputsPaid = $totalOutputsPaid;
        $this->totalEntriesPaidOrWaitingPayment = $totalEntriesPaidOrWaitingPayment;
        $this->totalOutputsPaidOrWaitingPayment = $totalOutputsPaidOrWaitingPayment;
        $this->user = $user;
    }

    /**
     * @return float
     */
    public function getTotalEntriesPaid()
    {
        return $this->totalEntriesPaid;
    }

    /**
     * @return float
     */
    public function getTotalOutputsPaid()
    {
        return $this->totalOutputsPaid;
    }

    /**
     * @return float
     */
    public function getTotalEntriesPaidOrWaitingPayment()
    {
        return $this->totalEntriesPaidOrWaitingPayment;
    }

    /**
     * @return float
     */
    public function getTotalOutputsPaidOrWaitingPayment()
    {
        return $this->totalOutputsPaidOrWaitingPayment;
    }

    /**
     * @return float
     */
    public function getTotalBalanceTransactionPaid()
    {
        return $this->totalEntriesPaid - $this->totalOutputsPaid;
    }

    /**
     * @return float
     */
    public function getTotalBalanceTransactionPaidOrWaitingPayment()
    {
        return $this->totalEntriesPaid - $this->totalOutputsPaidOrWaitingPayment;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }
}
