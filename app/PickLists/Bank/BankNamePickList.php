<?php

namespace NewMarkett\PickLists\Bank;

use NewMarkett\PickLists\Base\PickList;

class BankNamePickList extends PickList
{
    const BANCO_BANERJ_SA = 1;
    const BANCO_BANESTADO_SA = 2;
    const BANCO_BEG_SA = 3;
    const BANCO_BRADESCO_SA = 4;
    const BANCO_CITIBANK_SA = 5;
    const BANCO_AMAZONIA_SA = 6;
    const BANCO_BRASIL_SA = 7;
    const BANCO_ESTADO_SANTA_CATARINA_SA = 8;
    const BANCO_ESTADO_PARA_SA = 9;
    const BANCO_ESTADO_RIO_GRANDE_SUL_SA = 10;
    const BANCO_NORDESTE_BRASIL_SA = 11;
    const BANCO_ITAU = 12;
    const BANCO_COOPERATIVO_SICREDI = 13;
    const BANCO_SAFRA_SA = 14;
    const BANCO_SANTANDER_SA = 15;
    const BANESTES_SA_BANCO_ESTADO_ESP = 16;
    const BANCO_BRASILIA_BRB = 17;
    const CAIXA_ECONOMICA_FEDERAL = 18;
    const HSBC_BANK_BRASIL_SA = 19;

    protected static $data = [
        self::BANCO_BANERJ_SA => 'Banco Banerj S.A.',
        self::BANCO_BANESTADO_SA => 'Banco Banestado S.A.',
        self::BANCO_BEG_SA => 'Banco Beg S.A.',
        self::BANCO_BRADESCO_SA => 'Banco Bradesco S.A.',
        self::BANCO_CITIBANK_SA => 'Banco Citibank S.A.',
        self::BANCO_AMAZONIA_SA => 'Banco da Amazônia S.A.',
        self::BANCO_BRASIL_SA => 'Banco do Brasil S.A.',
        self::BANCO_ESTADO_SANTA_CATARINA_SA => 'Banco do Estado de Santa Catarina S.A.',
        self::BANCO_ESTADO_PARA_SA => 'Banco do Estado do Pará',
        self::BANCO_ESTADO_RIO_GRANDE_SUL_SA => 'Banco do Estado do Rio Grande do Sul S.A.',
        self::BANCO_NORDESTE_BRASIL_SA => 'Banco do Nordeste do Brasil S.A.',
        self::BANCO_ITAU => 'Banco Itaú',
        self::BANCO_COOPERATIVO_SICREDI => 'Banco Cooperativo Sicredi',
        self::BANCO_SAFRA_SA => 'Banco Safra S.A.',
        self::BANCO_SANTANDER_SA => 'Banco Santander S.A.',
        self::BANESTES_SA_BANCO_ESTADO_ESP => 'BANESTES S.A. Banco do Estado do Esp',
        self::BANCO_BRASILIA_BRB => 'Banco de Brasília - BRB',
        self::CAIXA_ECONOMICA_FEDERAL => 'Caixa Econômica Federal',
        self::HSBC_BANK_BRASIL_SA => 'HSBC Bank Brasil S.A.',
    ];
}
