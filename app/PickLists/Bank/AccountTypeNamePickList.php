<?php

namespace NewMarkett\PickLists\Bank;

use NewMarkett\PickLists\Base\PickList;

class AccountTypeNamePickList extends PickList
{
    const CHECKING_ACCOUNT = 1;
    const SAVINGS_ACCOUNT = 2;

    protected static $data = [
        self::CHECKING_ACCOUNT => 'Conta corrente',
        self::SAVINGS_ACCOUNT => 'Conta poupança',
    ];
}
