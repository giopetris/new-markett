<?php

namespace NewMarkett\PickLists\Base;

abstract class PickList
{
    /**
     * @var array
     */
    protected static $data;

    /**
     * @param $offset
     * @return bool
     */
    public static function exists($offset)
    {
        return array_key_exists($offset, static::$data);
    }

    /**
     * @param $offset
     * @return null|string
     */
    public static function get($offset)
    {
        if (is_null($offset) || ! self::exists($offset)) {
            return null;
        }

        return static::$data[$offset];
    }

    /**
     * @return array
     */
    public static function getData()
    {
        return static::$data;
    }
}
