<?php

namespace NewMarkett\PickLists\Financial;

use NewMarkett\PickLists\Base\PickList;

class PaymentFormNamePickList extends PickList
{
    // Quando a New Markett fica responsável pela forma de pagamento, pode ser depósito, boleto, etc
    const NEGOTIATED_WITH_NEW_MARKETT = 1;
    const DEDUCTED_FROM_THE_BALANCE = 2;

    protected static $data = [
        self::NEGOTIATED_WITH_NEW_MARKETT => 'Negociado com a New Markett',
        self::DEDUCTED_FROM_THE_BALANCE => 'Abatido do saldo',
    ];
}
