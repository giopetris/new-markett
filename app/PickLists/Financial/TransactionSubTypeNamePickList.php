<?php

namespace NewMarkett\PickLists\Financial;

use NewMarkett\PickLists\Base\PickList;

class TransactionSubTypeNamePickList extends PickList
{
    const DIRECT_COMMISSION = 1;
    const GLOBAL_SALES = 2;
    const AFFILIATE_MEMBERSHIP = 3;
    const PLAN_RENOVATION = 4;
    const LOOT = 5;
    const PLAN_CHANGE = 6;
    const MONTHLY_FEE_COLLECT = 7;
    const MONTHLY_FEE_PAYMENT = 8;
    const MONTHLY_FEE_COMMISSION = 9;

    protected static $data = [
        self::DIRECT_COMMISSION => 'Comissão direta',
        self::GLOBAL_SALES => 'Faturamento global',
        self::AFFILIATE_MEMBERSHIP => 'Adesão de afiliado',
        self::PLAN_RENOVATION => 'Renovação de plano',
        self::LOOT => 'Saque',
        self::PLAN_CHANGE => 'Mudança de plano',
        self::MONTHLY_FEE_COLLECT => 'Cobrança da taxa mensal',
        self::MONTHLY_FEE_PAYMENT => 'Pagamento da taxa mensal',
        self::MONTHLY_FEE_COMMISSION => 'Comissão da taxa mensal'
    ];
}
