<?php

namespace NewMarkett\PickLists\Financial;

use NewMarkett\PickLists\Base\PickList;

class BankDataToDepositPickList extends PickList
{
    const BANK = 'BANK';
    const AGENCY = 'AGENCY';
    const ACCOUNT_NUMBER = 'ACCOUNT_NUMBER';
    const OPERATION = 'OPERATION';
    const ACCOUNT_TYPE = 'ACCOUNT_TYPE';
    const PAYEE_NAME = 'PAYEE_NAME';
    const PAYEE_CPF = 'PAYEE_CPF';

    protected static $data = [
        self::BANK => 'Caixa Econômica Federal',
        self::AGENCY => '3555',
        self::ACCOUNT_NUMBER => '7118-6',
        self::OPERATION => '013',
        self::ACCOUNT_TYPE => 'Conta poupança',
        self::PAYEE_NAME => 'Adelino Moreira da Silva',
        self::PAYEE_CPF => '837.761.849-49',
    ];
}
