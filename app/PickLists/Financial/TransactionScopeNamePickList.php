<?php

namespace NewMarkett\PickLists\Financial;

use NewMarkett\PickLists\Base\PickList;

class TransactionScopeNamePickList extends PickList
{
    // The global scope is about the transactions that occurs in the financial box of the system
    const SCOPE_GLOBAL = 1;
    // The individual scope is about the transactions that occurs in the financial box of the affiliates
    const SCOPE_INDIVIDUAL = 2;

    protected static $data = [
        self::SCOPE_GLOBAL => 'Global',
        self::SCOPE_INDIVIDUAL => 'Individual',
    ];
}
