<?php

namespace NewMarkett\PickLists\Financial;

use NewMarkett\PickLists\Base\PickList;

class TransactionStatusNamePickList extends PickList
{
    const WAITING_PAYMENT = 1;
    const PAID = 2;
    const CANCELED = 3;

    protected static $data = [
        self::WAITING_PAYMENT => 'Aguardando pagamento',
        self::PAID => 'Pago',
        self::CANCELED => 'Cancelado',
    ];
}
