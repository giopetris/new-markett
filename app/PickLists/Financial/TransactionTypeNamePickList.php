<?php

namespace NewMarkett\PickLists\Financial;

use NewMarkett\PickLists\Base\PickList;

class TransactionTypeNamePickList extends PickList
{
    const INCOME = 1;
    const SPENT = 2;

    protected static $data = [
        self::INCOME => 'Entrada',
        self::SPENT => 'Saída',
    ];
}
