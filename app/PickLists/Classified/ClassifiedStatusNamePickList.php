<?php

namespace NewMarkett\PickLists\Classified;

use NewMarkett\PickLists\Base\PickList;

class ClassifiedStatusNamePickList extends PickList
{
    const WAITING_APPROVAL = 1;
    const ACTIVE = 2;
    const DISAPPROVED = 3;

    protected static $data = [
        self::WAITING_APPROVAL => 'Aguardando aprovação',
        self::ACTIVE => 'Ativo',
        self::DISAPPROVED => 'Reprovado',
    ];
}
