<?php

namespace NewMarkett\PickLists\User;

use NewMarkett\PickLists\Base\PickList;

class PlanNamePickList extends PickList
{
    const BRONZE = 1;
    const SILVER = 2;
    const GOLD = 3;
    const RUBY = 4;
    const DIAMOND = 5;

    protected static $data = [
        self::BRONZE => 'Bronze',
        self::SILVER => 'Prata',
        self::GOLD => 'Ouro',
        self::RUBY => 'Rubi',
        self::DIAMOND => 'Diamante',
    ];

    protected static $dataWithOriginalName = [
        self::BRONZE => 'Bronze',
        self::SILVER => 'Silver',
        self::GOLD => 'Gold',
        self::RUBY => 'Ruby',
        self::DIAMOND => 'Diamond',
    ];

    public static function getDataWithOriginalNames()
    {
        return self::$dataWithOriginalName;
    }

    /**
     * @param $offset
     * @return bool
     */
    public static function existsOriginalName($offset)
    {
        return array_key_exists($offset, static::$dataWithOriginalName);
    }

    /**
     * @param $offset
     * @return null|string
     */
    public static function getOriginalName($offset)
    {
        if (is_null($offset) || ! self::existsOriginalName($offset)) {
            return null;
        }

        return static::$dataWithOriginalName[$offset];
    }
}
