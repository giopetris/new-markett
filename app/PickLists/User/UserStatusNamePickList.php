<?php

namespace NewMarkett\PickLists\User;

use NewMarkett\PickLists\Base\PickList;

class UserStatusNamePickList extends PickList
{
    const WAITING_ACCEPTANCE_INVITATION = 1;
    const WAITING_PAYMENT_MEMBERSHIP = 2;
    const ACTIVE = 3;
    const WAITING_PAYMENT_RENEWAL_PLAN = 4;
    const INACTIVE = 5;
    const OVERDUE_MONTHLY_FEE = 6;

    protected static $data = [
        self::WAITING_ACCEPTANCE_INVITATION => 'Aguardando aceitação do convite',
        self::WAITING_PAYMENT_MEMBERSHIP => 'Aguardando pagamento da adesão',
        self::ACTIVE => 'Ativo',
        self::WAITING_PAYMENT_RENEWAL_PLAN => 'Aguardando pagamento da renovação do plano',
        self::INACTIVE => 'Inativo',
        self::OVERDUE_MONTHLY_FEE => 'Inadimplente com a taxa mensal',
    ];
}
