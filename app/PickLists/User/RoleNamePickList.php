<?php

namespace NewMarkett\PickLists\User;

use NewMarkett\PickLists\Base\PickList;

class RoleNamePickList extends PickList
{
    const ADMIN = 1;
    const AFFILIATED = 2;

    protected static $data = [
        self::ADMIN => 'Admin',
        self::AFFILIATED => 'Afiliado',
    ];
}
