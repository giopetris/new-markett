<?php

namespace NewMarkett\Specifications\Financial;

use NewMarkett\Entities\Financial\Transaction;
use NewMarkett\Entities\User\User;
use NewMarkett\PickLists\Financial\TransactionScopeNamePickList;
use NewMarkett\PickLists\Financial\TransactionSubTypeNamePickList;
use NewMarkett\Services\Financial\FinancialPosition\GetFinancialPositionService;
use NewMarkett\ValueObjects\Financial\FinancialPosition;
use NewMarkett\ValueObjects\Financial\MonthlyFee;

class UserHasBalanceToDeductMonthlyFeeSpecification
{
    /**
     * @var GetFinancialPositionService
     */
    private $getFinancialPositionService;

    /**
     * @param GetFinancialPositionService $getFinancialPositionService
     */
    public function __construct(GetFinancialPositionService $getFinancialPositionService)
    {
        $this->getFinancialPositionService = $getFinancialPositionService;
    }

    public function isSatisfiedBy(User $user)
    {
        /** @var FinancialPosition $financialPosition */
        $financialPosition = $this->getFinancialPositionService->ofUser($user);

        return $financialPosition->getTotalBalanceTransactionPaidOrWaitingPayment() >= MonthlyFee::VALUE;
    }
}
