<?php

namespace NewMarkett\Specifications\Financial;

use NewMarkett\Entities\Financial\Transaction;
use NewMarkett\PickLists\Financial\TransactionScopeNamePickList;
use NewMarkett\PickLists\Financial\TransactionSubTypeNamePickList;

class TransactionIsPaymentPlanSpecification
{
    public function isSatisfiedBy(Transaction $transaction)
    {
        if (
            $transaction->transaction_scope_id == TransactionScopeNamePickList::SCOPE_GLOBAL &&
            in_array(
                $transaction->transaction_subtype_id,
                [TransactionSubTypeNamePickList::AFFILIATE_MEMBERSHIP, TransactionSubTypeNamePickList::PLAN_RENOVATION]
            )
        ) {
            return true;
        }

        return false;
    }
}
