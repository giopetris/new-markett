<?php

namespace NewMarkett\Specifications\User;

use NewMarkett\Entities\User\User;
use NewMarkett\PickLists\User\UserStatusNamePickList;
use NewMarkett\Services\User\GetStatusService;

class UserIsInPreRegisterSpecification
{
    /**
     * @var GetStatusService
     */
    private $getStatusService;

    /**
     * @param GetStatusService $getStatusService
     */
    public function __construct(GetStatusService $getStatusService)
    {
        $this->getStatusService = $getStatusService;
    }

    public function isSatisfiedBy(User $user)
    {
        return $this->getStatusService->getStatusIdOf($user) == UserStatusNamePickList::WAITING_ACCEPTANCE_INVITATION;
    }
}
