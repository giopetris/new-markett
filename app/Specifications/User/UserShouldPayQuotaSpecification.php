<?php

namespace NewMarkett\Specifications\User;

use NewMarkett\Entities\User\User;

class UserShouldPayQuotaSpecification
{
    /**
     * @param User $user
     * @return bool
     */
    public function isSatisfiedBy(User $user)
    {
        return ! $user->planAccessionIsPaid() ||
                $user->planIsExpired() ||
                $user->isInactive();
    }
}
