<?php

namespace NewMarkett\Specifications\User;

use NewMarkett\Entities\User\User;

class UserIsFullRegistrationSpecification
{
    /**
     * @param User $user
     * @return bool
     */
    public function isSatisfiedBy(User $user)
    {
        if ( ! $user->name || ! $user->cpf || ! $user->email || ! $user->cellular) {
            return false;
        }

        $address = $user->address;
        if ( ! $address) {
            return false;
        }

        if ( ! $address->cep || ! $address->street || ! $address->number || ! $address->district || ! $address->city) {
            return false;
        }

        return true;
    }
}
