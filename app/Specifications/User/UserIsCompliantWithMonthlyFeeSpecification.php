<?php

namespace NewMarkett\Specifications\User;

use Carbon\Carbon;
use NewMarkett\Entities\Financial\Transaction;
use NewMarkett\Entities\User\User;
use NewMarkett\Repositories\Financial\TransactionRepository;

class UserIsCompliantWithMonthlyFeeSpecification
{
    /**
     * @var TransactionRepository
     */
    private $transactionRepository;

    /**
     * @param TransactionRepository $transactionRepository
     */
    public function __construct(TransactionRepository $transactionRepository)
    {
        $this->transactionRepository = $transactionRepository;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function isSatisfiedBy(User $user)
    {
        /** @var Transaction $transaction */
        $transaction = $this->transactionRepository->getLastMonthlyFeePaymentOf($user);

        if ( ! $transaction || ! $transaction->isWaitingPayment()) {
            return true;
        }

        $now = Carbon::now();

        return $now->diffInDays($transaction->created_at) < 10;
    }
}