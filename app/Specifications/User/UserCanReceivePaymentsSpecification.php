<?php

namespace NewMarkett\Specifications\User;

use NewMarkett\Entities\User\User;
use NewMarkett\PickLists\User\UserStatusNamePickList;
use NewMarkett\Services\User\GetStatusService;

class UserCanReceivePaymentsSpecification
{
    /**
     * @var GetStatusService
     */
    private $getStatusService;

    /**
     * @param GetStatusService $getStatusService
     */
    public function __construct(GetStatusService $getStatusService)
    {
        $this->getStatusService = $getStatusService;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function isSatisfiedBy(User $user)
    {
        return $this->getStatusService->getStatusIdOf($user) == UserStatusNamePickList::ACTIVE && ! $user->isAdmin();
    }
}
