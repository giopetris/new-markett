<?php

namespace NewMarkett\Entities\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laracasts\Presenter\PresentableTrait;
use NewMarkett\Presenters\User\PlanPresenter;

class Plan extends Model
{
    use PresentableTrait, SoftDeletes;

    protected $presenter = PlanPresenter::class;

    protected $table = 'plan';

    protected $fillable = [
        'name',
        'value',
    ];

    protected $dates = ['deleted_at'];
}
