<?php

namespace NewMarkett\Entities\User;

use Carbon\Carbon;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Laracasts\Presenter\PresentableTrait;
use NewMarkett\Entities\Address\Address;
use NewMarkett\Entities\Bank\BankData;
use NewMarkett\PickLists\User\RoleNamePickList;
use NewMarkett\Presenters\User\UserPresenter;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, PresentableTrait, SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $presenter = UserPresenter::class;

    protected $table = 'user';

    protected $fillable = [
        'name',
        'email',
        'password',
        'cpf',
        'phone',
        'cellular',
        'plan_expiration_date',
        'sponsor_id',
        'plan_id',
        'token_password_reset',
        'is_inactive'
    ];

    /**
     * @param $pass
     */
    public function setPasswordAttribute($pass)
    {
        $this->attributes['password'] = \Hash::make($pass);
    }

    /**
     * @param Role $role
     * @return bool
     */
    public function hasRole(Role $role)
    {
        return $this->role_id == $role->id;
    }

    public function sponsor()
    {
        return $this->belongsTo(User::class, 'sponsor_id');
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    public function plan()
    {
        return $this->belongsTo(Plan::class);
    }

    public function bankData()
    {
        return $this->hasOne(BankData::class);
    }

    /**
     * @return bool
     */
    public function sponsorIsAdmin()
    {
        return $this->sponsor->role_id == RoleNamePickList::ADMIN;
    }

    /**
     * @return bool
     */
    public function isAdmin()
    {
        return $this->role_id == RoleNamePickList::ADMIN;
    }

    /**
     * @return bool
     */
    public function planAccessionIsPaid()
    {
        return $this->plan_expiration_date != '0000-00-00';
    }

    /**
     * @return bool
     */
    public function planIsExpired()
    {
        if ( ! $this->planAccessionIsPaid()) {
            return false;
        }

        $planExpirationDate = Carbon::createFromFormat('Y-m-d', $this->plan_expiration_date);

        return $planExpirationDate->isPast();
    }

    /**
     * @return bool
     */
    public function isInactive()
    {
        return $this->is_inactive;
    }
}
