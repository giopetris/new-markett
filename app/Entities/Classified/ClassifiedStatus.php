<?php

namespace NewMarkett\Entities\Classified;

use Illuminate\Database\Eloquent\Model;

class ClassifiedStatus extends Model
{
    protected $table = 'classified_status';

    public $timestamps = false;

    protected $fillable = [
        'name',
    ];
}
