<?php

namespace NewMarkett\Entities\Classified;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laracasts\Presenter\PresentableTrait;
use NewMarkett\Entities\Address\Address;
use NewMarkett\Entities\User\User;
use NewMarkett\Presenters\Classified\ClassifiedPresenter;

class Classified extends Model
{
    use PresentableTrait, SoftDeletes;

    protected $presenter = ClassifiedPresenter::class;

    protected $table = 'classified';

    protected $fillable = [
        'classified_status_id',
        'logo_path',
        'file_name',
        'title',
        'phone',
        'cellular',
        'site',
        'email',
        'link_facebook',
        'address_id',
        'user_id'
    ];

    protected $dates = ['deleted_at'];

    public function classifiedStatus()
    {
        return $this->belongsTo(ClassifiedStatus::class);
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    public function getFullLogoPath()
    {
        return $this->logo_path . $this->file_name;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
