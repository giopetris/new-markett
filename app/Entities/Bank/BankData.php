<?php

namespace NewMarkett\Entities\Bank;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use NewMarkett\Entities\User\User;

class BankData extends Model
{
    use SoftDeletes;

    protected $table = 'bank_data';

    protected $fillable = [
        'bank_id',
        'user_id',
        'account_type_id',
        'agency',
        'account_number',
        'not_account_holder',
        'holder_name',
        'holder_cpf',
    ];

    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function bank()
    {
        return $this->belongsTo(Bank::class);
    }

    public function accountType()
    {
        return $this->belongsTo(AccountType::class);
    }
}
