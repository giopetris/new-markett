<?php

namespace NewMarkett\Entities\Address;

use Artesaos\Cidade;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laracasts\Presenter\PresentableTrait;
use NewMarkett\Presenters\Address\AddressPresenter;

class Address extends Model
{
    use PresentableTrait, SoftDeletes;

    protected $presenter = AddressPresenter::class;

    protected $table = 'address';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'cep',
        'street',
        'number',
        'district',
        'complement',
        'city_id',
    ];

    public function city()
    {
        return $this->belongsTo(Cidade::class);
    }
}
