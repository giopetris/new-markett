<?php

namespace NewMarkett\Entities\Financial;

use Illuminate\Database\Eloquent\Model;

class TransactionStatus extends Model
{
    protected $table = 'transaction_status';

    public $timestamps = false;

    protected $fillable = [
        'name',
    ];
}
