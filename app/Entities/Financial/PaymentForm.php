<?php

namespace NewMarkett\Entities\Financial;

use Illuminate\Database\Eloquent\Model;

class PaymentForm extends Model
{
    protected $table = 'payment_form';

    public $timestamps = false;

    protected $fillable = [
        'name',
    ];
}
