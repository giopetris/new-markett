<?php

namespace NewMarkett\Entities\Financial;

use Illuminate\Database\Eloquent\Model;

class TransactionScope extends Model
{
    protected $table = 'transaction_scope';

    public $timestamps = false;

    protected $fillable = [
        'name',
    ];
}
