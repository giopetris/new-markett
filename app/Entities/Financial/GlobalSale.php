<?php

namespace NewMarkett\Entities\Financial;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GlobalSale extends Model
{
    use SoftDeletes;

    protected $table = 'global_sale';

    protected $fillable = [
        'date',
        'total_sale',
        'number_users_with_bronze_plan',
        'total_value_to_users_with_bronze_plan',
        'number_users_with_silver_plan',
        'total_value_to_users_with_silver_plan',
        'number_users_with_gold_plan',
        'total_value_to_users_with_gold_plan',
        'number_users_with_ruby_plan',
        'total_value_to_users_with_ruby_plan',
        'number_users_with_diamond_plan',
        'total_value_to_users_with_diamond_plan',
        'divided_at',
    ];

    protected $dates = ['divided_at', 'date', 'deleted_at'];

    /**
     * @return bool
     */
    public function isDivided()
    {
        return $this->divided_at != null;
    }
}
