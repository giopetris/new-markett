<?php

namespace NewMarkett\Entities\Financial;

use Illuminate\Database\Eloquent\Model;

class TransactionType extends Model
{
    protected $table = 'transaction_type';

    public $timestamps = false;

    protected $fillable = [
        'name',
    ];
}
