<?php

namespace NewMarkett\Entities\Financial;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laracasts\Presenter\PresentableTrait;
use NewMarkett\Entities\User\User;
use NewMarkett\Events\Financial\TransactionStatusWasChanged;
use NewMarkett\PickLists\Financial\TransactionStatusNamePickList;
use NewMarkett\Presenters\Financial\TransactionPresenter;

class Transaction extends Model
{
    use PresentableTrait, SoftDeletes;

    protected $presenter = TransactionPresenter::class;

    protected $table = 'transaction';

    protected $fillable = [
        'user_id',
        'transaction_subtype_id',
        'transaction_status_id',
        'transaction_scope_id',
        'description',
        'value',
        'paid_at',
        'track_status_transaction_id',
        'payment_form_id'
    ];

    protected $dates = ['paid_at', 'deleted_at'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function transactionStatus()
    {
        return $this->belongsTo(TransactionStatus::class);
    }

    public function transactionSubtype()
    {
        return $this->belongsTo(TransactionSubType::class);
    }

    /**
     * @return bool
     */
    public function isPaid()
    {
        return $this->paid_at != null;
    }

    /**
     * @return bool
     */
    public function isWaitingPayment()
    {
        return $this->transaction_status_id == TransactionStatusNamePickList::WAITING_PAYMENT;
    }

    /**
     * @return bool
     */
    public function isCanceled()
    {
        return $this->transaction_status_id == TransactionStatusNamePickList::CANCELED;
    }

    /**
     * @param $transactionStatusId
     */
    public function setTransactionStatusIdAttribute($transactionStatusId)
    {
        $oldTransactionStatusId = $this->transaction_status_id;
        $this->attributes['transaction_status_id'] = $transactionStatusId;

        if ($oldTransactionStatusId != $transactionStatusId) {
            \Event::fire(new TransactionStatusWasChanged($this));
        }
    }
}
