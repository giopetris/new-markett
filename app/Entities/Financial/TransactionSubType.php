<?php

namespace NewMarkett\Entities\Financial;

use Illuminate\Database\Eloquent\Model;

class TransactionSubType extends Model
{
    protected $table = 'transaction_sub_type';

    public $timestamps = false;

    protected $fillable = [
        'name',
    ];

    public function transactionType()
    {
        return $this->belongsTo(TransactionType::class);
    }
}
