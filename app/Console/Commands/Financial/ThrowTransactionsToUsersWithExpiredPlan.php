<?php

namespace NewMarkett\Console\Commands\Financial;

use Illuminate\Console\Command;
use NewMarkett\Services\Financial\Transaction\ThrowTransactionsToUsersWithExpiredPlanService;

class ThrowTransactionsToUsersWithExpiredPlan extends Command
{
    /**
     * @var ThrowTransactionsToUsersWithExpiredPlanService
     */
    private $throwTransactionsToUsersWithExpiredPlanService;

    /**
     * @param ThrowTransactionsToUsersWithExpiredPlanService $throwTransactionsToUsersWithExpiredPlanService
     */
    public function __construct(ThrowTransactionsToUsersWithExpiredPlanService $throwTransactionsToUsersWithExpiredPlanService)
    {
        parent::__construct();

        $this->throwTransactionsToUsersWithExpiredPlanService = $throwTransactionsToUsersWithExpiredPlanService;
    }

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'throw-transactions-to-users-with-expired-plan';

    public function handle()
    {
        $this->throwTransactionsToUsersWithExpiredPlanService->throwTransactions();
    }
}
