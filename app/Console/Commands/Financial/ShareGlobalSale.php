<?php

namespace NewMarkett\Console\Commands\Financial;

use Illuminate\Console\Command;
use NewMarkett\Services\Financial\GlobalSale\ShareService as GlobalSaleShareService;

class ShareGlobalSale extends Command
{
    /**
     * @var GlobalSaleShareService
     */
    private $globalSaleShareService;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'share-global-sale';

    /**
     * @param GlobalSaleShareService $globalSaleShareService
     */
    public function __construct(GlobalSaleShareService $globalSaleShareService)
    {
        parent::__construct();

        $this->globalSaleShareService = $globalSaleShareService;
    }

    public function handle()
    {
        $this->globalSaleShareService->share();
    }
}
