<?php

namespace NewMarkett\Console\Commands\Financial;

use Illuminate\Console\Command;
use NewMarkett\Services\Financial\MonthlyFee\CollectService;

class CollectMonthlyFee extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'collect-monthly-fee';

    /**
     * @var CollectService
     */
    private $collectService;

    /**
     * @param CollectService $collectService
     */
    public function __construct(CollectService $collectService)
    {
        parent::__construct();

        $this->collectService = $collectService;
    }

    public function handle()
    {
        $this->collectService->collect();
    }
}