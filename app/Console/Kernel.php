<?php

namespace NewMarkett\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use NewMarkett\Console\Commands\Financial\ShareGlobalSale;
use NewMarkett\Console\Commands\Financial\ThrowTransactionsToUsersWithExpiredPlan;
use NewMarkett\Console\Commands\Financial\CollectMonthlyFee;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \NewMarkett\Console\Commands\Inspire::class,
        ShareGlobalSale::class,
        ThrowTransactionsToUsersWithExpiredPlan::class,
        CollectMonthlyFee::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('inspire')
                 ->hourly();

        $schedule->command('share-global-sale')
            ->daily();
        $schedule->command('throw-transactions-to-users-with-expired-plan')
            ->daily();
        $schedule->command('collect-monthly-fee')
            ->monthly();
    }
}
