<?php

namespace NewMarkett\Events\Financial;

use NewMarkett\Entities\Financial\Transaction;
use NewMarkett\Events\Event;

class TransactionStatusWasChanged extends Event
{
    /**
     * @var Transaction
     */
    private $transaction;

    /**
     * @param Transaction $transaction
     */
    public function __construct(Transaction $transaction)
    {
        $this->transaction = $transaction;
    }

    /**
     * @return Transaction
     */
    public function getTransaction()
    {
        return $this->transaction;
    }
}
