<?php

namespace NewMarkett\Services\User;

use Carbon\Carbon;
use NewMarkett\Entities\User\User;
use NewMarkett\PickLists\User\UserStatusNamePickList;
use NewMarkett\Repositories\Financial\TransactionRepository;
use NewMarkett\Specifications\User\UserIsCompliantWithMonthlyFeeSpecification;

class GetStatusService
{
    /**
     * @var UserIsCompliantWithMonthlyFeeSpecification
     */
    private $userIsCompliantWithMonthlyFeeSpecification;

    /**
     * @param UserIsCompliantWithMonthlyFeeSpecification $userIsCompliantWithMonthlyFeeSpecification
     */
    public function __construct(UserIsCompliantWithMonthlyFeeSpecification $userIsCompliantWithMonthlyFeeSpecification)
    {
        $this->userIsCompliantWithMonthlyFeeSpecification = $userIsCompliantWithMonthlyFeeSpecification;
    }

    /**
     * @param User $user
     * @return null|string
     */
    public function getStatusNameOf(User $user)
    {
        return UserStatusNamePickList::get($this->getStatusIdOf($user));
    }

    /**
     * @param User $user
     * @return int
     */
    public function getStatusIdOf(User $user)
    {
        if ($user->isInactive()) {
            return UserStatusNamePickList::INACTIVE;
        }

        if ( ! $user->password) {
            return UserStatusNamePickList::WAITING_ACCEPTANCE_INVITATION;
        }

        $planExpirationDate = $user->plan_expiration_date;

        if ($planExpirationDate == '0000-00-00') {
            return UserStatusNamePickList::WAITING_PAYMENT_MEMBERSHIP;
        }

        $date = Carbon::createFromFormat('Y-m-d', $planExpirationDate);

        if ($date->isPast()) {
            return UserStatusNamePickList::WAITING_PAYMENT_RENEWAL_PLAN;
        }

        if ( ! $this->userIsCompliantWithMonthlyFeeSpecification->isSatisfiedBy($user)) {
            return UserStatusNamePickList::OVERDUE_MONTHLY_FEE;
        }

        return UserStatusNamePickList::ACTIVE;
    }

}