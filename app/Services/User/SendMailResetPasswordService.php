<?php

namespace NewMarkett\Services\User;

use NewMarkett\Entities\User\User;

class SendMailResetPasswordService
{
    public function to(User $user)
    {
        $resetPasswordUrl = route('app.auth.reset-password', ['tokenPasswordReset' => $user->token_password_reset]);

        \Mail::send(
            'mail.app.auth.reset-password',
            ['user' => $user, 'resetPasswordUrl' => $resetPasswordUrl],
            function($m) use ($user) {
                $m
                    ->to($user->email, $user->name)
                    ->subject('New Markett - Mudar senha');
            }
        );
    }
}
