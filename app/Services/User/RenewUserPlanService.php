<?php

namespace NewMarkett\Services\User;

use Carbon\Carbon;
use NewMarkett\Entities\User\User;
use NewMarkett\Repositories\User\UserRepository;

class RenewUserPlanService
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function to(User $user)
    {
        $planExpirationDate = new Carbon();
        $planExpirationDate->addYear(1);

        $this->userRepository->update(['plan_expiration_date' => $planExpirationDate], $user->id);
    }
}
