<?php

namespace NewMarkett\Services\Financial\Calculator;

use NewMarkett\Entities\User\Plan;

class GlobalSaleCalculator
{
    /**
     * @param $totalSale
     * @param Plan $plan
     * @param $numberUsersBelongingToPlan
     * @return float
     */
    public function calculateTotalValueToUser($totalSale, Plan $plan, $numberUsersBelongingToPlan)
    {
        if ($numberUsersBelongingToPlan == 0 || $totalSale == 0) {
            return 0;
        }

        $valueToShare = $totalSale * ($plan->percentage_comission_global_value / 100);

        $valueToRound = $valueToShare / $numberUsersBelongingToPlan;

        return floor($valueToRound * 100) / 100;
    }
}
