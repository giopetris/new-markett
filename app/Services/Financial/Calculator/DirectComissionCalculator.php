<?php

namespace NewMarkett\Services\Financial\Calculator;

use NewMarkett\Entities\User\Plan;

class DirectComissionCalculator
{
    public function calculeToPlan(Plan $plan)
    {
        return $plan->value * 0.1;
    }
}
