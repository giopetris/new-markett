<?php

namespace NewMarkett\Services\Financial\MonthlyFee;

use NewMarkett\Entities\User\User;
use NewMarkett\Repositories\User\UserRepository;
use NewMarkett\Services\Financial\Transaction\ThrowTransactionToMonthlyFeeService;

class CollectService
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var ThrowTransactionToMonthlyFeeService
     */
    private $throwTransactionToMonthlyFeeService;

    /**
     * @param UserRepository $userRepository
     * @param ThrowTransactionToMonthlyFeeService $throwTransactionToMonthlyFeeService
     */
    public function __construct(UserRepository $userRepository, ThrowTransactionToMonthlyFeeService $throwTransactionToMonthlyFeeService)
    {
        $this->userRepository = $userRepository;
        $this->throwTransactionToMonthlyFeeService = $throwTransactionToMonthlyFeeService;
    }

    public function collect()
    {
        $users = $this->userRepository->findActiveAffiliates();

        /** @var User $user */
        foreach ($users as $user) {
            $this->throwTransactionToMonthlyFeeService->to($user);
        }
    }
}