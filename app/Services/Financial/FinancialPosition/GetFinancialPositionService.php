<?php

namespace NewMarkett\Services\Financial\FinancialPosition;

use NewMarkett\Entities\User\User;
use NewMarkett\PickLists\Financial\PaymentFormNamePickList;
use NewMarkett\PickLists\Financial\TransactionScopeNamePickList;
use NewMarkett\PickLists\Financial\TransactionStatusNamePickList;
use NewMarkett\PickLists\Financial\TransactionTypeNamePickList;
use NewMarkett\Repositories\Financial\TransactionRepository;
use NewMarkett\Repositories\Financial\TransactionScopeRepository;
use NewMarkett\Repositories\Financial\TransactionTypeRepository;
use NewMarkett\ValueObjects\Financial\FinancialPosition;

class GetFinancialPositionService
{
    /**
     * @var TransactionRepository
     */
    private $transactionRepository;

    /**
     * @var TransactionTypeRepository
     */
    private $transactionTypeRepository;

    /**
     * @var TransactionScopeRepository
     */
    private $transactionScopeRepository;

    /**
     * @param TransactionRepository $transactionRepository
     * @param TransactionTypeRepository $transactionTypeRepository
     * @param TransactionScopeRepository $transactionScopeRepository
     */
    public function __construct(
        TransactionRepository $transactionRepository,
        TransactionTypeRepository $transactionTypeRepository,
        TransactionScopeRepository $transactionScopeRepository
    ) {
        $this->transactionRepository = $transactionRepository;
        $this->transactionTypeRepository = $transactionTypeRepository;
        $this->transactionScopeRepository = $transactionScopeRepository;
    }

    /**
     * @param User $user
     * @return FinancialPosition
     */
    public function ofUser(User $user)
    {
        $transactionTypeIncome = $this->transactionTypeRepository->find(TransactionTypeNamePickList::INCOME);
        $transactionTypeSpent = $this->transactionTypeRepository->find(TransactionTypeNamePickList::SPENT);
        $transactionScope = $this->transactionScopeRepository->find(TransactionScopeNamePickList::SCOPE_INDIVIDUAL);

        $totalValueEntriesPaid = $this->transactionRepository->getTotalValueByTransactionTypeTransactionStatusUserAndScope(
            $transactionTypeIncome,
            [TransactionStatusNamePickList::PAID],
            $user,
            $transactionScope
        );
        $totalValueOutputsPaid = $this->transactionRepository->getTotalValueByTransactionTypeTransactionStatusUserAndScope(
            $transactionTypeSpent,
            [TransactionStatusNamePickList::PAID],
            $user,
            $transactionScope,
            [PaymentFormNamePickList::DEDUCTED_FROM_THE_BALANCE, null]
        );

        $totalValueEntriesPaidOrWaitingPayment = $this->transactionRepository->getTotalValueByTransactionTypeTransactionStatusUserAndScope(
            $transactionTypeIncome,
            [TransactionStatusNamePickList::PAID, TransactionStatusNamePickList::WAITING_PAYMENT],
            $user,
            $transactionScope
        );
        $totalValueOutputsPaidOrWaitingPayment = $this->transactionRepository->getTotalValueByTransactionTypeTransactionStatusUserAndScope(
            $transactionTypeSpent,
            [TransactionStatusNamePickList::PAID, TransactionStatusNamePickList::WAITING_PAYMENT],
            $user,
            $transactionScope,
            [PaymentFormNamePickList::DEDUCTED_FROM_THE_BALANCE, null]
        );

        return new FinancialPosition(
            $totalValueEntriesPaid,
            $totalValueOutputsPaid,
            $totalValueEntriesPaidOrWaitingPayment,
            $totalValueOutputsPaidOrWaitingPayment,
            $user
        );
    }
}
