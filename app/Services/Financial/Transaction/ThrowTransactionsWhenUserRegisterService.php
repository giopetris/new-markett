<?php

namespace NewMarkett\Services\Financial\Transaction;

use NewMarkett\Entities\User\User;
use NewMarkett\PickLists\Financial\TransactionScopeNamePickList;
use NewMarkett\PickLists\Financial\TransactionStatusNamePickList;
use NewMarkett\PickLists\Financial\TransactionSubTypeNamePickList;
use NewMarkett\Repositories\Financial\TransactionRepository;
use NewMarkett\Specifications\User\UserCanReceivePaymentsSpecification;
use NewMarkett\Services\Financial\Calculator\DirectComissionCalculator;

class ThrowTransactionsWhenUserRegisterService
{
    /**
     * @var TransactionRepository
     */
    private $transactionRepository;

    /**
     * @var DirectComissionCalculator
     */
    private $directComissionCalculator;

    /**
     * @var UserCanReceivePaymentsSpecification
     */
    private $userCanReceivePaymentsSpecification;

    /**
     * @param TransactionRepository $transactionRepository
     * @param DirectComissionCalculator $directComissionCalculator
     * @param UserCanReceivePaymentsSpecification $userCanReceivePaymentsSpecification
     */
    public function __construct(
        TransactionRepository $transactionRepository,
        DirectComissionCalculator $directComissionCalculator,
        UserCanReceivePaymentsSpecification $userCanReceivePaymentsSpecification
    ) {
        $this->transactionRepository = $transactionRepository;
        $this->directComissionCalculator = $directComissionCalculator;
        $this->userCanReceivePaymentsSpecification = $userCanReceivePaymentsSpecification;
    }

    public function to(User $user)
    {
        $plan = $user->plan;
        $sponsor = $user->sponsor;

        $affiliateMembershipTransaction = $this->transactionRepository->create([
            'user_id' => $user->id,
            'transaction_subtype_id' => TransactionSubTypeNamePickList::AFFILIATE_MEMBERSHIP,
            'transaction_status_id' => TransactionStatusNamePickList::WAITING_PAYMENT,
            'transaction_scope_id' => TransactionScopeNamePickList::SCOPE_GLOBAL,
            'description' => 'Adesão de afiliado',
            'value' => $plan->value
        ]);

        if ($this->userCanReceivePaymentsSpecification->isSatisfiedBy($sponsor)) {
            $this->transactionRepository->create([
                'user_id' => $sponsor->id,
                'transaction_subtype_id' => TransactionSubTypeNamePickList::DIRECT_COMMISSION,
                'transaction_status_id' => TransactionStatusNamePickList::WAITING_PAYMENT,
                'transaction_scope_id' => TransactionScopeNamePickList::SCOPE_INDIVIDUAL,
                'description' => 'Comissão direta sobre adesão do afiliado ' . $user->name . ' (ID: ' . $user->id . ')',
                'value' => $this->directComissionCalculator->calculeToPlan($plan),
                'track_status_transaction_id' => $affiliateMembershipTransaction->id
            ]);
        }
    }
}
