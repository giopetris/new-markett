<?php

namespace NewMarkett\Services\Financial\Transaction;

use NewMarkett\Entities\Financial\Transaction;
use NewMarkett\Repositories\Financial\TransactionRepository;
use NewMarkett\Repositories\User\UserRepository;
use NewMarkett\Services\User\RenewUserPlanService;
use NewMarkett\Specifications\Financial\TransactionIsPaymentPlanSpecification;
use NewMarkett\Services\Financial\GlobalSale\AggregatePaymentService as GlobalSaleAggregatePaymentService;

class UpdateBillToReceiveService
{
    /**
     * @var TransactionRepository
     */
    private $transactionRepository;

    /**
     * @var TransactionIsPaymentPlanSpecification
     */
    private $transactionIsPaymentPlanSpecification;

    /**
     * @var RenewUserPlanService
     */
    private $renewUserPlanService;

    /**
     * @var GlobalSaleAggregatePaymentService
     */
    private $globalSaleAggregatePaymentService;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @param TransactionRepository $transactionRepository
     * @param TransactionIsPaymentPlanSpecification $transactionIsPaymentPlanSpecification
     * @param RenewUserPlanService $renewUserPlanService
     * @param GlobalSaleAggregatePaymentService $globalSaleAggregatePaymentService
     * @param UserRepository $userRepository
     */
    public function __construct(
        TransactionRepository $transactionRepository,
        TransactionIsPaymentPlanSpecification $transactionIsPaymentPlanSpecification,
        RenewUserPlanService $renewUserPlanService,
        GlobalSaleAggregatePaymentService $globalSaleAggregatePaymentService,
        UserRepository $userRepository
    ) {
        $this->transactionRepository = $transactionRepository;
        $this->transactionIsPaymentPlanSpecification = $transactionIsPaymentPlanSpecification;
        $this->renewUserPlanService = $renewUserPlanService;
        $this->globalSaleAggregatePaymentService = $globalSaleAggregatePaymentService;
        $this->userRepository = $userRepository;
    }

    /**
     * @param $attributesToUpdate
     * @param Transaction $transaction
     */
    public function update($attributesToUpdate, Transaction $transaction)
    {
        /** @var Transaction $transaction */
        $transaction = $this->transactionRepository->update(
            $attributesToUpdate,
            $transaction->id
        );

        if ($this->transactionIsPaymentPlanSpecification->isSatisfiedBy($transaction)) {
            $user = $transaction->user;

            if ($transaction->isPaid()) {
                $this->renewUserPlanService->to($user);
            }

            $this->userRepository->update(['is_inactive' => $transaction->isCanceled()], $user->id);
        }

        if ($transaction->isPaid()) {
            $this->globalSaleAggregatePaymentService->aggregateValueOfTransaction($transaction);
        }
    }
}
