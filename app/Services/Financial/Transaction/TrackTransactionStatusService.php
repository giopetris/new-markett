<?php

namespace NewMarkett\Services\Financial\Transaction;

use NewMarkett\Entities\Financial\Transaction;
use NewMarkett\Repositories\Financial\TransactionRepository;

class TrackTransactionStatusService
{
    /**
     * @var TransactionRepository
     */
    private $transactionRepository;

    /**
     * @param TransactionRepository $transactionRepository
     */
    public function __construct(TransactionRepository $transactionRepository)
    {
        $this->transactionRepository = $transactionRepository;
    }

    /**
     * @param Transaction $transaction
     */
    public function of(Transaction $transaction)
    {
        if ($transaction->id == null) {
            return;
        }

        $transactions = $this->transactionRepository->findByField('track_status_transaction_id', $transaction->id);

        foreach ($transactions as $transactionToUpdate) {
            $this->transactionRepository->update(
                [
                    'transaction_status_id' => $transaction->transaction_status_id,
                    'paid_at' => $transaction->paid_at
                ],
                $transactionToUpdate->id
            );
        }
    }
}
