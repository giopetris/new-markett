<?php

namespace NewMarkett\Services\Financial\Transaction;

use NewMarkett\Entities\User\User;
use NewMarkett\PickLists\Financial\TransactionScopeNamePickList;
use NewMarkett\PickLists\Financial\TransactionStatusNamePickList;
use NewMarkett\PickLists\Financial\TransactionSubTypeNamePickList;
use NewMarkett\Repositories\Financial\TransactionRepository;
use NewMarkett\Repositories\User\UserRepository;
use NewMarkett\Services\Financial\Calculator\DirectComissionCalculator;
use NewMarkett\Specifications\User\UserCanReceivePaymentsSpecification;

class ThrowTransactionsToUsersWithExpiredPlanService
{
    /**
     * @var TransactionRepository
     */
    private $transactionRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var DirectComissionCalculator
     */
    private $directComissionCalculator;

    /**
     * @var UserCanReceivePaymentsSpecification
     */
    private $userCanReceivePaymentsSpecification;

    /**
     * @param TransactionRepository $transactionRepository
     * @param UserRepository $userRepository
     * @param DirectComissionCalculator $directComissionCalculator
     * @param UserCanReceivePaymentsSpecification $userCanReceivePaymentsSpecification
     */
    public function __construct(
        TransactionRepository $transactionRepository,
        UserRepository $userRepository,
        DirectComissionCalculator $directComissionCalculator,
        UserCanReceivePaymentsSpecification $userCanReceivePaymentsSpecification
    ) {
        $this->transactionRepository = $transactionRepository;
        $this->userRepository = $userRepository;
        $this->directComissionCalculator = $directComissionCalculator;
        $this->userCanReceivePaymentsSpecification = $userCanReceivePaymentsSpecification;
    }

    public function throwTransactions()
    {
        $users = $this->userRepository->findAffiliatesWithExpiredPlan();

        /** @var User $user */
        foreach ($users as $user) {
            $existTransactionToPlanRenovation = $this->transactionRepository->findWhere([
                'user_id' => $user->id,
                'transaction_subtype_id' => TransactionSubTypeNamePickList::PLAN_RENOVATION,
                'transaction_status_id' => TransactionStatusNamePickList::WAITING_PAYMENT
            ])->first();

            if ($existTransactionToPlanRenovation) {
                continue;
            }

            $plan = $user->plan;
            $sponsor = $user->sponsor;

            $affiliateRenovationPlanTransaction = $this->transactionRepository->create([
                'user_id' => $user->id,
                'transaction_subtype_id' => TransactionSubTypeNamePickList::PLAN_RENOVATION,
                'transaction_status_id' => TransactionStatusNamePickList::WAITING_PAYMENT,
                'transaction_scope_id' => TransactionScopeNamePickList::SCOPE_GLOBAL,
                'description' => 'Renovação do plano',
                'value' => $plan->value
            ]);

            if ($this->userCanReceivePaymentsSpecification->isSatisfiedBy($sponsor)) {
                $this->transactionRepository->create([
                    'user_id' => $sponsor->id,
                    'transaction_subtype_id' => TransactionSubTypeNamePickList::DIRECT_COMMISSION,
                    'transaction_status_id' => TransactionStatusNamePickList::WAITING_PAYMENT,
                    'transaction_scope_id' => TransactionScopeNamePickList::SCOPE_INDIVIDUAL,
                    'description' => 'Comissão direta sobre renovação de plano do afiliado ' . $user->name . ' (ID: ' . $user->id . ')',
                    'value' => $this->directComissionCalculator->calculeToPlan($plan),
                    'track_status_transaction_id' => $affiliateRenovationPlanTransaction->id
                ]);
            }
        }
    }
}
