<?php

namespace NewMarkett\Services\Financial\Transaction;

use NewMarkett\Entities\Financial\GlobalSale;
use NewMarkett\Entities\User\User;
use NewMarkett\PickLists\Financial\TransactionScopeNamePickList;
use NewMarkett\PickLists\Financial\TransactionStatusNamePickList;
use NewMarkett\PickLists\Financial\TransactionSubTypeNamePickList;
use NewMarkett\PickLists\User\PlanNamePickList;
use NewMarkett\Repositories\Financial\TransactionRepository;
use NewMarkett\Repositories\User\UserRepository;
use NewMarkett\Services\User\GetStatusService;

class ThrowTransactionsToGlobalSaleService
{
    /**
     * @var TransactionRepository
     */
    private $transactionRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @param TransactionRepository $transactionRepository
     * @param UserRepository $userRepository
     */
    public function __construct(
        TransactionRepository $transactionRepository,
        UserRepository $userRepository) {
        $this->transactionRepository = $transactionRepository;
        $this->userRepository = $userRepository;
    }

    public function to(GlobalSale $globalSale)
    {
        $users = $this->userRepository->findAffiliatesValidToParticipateOfShareGlobal();

        /** @var User $user */
        foreach ($users as $user) {
            $originalPlanName = PlanNamePickList::getOriginalName($user->plan_id);
            $totalValueToUsersBelongingToPlanAttributeName = sprintf('total_value_to_users_with_%s_plan', strtolower($originalPlanName));
            $value = $globalSale->$totalValueToUsersBelongingToPlanAttributeName;

            $this->transactionRepository->create([
                'user_id' => $user->id,
                'transaction_subtype_id' => TransactionSubTypeNamePickList::GLOBAL_SALES,
                'transaction_status_id' => TransactionStatusNamePickList::PAID,
                'transaction_scope_id' => TransactionScopeNamePickList::SCOPE_INDIVIDUAL,
                'description' => 'Faturamento global',
                'value' => $value,
                'paid_at' => $globalSale->divided_at->toDateString()
            ]);
        }
    }
}