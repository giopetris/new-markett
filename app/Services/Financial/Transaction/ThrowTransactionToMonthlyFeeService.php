<?php

namespace NewMarkett\Services\Financial\Transaction;

use Carbon\Carbon;
use NewMarkett\Entities\User\User;
use NewMarkett\PickLists\Financial\PaymentFormNamePickList;
use NewMarkett\PickLists\Financial\TransactionScopeNamePickList;
use NewMarkett\PickLists\Financial\TransactionStatusNamePickList;
use NewMarkett\PickLists\Financial\TransactionSubTypeNamePickList;
use NewMarkett\Repositories\Financial\TransactionRepository;
use NewMarkett\Specifications\Financial\UserHasBalanceToDeductMonthlyFeeSpecification;
use NewMarkett\Specifications\User\UserCanReceivePaymentsSpecification;
use NewMarkett\ValueObjects\Financial\MonthlyFee;

class ThrowTransactionToMonthlyFeeService
{
    /**
     * @var TransactionRepository
     */
    private $transactionRepository;

    /**
     * @var UserHasBalanceToDeductMonthlyFeeSpecification
     */
    private $userHasBalanceToDeductMonthlyFeeSpecification;

    /**
     * @var UserCanReceivePaymentsSpecification
     */
    private $userCanReceivePaymentsSpecification;

    /**
     * @param TransactionRepository $transactionRepository
     * @param UserHasBalanceToDeductMonthlyFeeSpecification $userHasBalanceToDeductMonthlyFeeSpecification
     * @param UserCanReceivePaymentsSpecification $userCanReceivePaymentsSpecification
     */
    public function __construct(
        TransactionRepository $transactionRepository,
        UserHasBalanceToDeductMonthlyFeeSpecification $userHasBalanceToDeductMonthlyFeeSpecification,
        UserCanReceivePaymentsSpecification $userCanReceivePaymentsSpecification
    ) {
        $this->transactionRepository = $transactionRepository;
        $this->userHasBalanceToDeductMonthlyFeeSpecification = $userHasBalanceToDeductMonthlyFeeSpecification;
        $this->userCanReceivePaymentsSpecification = $userCanReceivePaymentsSpecification;
    }

    public function to(User $user)
    {
        $now = new Carbon();

        if ( ! $this->userHasBalanceToDeductMonthlyFeeSpecification->isSatisfiedBy($user)) {
            // IMPORTANT: This code is duplicate below and should be replace in these two places
            $monthlyFeeCollect = $this->transactionRepository->create([
                'user_id' => $user->id,
                'transaction_subtype_id' => TransactionSubTypeNamePickList::MONTHLY_FEE_COLLECT,
                'transaction_status_id' => TransactionStatusNamePickList::WAITING_PAYMENT,
                'transaction_scope_id' => TransactionScopeNamePickList::SCOPE_GLOBAL,
                'description' => sprintf('Cobrança da taxa mensal de %s/%d', $now->format('m'), $now->format('Y')),
                'value' => MonthlyFee::VALUE,
                'payment_form_id' => PaymentFormNamePickList::NEGOTIATED_WITH_NEW_MARKETT,
            ]);

            $this->transactionRepository->create([
                'user_id' => $user->id,
                'transaction_subtype_id' => TransactionSubTypeNamePickList::MONTHLY_FEE_PAYMENT,
                'transaction_status_id' => TransactionStatusNamePickList::WAITING_PAYMENT,
                'transaction_scope_id' => TransactionScopeNamePickList::SCOPE_INDIVIDUAL,
                'description' => sprintf('Pagamento da taxa mensal de %s/%d', $now->format('m'), $now->format('Y')),
                'value' => MonthlyFee::VALUE,
                'track_status_transaction_id' => $monthlyFeeCollect->id,
                'payment_form_id' => PaymentFormNamePickList::NEGOTIATED_WITH_NEW_MARKETT
            ]);

            $userCurrentLevelIterator = $user;
            for ($level = 1; $level <= MonthlyFee::TOTAL_LEVELS; $level++) {
                $sponsor = $userCurrentLevelIterator->sponsor;

                if ( ! $sponsor) {
                    break;
                }

                $userCurrentLevelIterator = $sponsor;

                if ( ! $this->userCanReceivePaymentsSpecification->isSatisfiedBy($sponsor)) {
                    continue;
                }

                $this->transactionRepository->create([
                    'user_id' => $sponsor->id,
                    'transaction_subtype_id' => TransactionSubTypeNamePickList::MONTHLY_FEE_COMMISSION,
                    'transaction_status_id' => TransactionStatusNamePickList::WAITING_PAYMENT,
                    'transaction_scope_id' => TransactionScopeNamePickList::SCOPE_INDIVIDUAL,
                    'description' => sprintf(
                        'Comissão da taxa mensal de %s/%d sobre o afiliado %s - ID: %d (%dº nível)',
                        $now->format('m'),
                        $now->format('Y'),
                        $user->name,
                        $user->id,
                        $level
                    ),
                    'value' => constant('NewMarkett\ValueObjects\Financial\MonthlyFee::COMMISSION_LEVEL_' . $level),
                    'track_status_transaction_id' => $monthlyFeeCollect->id,
                ]);

            }

            return;
        }

        // IMPORTANT: This code is duplicate above and should be replace in these two places
        $monthlyFeeCollect = $this->transactionRepository->create([
            'user_id' => $user->id,
            'transaction_subtype_id' => TransactionSubTypeNamePickList::MONTHLY_FEE_COLLECT,
            'transaction_status_id' => TransactionStatusNamePickList::PAID,
            'transaction_scope_id' => TransactionScopeNamePickList::SCOPE_GLOBAL,
            'description' => sprintf('Cobrança da taxa mensal de %s/%d', $now->format('m'), $now->format('Y')),
            'value' => MonthlyFee::VALUE,
            'payment_form_id' => PaymentFormNamePickList::DEDUCTED_FROM_THE_BALANCE,
            'paid_at' => $now->toDateString()
        ]);

        $this->transactionRepository->create([
            'user_id' => $user->id,
            'transaction_subtype_id' => TransactionSubTypeNamePickList::MONTHLY_FEE_PAYMENT,
            'transaction_status_id' => TransactionStatusNamePickList::PAID,
            'transaction_scope_id' => TransactionScopeNamePickList::SCOPE_INDIVIDUAL,
            'description' => sprintf('Pagamento da taxa mensal de %s/%d', $now->format('m'), $now->format('Y')),
            'value' => MonthlyFee::VALUE,
            'track_status_transaction_id' => $monthlyFeeCollect->id,
            'payment_form_id' => PaymentFormNamePickList::DEDUCTED_FROM_THE_BALANCE,
            'paid_at' => $now->toDateString()
        ]);

        $userCurrentLevelIterator = $user;
        for ($level = 1; $level <= MonthlyFee::TOTAL_LEVELS; $level++) {
            $sponsor = $userCurrentLevelIterator->sponsor;

            if ( ! $sponsor) {
                break;
            }

            $userCurrentLevelIterator = $sponsor;

            if ( ! $this->userCanReceivePaymentsSpecification->isSatisfiedBy($sponsor)) {
                continue;
            }

            $this->transactionRepository->create([
                'user_id' => $sponsor->id,
                'transaction_subtype_id' => TransactionSubTypeNamePickList::MONTHLY_FEE_COMMISSION,
                'transaction_status_id' => TransactionStatusNamePickList::PAID,
                'transaction_scope_id' => TransactionScopeNamePickList::SCOPE_INDIVIDUAL,
                'description' => sprintf(
                    'Comissão da taxa mensal de %s/%d sobre o afiliado %s - ID: %d (%dº nível)',
                    $now->format('m'),
                    $now->format('Y'),
                    $user->name,
                    $user->id,
                    $level
                ),
                'value' => constant('NewMarkett\ValueObjects\Financial\MonthlyFee::COMMISSION_LEVEL_' . $level),
                'track_status_transaction_id' => $monthlyFeeCollect->id,
                'paid_at' => $now->toDateString()
            ]);

            $userCurrentLevelIterator = $sponsor;
        }
    }
}