<?php

namespace NewMarkett\Services\Financial\GlobalSale;

use Carbon\Carbon;
use NewMarkett\Entities\Financial\GlobalSale;
use NewMarkett\PickLists\User\PlanNamePickList;
use NewMarkett\Repositories\Financial\GlobalSaleRepository;
use NewMarkett\Repositories\User\PlanRepository;
use NewMarkett\Repositories\User\UserRepository;
use NewMarkett\Services\Financial\Calculator\GlobalSaleCalculator;
use NewMarkett\Services\Financial\Transaction\ThrowTransactionsToGlobalSaleService;

class ShareService
{
    /**
     * @var GlobalSaleRepository
     */
    private $globalSaleRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var GlobalSaleCalculator
     */
    private $globalSaleCalculator;

    /**
     * @var PlanRepository
     */
    private $planRepository;

    /**
     * @var ThrowTransactionsToGlobalSaleService
     */
    private $throwTransactionsToGlobalSaleService;

    /**
     * @param GlobalSaleRepository $globalSaleRepository
     * @param UserRepository $userRepository
     * @param GlobalSaleCalculator $globalSaleCalculator
     * @param PlanRepository $planRepository
     * @param ThrowTransactionsToGlobalSaleService $throwTransactionsToGlobalSaleService
     */
    public function __construct(
        GlobalSaleRepository $globalSaleRepository,
        UserRepository $userRepository,
        GlobalSaleCalculator $globalSaleCalculator,
        PlanRepository $planRepository,
        ThrowTransactionsToGlobalSaleService $throwTransactionsToGlobalSaleService
    ) {
        $this->globalSaleRepository = $globalSaleRepository;
        $this->userRepository = $userRepository;
        $this->globalSaleCalculator = $globalSaleCalculator;
        $this->planRepository = $planRepository;
        $this->throwTransactionsToGlobalSaleService = $throwTransactionsToGlobalSaleService;
    }

    public function share()
    {
        $globalSales = $this->globalSaleRepository->findGlobalSalesToDivide()->all();

        /** @var GlobalSale $globalSale */
        foreach ($globalSales as $globalSale) {
            $globalSale = $this->calculateValuesToGlobalSale($globalSale);

            $this->throwTransactionsToGlobalSaleService->to($globalSale);
        }
    }

    private function calculateValuesToGlobalSale(GlobalSale $globalSale)
    {
        $plans = PlanNamePickList::getDataWithOriginalNames();

        foreach ($plans as $planId => $planOriginalName) {
            $plan = $this->planRepository->find($planId);

            $numberUsersBelongingToPlan = count($this
                ->userRepository
                ->findAffiliatesValidToParticipateOfShareGlobalBelongingToPlan($plan)
            );

            $totalValueToUserBelongingToPlan = $this->globalSaleCalculator->calculateTotalValueToUser(
                $globalSale->total_sale,
                $plan,
                $numberUsersBelongingToPlan
            );

            $numberUsersBelongingToPlanVariableName = sprintf('number_users_with_%s_plan', strtolower($planOriginalName));
            $totalValueToUsersBelongingToPlanVariableName = sprintf('total_value_to_users_with_%s_plan', strtolower($planOriginalName));
            $globalSale->$numberUsersBelongingToPlanVariableName = $numberUsersBelongingToPlan;
            $globalSale->$totalValueToUsersBelongingToPlanVariableName = $totalValueToUserBelongingToPlan;
        }

        $globalSale->divided_at = new Carbon();
        $globalSale->save();

        return $globalSale;
    }
}
