<?php

namespace NewMarkett\Services\Financial\GlobalSale;

use Carbon\Carbon;
use NewMarkett\Entities\Financial\Transaction;
use NewMarkett\Repositories\Financial\GlobalSaleRepository;

class AggregatePaymentService
{
    /**
     * @var GlobalSaleRepository
     */
    private $globalSaleRepository;

    /**
     * @param GlobalSaleRepository $globalSaleRepository
     */
    public function __construct(GlobalSaleRepository $globalSaleRepository)
    {
        $this->globalSaleRepository = $globalSaleRepository;
    }

    /**
     * @param Transaction $transaction
     */
    public function aggregateValueOfTransaction(Transaction $transaction)
    {
        $date = new Carbon();

        $globalSale = $this->globalSaleRepository->findByField('date', $date->toDateString())->first();

        if ($globalSale) {
            $totalSale = $globalSale->total_sale + $transaction->value;
            $this->globalSaleRepository->update(['total_sale' => $totalSale], $globalSale->id);

            return;
        }

        $this->globalSaleRepository->create(['date' => $date, 'total_sale' => $transaction->value]);
    }
}
