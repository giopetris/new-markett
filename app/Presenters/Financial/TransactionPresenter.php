<?php

namespace NewMarkett\Presenters\Financial;

use Carbon\Carbon;
use Laracasts\Presenter\Presenter;
use NewMarkett\DataTransformers\Money\FloatToMoneyDataTransformer;
use NewMarkett\Entities\Financial\TransactionSubType;
use NewMarkett\Entities\Financial\TransactionType;

class TransactionPresenter extends Presenter
{
    public function value()
    {
        $floatToMoneyDataTransformer = new FloatToMoneyDataTransformer();

        return $floatToMoneyDataTransformer->transform($this->entity->value);
    }

    public function createdAt()
    {
        /** @var Carbon $date */
        $date = $this->entity->created_at;

        return $date->format('d/m/Y');
    }

    public function paidAt()
    {
        /** @var Carbon $date */
        $date = $this->entity->paid_at;

        if ($date == null) {
            return null;
        }

        return $date->format('d/m/Y');
    }

    public function typeAndSubtype()
    {
        /** @var TransactionSubType $subtype */
        $subtype = $this->entity->transactionSubtype;
        /** @var TransactionType $type */
        $type = $subtype->transactionType;

        return sprintf('%s (%s)', $type->name, $subtype->name);
    }
}
