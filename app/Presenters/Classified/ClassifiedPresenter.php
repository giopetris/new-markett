<?php

namespace NewMarkett\Presenters\Classified;

use Carbon\Carbon;
use Laracasts\Presenter\Presenter;
use NewMarkett\DataTransformers\Phone\StringToPhoneDataTransformer;

class ClassifiedPresenter extends Presenter
{
    public function phone()
    {
        $stringToPhoneDataTransformer = new StringToPhoneDataTransformer();

        return $stringToPhoneDataTransformer->transform($this->entity->phone);
    }

    public function cellular()
    {
        $stringToPhoneDataTransformer = new StringToPhoneDataTransformer();

        return $stringToPhoneDataTransformer->transform($this->entity->cellular);
    }

    public function phones()
    {
        if ($this->entity->phone && $this->entity->cellular) {
            return $this->phone() . ' ou ' . $this->cellular();
        }

        if ($this->entity->phone) {
            return $this->phone();
        }

        if ($this->entity->cellular) {
            return $this->cellular();
        }
    }

    public function updatedAt()
    {
        /** @var Carbon $date */
        $date = $this->entity->updated_at;

        if ($date == null) {
            return null;
        }

        return $date->format('d/m/Y');
    }
}
