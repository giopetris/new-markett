<?php

namespace NewMarkett\Presenters\Address;

use Laracasts\Presenter\Presenter;
use NewMarkett\DataTransformers\Address\Cep\StringToCepDataTransformer;

class AddressPresenter extends Presenter
{
    public function cep()
    {
        $stringToCepDataTransformer = new StringToCepDataTransformer();

        return $stringToCepDataTransformer->transform($this->entity->cep);
    }
}
