<?php

namespace NewMarkett\Presenters\ValueObject\Financial;

use Laracasts\Presenter\Presenter;
use NewMarkett\DataTransformers\Money\FloatToMoneyDataTransformer;

class FinancialPositionPresenter extends Presenter
{
    public function getTotalEntriesPaid()
    {
        $floatToMoneyDataTransformer = new FloatToMoneyDataTransformer();

        return $floatToMoneyDataTransformer->transform($this->entity->getTotalEntriesPaid());
    }

    public function getTotalOutputsPaid()
    {
        $floatToMoneyDataTransformer = new FloatToMoneyDataTransformer();

        return $floatToMoneyDataTransformer->transform($this->entity->getTotalOutputsPaid());
    }

    public function getTotalEntriesPaidOrWaitingPayment()
    {
        $floatToMoneyDataTransformer = new FloatToMoneyDataTransformer();

        return $floatToMoneyDataTransformer->transform($this->entity->getTotalEntriesPaidOrWaitingPayment());
    }

    public function getTotalOutputsPaidOrWaitingPayment()
    {
        $floatToMoneyDataTransformer = new FloatToMoneyDataTransformer();

        return $floatToMoneyDataTransformer->transform($this->entity->getTotalOutputsPaidOrWaitingPayment());
    }

    public function getTotalBalanceTransactionPaid()
    {
        $floatToMoneyDataTransformer = new FloatToMoneyDataTransformer();

        return $floatToMoneyDataTransformer->transform($this->entity->getTotalBalanceTransactionPaid());
    }

    public function getTotalBalanceTransactionPaidOrWaitingPayment()
    {
        $floatToMoneyDataTransformer = new FloatToMoneyDataTransformer();

        return $floatToMoneyDataTransformer->transform($this->entity->getTotalBalanceTransactionPaidOrWaitingPayment());
    }
}
