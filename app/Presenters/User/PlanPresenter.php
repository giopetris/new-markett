<?php

namespace NewMarkett\Presenters\User;

use Laracasts\Presenter\Presenter;
use NewMarkett\DataTransformers\Money\FloatToMoneyDataTransformer;

class PlanPresenter extends Presenter
{
    public function value()
    {
        $floatToMoneyDataTransformer = new FloatToMoneyDataTransformer();

        return $floatToMoneyDataTransformer->transform($this->entity->value);
    }

    public function quotas()
    {
        return $this->entity->shares_number == 1 ?
            $this->entity->shares_number . ' cota' :
            $this->entity->shares_number . ' cotas';
    }

    public function percentageComissionGlobalValue()
    {
        return $this->percentage_comission_global_value . '% divisão global';
    }
}
