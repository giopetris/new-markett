<?php

namespace NewMarkett\Presenters\User;

use Carbon\Carbon;
use Laracasts\Presenter\Presenter;
use NewMarkett\DataTransformers\PersonalData\Cpf\StringToCpfDataTransformer;
use NewMarkett\DataTransformers\Phone\StringToPhoneDataTransformer;

class UserPresenter extends Presenter
{
    public function phone()
    {
        $stringToPhoneDataTransformer = new StringToPhoneDataTransformer();

        return $stringToPhoneDataTransformer->transform($this->entity->phone);
    }

    public function cellular()
    {
        $stringToPhoneDataTransformer = new StringToPhoneDataTransformer();

        return $stringToPhoneDataTransformer->transform($this->entity->cellular);
    }

    public function phones($breakLine = false)
    {
        if ($this->entity->phone && $this->entity->cellular) {
            if ( ! $breakLine) {
                return $this->phone() . ' ou ' . $this->cellular();
            }

            return $this->phone() . ' <br /> ' . $this->cellular();
        }

        if ($this->entity->phone) {
            return $this->phone();
        }

        if ($this->entity->cellular) {
            return $this->cellular();
        }
    }

    public function cpf()
    {
        $stringToCpfDataTransformer = new StringToCpfDataTransformer();

        return $stringToCpfDataTransformer->transform($this->entity->cpf);
    }

    public function createdAt()
    {
        /** @var Carbon $date */
        $date = $this->entity->created_at;

        return $date->format('d/m/Y');
    }
}
