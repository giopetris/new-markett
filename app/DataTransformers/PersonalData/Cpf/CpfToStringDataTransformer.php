<?php

namespace NewMarkett\DataTransformers\PersonalData\Cpf;

use NewMarkett\DataTransformers\Base\DataTransformer;

class CpfToStringDataTransformer implements DataTransformer
{
    /**
     * {@inheritdoc}
     */
    public function transform($value)
    {
        return str_replace('.', '', str_replace('-', '', $value));
    }
}