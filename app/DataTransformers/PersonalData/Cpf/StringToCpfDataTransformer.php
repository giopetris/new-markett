<?php

namespace NewMarkett\DataTransformers\PersonalData\Cpf;

use NewMarkett\DataTransformers\Base\DataTransformer;
use Mask\Predefined\Br\Cpf as CpfMask;

class StringToCpfDataTransformer implements DataTransformer
{
    /**
     * @param $value
     * @return bool
     */
    public function isValid($value)
    {
        if (strlen($value) !== 11) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function transform($value)
    {
        if ( ! $this->isValid($value)) {
            return '';
        }

        $cpfMask = new CpfMask();

        return $cpfMask->mask($value);
    }
}
