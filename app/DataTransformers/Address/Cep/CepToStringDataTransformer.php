<?php

namespace NewMarkett\DataTransformers\Address\Cep;

use NewMarkett\DataTransformers\Base\DataTransformer;

class CepToStringDataTransformer implements DataTransformer
{
    /**
     * {@inheritdoc}
     */
    public function transform($value)
    {
        return preg_replace("/[^0-9]/", "", $value);
    }
}
