<?php

namespace NewMarkett\DataTransformers\Address\Cep;

use NewMarkett\DataTransformers\Base\DataTransformer;
use Mask\Predefined\Br\Cep as CepMask;

class StringToCepDataTransformer implements DataTransformer
{
    /**
     * @param $value
     * @return bool
     */
    public function isValid($value)
    {
        if (strlen($value) !== 8) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function transform($value)
    {
        if ( ! $this->isValid($value)) {
            return '';
        }

        $mask = new CepMask();

        return $mask->mask($value);
    }
}
