<?php

namespace NewMarkett\DataTransformers\Date;

use Carbon\Carbon;
use NewMarkett\DataTransformers\Base\DataTransformer;

class StringToDateDataTransformer implements DataTransformer
{
    /**
     * {@inheritdoc}
     */
    public function transform($date)
    {
        return Carbon::createFromFormat('d/m/Y', $date);
    }
}
