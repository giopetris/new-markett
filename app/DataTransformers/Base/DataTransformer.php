<?php

namespace NewMarkett\DataTransformers\Base;

interface DataTransformer
{
    /**
     * @param $value
     * @return mixed
     */
    public function transform($value);
}