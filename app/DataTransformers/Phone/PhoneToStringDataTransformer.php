<?php

namespace NewMarkett\DataTransformers\Phone;

use NewMarkett\DataTransformers\Base\DataTransformer;

class PhoneToStringDataTransformer implements DataTransformer
{
    /**
     * {@inheritdoc}
     */
    public function transform($value)
    {
        return preg_replace("/[^0-9]/", "", $value);
    }
}
