<?php

namespace NewMarkett\DataTransformers\Phone;

use NewMarkett\DataTransformers\Base\DataTransformer;
use NewMarkett\Masks\PhoneMask;

class StringToPhoneDataTransformer implements DataTransformer
{
    /**
     * @param $money
     * @return bool
     */
    public function isValid($value)
    {
        if (strlen($value) < 10 || strlen($value) > 11) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function transform($value)
    {
        if ( ! $this->isValid($value)) {
            return '';
        }

        $phoneMask = new PhoneMask(strlen($value));

        return $phoneMask->mask($value);
    }
}
