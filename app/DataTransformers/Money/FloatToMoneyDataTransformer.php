<?php

namespace NewMarkett\DataTransformers\Money;

use NewMarkett\DataTransformers\Base\DataTransformer;

class FloatToMoneyDataTransformer implements DataTransformer
{
    /**
     * @param $money
     * @return bool
     */
    public function isValid($money)
    {
        if ( ! is_numeric($money)) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function transform($money)
    {
        if ( ! $this->isValid($money)) {
            return '';
        }

        return 'R$ ' . number_format($money, 2, ',', '.');
    }
}
