<?php

namespace NewMarkett\DataTransformers\Money;

use NewMarkett\DataTransformers\Base\DataTransformer;

class MoneyToFloatDataTransformer implements DataTransformer
{
    /**
     * {@inheritdoc}
     */
    public function transform($money)
    {
        if ( ! $money) {
            return '';
        }

        return (float) str_replace(
            'R$ ',
            '',
            str_replace(
                ',',
                '.',
                str_replace('.', '', $money)
            )
        );
    }

}