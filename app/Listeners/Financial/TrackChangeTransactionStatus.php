<?php

namespace NewMarkett\Listeners\Financial;

use NewMarkett\Events\Financial\TransactionStatusWasChanged;
use NewMarkett\Services\Financial\Transaction\TrackTransactionStatusService;

class TrackChangeTransactionStatus
{
    /**
     * @var TrackTransactionStatusService
     */
    private $trackTransactionStatusService;

    /**
     * @param TrackTransactionStatusService $trackTransactionStatusService
     */
    public function __construct(TrackTransactionStatusService $trackTransactionStatusService)
    {
        $this->trackTransactionStatusService = $trackTransactionStatusService;
    }

    public function handle(TransactionStatusWasChanged $event)
    {
        $transaction = $event->getTransaction();

        $this->trackTransactionStatusService->of($transaction);
    }
}
