<?php

namespace NewMarkett\Validators;

class MinWordsValidator
{
    public function validate($attribute, $value, $parameters)
    {
        return count(
            explode(' ', trim($value))
        ) >= $parameters[0];
    }
}