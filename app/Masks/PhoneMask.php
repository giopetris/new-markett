<?php

namespace NewMarkett\Masks;

use Mask\AbstractMask;

class PhoneMask extends AbstractMask
{
    /**
     * @var int
     */
    private $phoneLength;

    /**
     * @param int $phoneLength
     */
    public function __construct($phoneLength = 10)
    {
        $this->phoneLength = $phoneLength;
    }

    /**
     *
     * @return string
     */
    public function getStringMask()
    {
        if ($this->phoneLength == 10) {
           return '(##) ####-####';
        }

        return '(##) ####-#####';
    }

}
