<?php

use NewMarkett\PickLists\User\RoleNamePickList;

Route::group(['domain' => env('DOMAIN_APP')], function () {
    Route::get(
        '/registro',
        [
            'as' => 'app.register',
            'uses' => 'App\Register\IndexController@getAction'
        ]
    );
    Route::post(
        '/registro',
        [
            'uses' => 'App\Register\IndexController@postAction'
        ]
    );

    Route::get(
        '/login',
        [
            'as' => 'app.auth.login',
            'uses' => 'App\Auth\LoginController@getAction'
        ]
    );

    Route::post(
        '/login',
        [
            'uses' => 'App\Auth\LoginController@postAction'
        ]
    );

    Route::get(
        '/esqueci-minha-senha',
        [
            'as' => 'app.auth.forgot-my-password',
            'uses' => 'App\Auth\ForgotMyPasswordController@getAction'
        ]
    );

    Route::post(
        '/esqueci-minha-senha',
        [
            'uses' => 'App\Auth\ForgotMyPasswordController@postAction'
        ]
    );

    Route::get(
        '/instrucoes-email-resetar-senha',
        [
            'as' => 'app.auth.email-instructions-to-reset-password',
            'uses' => 'App\Auth\EmailInstructionsToResetPasswordController@getAction'
        ]
    );

    Route::get(
        '/resetar-senha/{tokenPasswordReset}',
        [
            'as' => 'app.auth.reset-password',
            'uses' => 'App\Auth\ResetPasswordController@getAction'
        ]
    );

    Route::post(
        '/resetar-senha/{tokenPasswordReset}',
        [
            'uses' => 'App\Auth\ResetPasswordController@postAction'
        ]
    );

    Route::group(['middleware' => ['auth', sprintf('role:%d', RoleNamePickList::AFFILIATED)]], function() {
        Route::get(
            '/logout',
            [
                'as' => 'app.auth.logout',
                'uses' => 'App\Auth\LogoutController@getAction'
            ]
        );

        Route::get(
            '/minha-conta',
            [
                'as' => 'app.my-account',
                'uses' => 'App\MyAccount\IndexController@getAction'
            ]
        );

        Route::post(
            '/minha-conta',
            [
                'uses' => 'App\MyAccount\IndexController@postAction'
            ]
        );

        Route::get(
            '/minha-conta/alterar-senha',
            [
                'as' => 'app.my-account.change-password',
                'uses' => 'App\MyAccount\ChangePasswordController@getAction'
            ]
        );

        Route::post(
            '/minha-conta/alterar-senha',
            [
                'uses' => 'App\MyAccount\ChangePasswordController@postAction'
            ]
        );

        Route::get(
            '/minha-conta/pagamento-cota',
            [
                'as' => 'app.my-account.quota-payment',
                'uses' => 'App\MyAccount\QuotaPaymentController@getAction'
            ]
        );

        Route::get(
            '/minha-conta/pagamento-taxa-mensal',
            [
                'as' => 'app.my-account.monthly-fee-payment',
                'uses' => 'App\MyAccount\MonthlyFeePaymentController@getAction'
            ]
        );

        Route::group(['middleware' => ['restrict_access_by_user_status', 'restrict_access_by_full_registration']], function() {
            Route::get(
                '/minha-rede',
                [
                    'as' => 'app.my-network',
                    'uses' => 'App\MyNetwork\ListController@getAction'
                ]
            );

            Route::get(
                '/extrato',
                [
                    'as' => 'app.extract',
                    'uses' => 'App\Extract\IndexController@getAction'
                ]
            );

            Route::get(
                '/minha-rede/novo-afiliado',
                [
                    'as' => 'app.my-network.new-affiliate',
                    'uses' => 'App\MyNetwork\NewAffiliateController@getAction'
                ]
            );

            Route::post(
                '/minha-rede/novo-afiliado',
                [
                    'uses' => 'App\MyNetwork\NewAffiliateController@postAction'
                ]
            );

            Route::get(
                '/saque',
                [
                    'as' => 'app.loot',
                    'uses' => 'App\Loot\IndexController@getAction'
                ]
            );

            Route::post(
                '/dados-bancarios',
                [
                    'as' => 'app.loot.bank-data',
                    'uses' => 'App\Loot\BankDataController@postAction'
                ]
            );

            Route::post(
                '/solicitacao-saque',
                [
                    'as' => 'app.loot.loot-request',
                    'uses' => 'App\Loot\LootRequestController@postAction'
                ]
            );

            Route::get(
                '/meu-classificado',
                [
                    'as' => 'app.my-classified',
                    'uses' => 'App\MyClassified\IndexController@getAction'
                ]
            );

            Route::post(
                '/meu-classificado',
                [
                    'uses' => 'App\MyClassified\IndexController@postAction'
                ]
            );

            Route::get(
                '/meu-classificado/remover',
                [
                    'as' => 'app.my-classified.remove',
                    'uses' => 'App\MyClassified\RemoveController@getAction'
                ]
            );
        });
    });
});
