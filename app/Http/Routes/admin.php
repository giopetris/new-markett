<?php

use NewMarkett\PickLists\User\RoleNamePickList;

Route::group(['domain' => env('DOMAIN_ADMIN')], function () {
    Route::get(
        '/login',
        [
            'as' => 'admin.auth.login',
            'uses' => 'Admin\Auth\LoginController@getAction'
        ]
    );
    Route::post(
        '/login',
        [
            'uses' => 'Admin\Auth\LoginController@postAction'
        ]
    );

    Route::group(['middleware' => ['auth', sprintf('role:%d', RoleNamePickList::ADMIN)]], function() {
        Route::get(
            '/logout',
            [
                'as' => 'admin.auth.logout',
                'uses' => 'Admin\Auth\LogoutController@getAction'
            ]
        );

        Route::get(
            '/afiliado',
            [
                'as' => 'admin.affiliated',
                'uses' => 'Admin\Affiliated\ListController@getAction'
            ]
        );

        Route::get(
            '/afiliado/editar/{id}',
            [
                'as' => 'admin.affiliated.edit',
                'uses' => 'Admin\Affiliated\EditController@getAction'
            ]
        );

        Route::post(
            '/afiliado/download-excel',
            [
                'as' => 'admin.affiliated.download-excel',
                'uses' => 'Admin\Affiliated\DownloadExcelController@getAction'
            ]
        );

        Route::post(
            '/afiliado/editar/{id}',
            [
                'uses' => 'Admin\Affiliated\EditController@postAction'
            ]
        );

        Route::get(
            '/financeiro/conta-receber',
            [
                'as' => 'admin.financial.bill-to-receive',
                'uses' => 'Admin\Financial\BillToReceive\ListController@getAction'
            ]
        );

        Route::get(
            '/financeiro/conta-receber/editar/{id}',
            [
                'as' => 'admin.financial.bill-to-receive.edit',
                'uses' => 'Admin\Financial\BillToReceive\EditController@getAction'
            ]
        );

        Route::post(
            '/financeiro/conta-receber/editar/{id}',
            [
                'uses' => 'Admin\Financial\BillToReceive\EditController@postAction'
            ]
        );


        Route::get(
            '/financeiro/solicitacao-saque',
            [
                'as' => 'admin.financial.loot-request',
                'uses' => 'Admin\Financial\LootRequest\ListController@getAction'
            ]
        );

        Route::get(
            '/financeiro/solicitacao-saque/editar/{id}',
            [
                'as' => 'admin.financial.loot-request.edit',
                'uses' => 'Admin\Financial\LootRequest\EditController@getAction'
            ]
        );

        Route::post(
            '/financeiro/solicitacao-saque/editar/{id}',
            [
                'uses' => 'Admin\Financial\LootRequest\EditController@postAction'
            ]
        );

        Route::get(
            '/classificado',
            [
                'as' => 'admin.classified',
                'uses' => 'Admin\Classified\ListController@getAction'
            ]
        );

        Route::get(
            '/classificado/editar/{id}',
            [
                'as' => 'admin.classified.edit',
                'uses' => 'Admin\Classified\EditController@getAction'
            ]
        );

        Route::post(
            '/classificado/editar/{id}',
            [
                'uses' => 'Admin\Classified\EditController@postAction'
            ]
        );

        Route::get(
            '/minha-conta',
            [
                'as' => 'admin.my-account',
                'uses' => 'Admin\MyAccount\IndexController@getAction'
            ]
        );

        Route::post(
            '/minha-conta',
            [
                'uses' => 'Admin\MyAccount\IndexController@postAction'
            ]
        );

        Route::get(
            '/minha-conta/alterar-senha',
            [
                'as' => 'admin.my-account.change-password',
                'uses' => 'Admin\MyAccount\ChangePasswordController@getAction'
            ]
        );

        Route::post(
            '/minha-conta/alterar-senha',
            [
                'uses' => 'Admin\MyAccount\ChangePasswordController@postAction'
            ]
        );
    });
});
