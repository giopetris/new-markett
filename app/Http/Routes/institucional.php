<?php

//Route::group(['domain' => env('DOMAIN')], function () {
Route::group([], function () {
    Route::get(
        '/',
        [
            'as' => 'institutional.home',
            'uses' => 'Institutional\Home\IndexController@getAction'
        ]
    );

    Route::post(
        '/contact',
        [
            'as' => 'institutional.contact',
            'uses' => 'Institutional\Home\ContactController@postAction'
        ]
    );

    Route::get(
        '/new-classificados',
        [
            'as' => 'institutional.new-classified',
            'uses' => 'Institutional\NewClassified\ListController@getAction'
        ]
    );

    Route::get(
        '/new-classificados/{id}',
        [
            'as' => 'institutional.new-classified.view',
            'uses' => 'Institutional\NewClassified\ViewController@getAction'
        ]
    );

    Route::get(
        '/download-apresentacao-comercial',
        [
            'as' => 'institutional.download-commercial-presentation',
            'uses' => 'Institutional\Home\DownloadCommercialPresentationController@getAction'
        ]
    );

});
