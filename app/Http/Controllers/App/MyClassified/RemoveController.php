<?php

namespace NewMarkett\Http\Controllers\App\MyClassified;

use Illuminate\Http\Request;
use NewMarkett\Http\Controllers\Controller;
use NewMarkett\Repositories\Classified\ClassifiedRepository;

class RemoveController extends Controller
{
    /**
     * @var ClassifiedRepository
     */
    private $classifiedRepository;

    /**
     * @param ClassifiedRepository $classifiedRepository
     */
    public function __construct(ClassifiedRepository $classifiedRepository)
    {
        $this->classifiedRepository = $classifiedRepository;
    }

    public function getAction(Request $request)
    {
        $user = \Auth::user();

        $classified = $this->classifiedRepository->findByField('user_id', $user->id)->first();

        if ( ! $classified) {
            return redirect()->route('app.my-classified');
        }

        $this->classifiedRepository->delete($classified->id);
        $request->session()->flash('alert-success', 'Classificado removido com sucesso.');

        return redirect()->route('app.my-classified');
    }
}
