<?php

namespace NewMarkett\Http\Controllers\App\MyAccount;

use NewMarkett\Http\Controllers\Controller;

class QuotaPaymentController extends Controller
{
    public function getAction()
    {
        $user = \Auth::user();

        if ($user->plan_expiration_date != '0000-00-00') {
            return redirect()->route('app.my-network');
        }

        return view(
            'app.my-account.quota-payment',
            [
                'user' => $user
            ]
        );
    }
}
