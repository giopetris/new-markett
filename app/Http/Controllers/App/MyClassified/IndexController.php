<?php

namespace NewMarkett\Http\Controllers\App\MyClassified;

use Illuminate\Database\Eloquent\Collection;
use NewMarkett\Http\Controllers\Controller;
use NewMarkett\Http\Requests\App\MyClassified\IndexRequest as MyClassifiedRequest;
use NewMarkett\PickLists\Classified\ClassifiedStatusNamePickList;
use NewMarkett\Repositories\Address\AddressRepository;
use NewMarkett\Repositories\Classified\ClassifiedRepository;
use NewMarkett\Repositories\Classified\ClassifiedStatusRepository;

class IndexController extends Controller
{
    /**
     * @var ClassifiedRepository
     */
    private $classifiedRepository;

    /**
     * @var AddressRepository
     */
    private $addressRepository;

    /**
     * @var ClassifiedStatusRepository
     */
    private $classifiedStatusRepository;

    /**
     * @param ClassifiedRepository $classifiedRepository
     * @param AddressRepository $addressRepository
     * @param ClassifiedStatusRepository $classifiedStatusRepository
     */
    public function __construct(
        ClassifiedRepository $classifiedRepository,
        AddressRepository $addressRepository,
        ClassifiedStatusRepository $classifiedStatusRepository
    ) {
        $this->classifiedRepository = $classifiedRepository;
        $this->addressRepository = $addressRepository;
        $this->classifiedStatusRepository = $classifiedStatusRepository;
    }

    public function getAction()
    {
        $user = \Auth::user();

        $classified = $this->classifiedRepository->findByField('user_id', $user->id)->first();
        if ($classified) {
            $classified->load('address');
        }
        /** @var Collection $statusList */
        $statusList = $this->classifiedStatusRepository->all()->lists('name', 'id');

        return view(
            'app.my-classified.index',
            [
                'classified' => $classified,
                'statusList' => $statusList
            ]
        );
    }

    public function postAction(MyClassifiedRequest $request)
    {
        $user = \Auth::user();

        $dataRequest = $request->all();

        $dataRequest['classified_status_id'] = ClassifiedStatusNamePickList::WAITING_APPROVAL;

        $logoFile = $request->file('logo');
        if ($logoFile) {
            $filePath = '/uploads/app/user/' . $user->id . '/';
            $fileName = 'my_classified_logo.' . $logoFile->getClientOriginalExtension();
            $logoFile->move(public_path() . $filePath, $fileName);

            $dataRequest['logo_path'] = $filePath;
            $dataRequest['file_name'] = $fileName;
            $dataRequest['user_id'] = $user->id;
        }

        $classified = $this->classifiedRepository->findByField('user_id', $user->id)->first();

        if ($classified) {
            $this->addressRepository->update($dataRequest['address'], $classified->address->id);

            $this->classifiedRepository->update($dataRequest, $classified->id);
            $request->session()->flash(
                'alert-success',
                'Dados atualizados com sucesso, em breve suas atualizações serão aprovadas e o classificado estará disponível'
            );
        } else {
            $address = $this->addressRepository->create($dataRequest['address']);
            $dataRequest['address_id'] = $address->id;

            $this->classifiedRepository->create($dataRequest);
            $request->session()->flash(
                'alert-success',
                'Dados cadastrados com sucesso, em breve seu classificado será aprovado e estará disponível'
            );
        }

        return redirect()->route('app.my-classified');
    }
}
