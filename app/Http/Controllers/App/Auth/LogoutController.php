<?php

namespace NewMarkett\Http\Controllers\App\Auth;

use NewMarkett\Http\Controllers\Controller;

class LogoutController extends Controller
{
    public function getAction()
    {
        \Auth::logout();

        return redirect()->route('app.auth.login');
    }
}
