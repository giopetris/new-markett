<?php

namespace NewMarkett\Http\Controllers\App\Auth;

use NewMarkett\Http\Controllers\Controller;
use NewMarkett\Http\Requests\App\Auth\ForgotMyPasswordRequest;
use NewMarkett\PickLists\User\RoleNamePickList;
use NewMarkett\Repositories\User\UserRepository;
use NewMarkett\Services\User\SendMailResetPasswordService;

class ForgotMyPasswordController extends Controller
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var SendMailResetPasswordService
     */
    private $sendMailResetPasswordService;

    /**
     * @param UserRepository $userRepository
     * @param SendMailResetPasswordService $sendMailResetPasswordService
     */
    public function __construct(UserRepository $userRepository, SendMailResetPasswordService $sendMailResetPasswordService)
    {
        $this->userRepository = $userRepository;
        $this->sendMailResetPasswordService = $sendMailResetPasswordService;
    }

    public function getAction()
    {
        if (\Auth::check() && \Auth::user()->role_id == RoleNamePickList::AFFILIATED) {
            return redirect()->route('app.my-network');
        }

        return view('app.auth.forgot-my-password');
    }

    public function postAction(ForgotMyPasswordRequest $request)
    {
        $tokenPasswordReset = uniqid();
        $userId = $request->get('id');

        $user = $this->userRepository->update(['token_password_reset' => $tokenPasswordReset], $userId);

        $this->sendMailResetPasswordService->to($user);

        $request->session()->flash('user.email.reset-password', $user->email);

        return redirect()->route('app.auth.email-instructions-to-reset-password');
    }
}
