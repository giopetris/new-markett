<?php

namespace NewMarkett\Http\Controllers\App\Auth;

use Illuminate\Http\Request;
use NewMarkett\Http\Controllers\Controller;

class EmailInstructionsToResetPasswordController extends Controller
{
    public function getAction(Request $request)
    {
        return view(
            'app.auth.email-instructions-to-reset-password',
            ['email' => $request->session()->get('user.email.reset-password')]
        );
    }
}
