<?php

namespace NewMarkett\Http\Controllers\App\Auth;

use NewMarkett\Http\Controllers\Controller;
use NewMarkett\Http\Requests\App\Auth\ResetPasswordRequest;
use NewMarkett\PickLists\User\RoleNamePickList;
use NewMarkett\Repositories\User\UserRepository;

class ResetPasswordController extends Controller
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function getAction($tokenPasswordReset)
    {
        if (\Auth::check() && \Auth::user()->role_id == RoleNamePickList::AFFILIATED) {
            return redirect()->route('app.my-network');
        }

        $user = $this->userRepository->findByField('token_password_reset', $tokenPasswordReset)->first();

        if ( ! $user) {
            die('Token inválido');
        }

        return view('app.auth.reset-password', ['user' => $user]);
    }

    public function postAction(ResetPasswordRequest $request, $tokenPasswordReset)
    {
        $user = $this->userRepository->findByField('token_password_reset', $tokenPasswordReset)->first();

        if ( ! $user) {
            die('Token inválido');
        }

        $this->userRepository->update(
            [
                'password' => $request->get('password'),
                'token_password_reset' => null
            ],
            $user->id
        );

        $request->session()->flash('alert-success', 'Senha atualizada com sucesso!');

        \Auth::login($user);

        return redirect()->route('app.my-network');
    }
}
