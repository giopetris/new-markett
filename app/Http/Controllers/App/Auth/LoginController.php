<?php

namespace NewMarkett\Http\Controllers\App\Auth;

use Illuminate\Http\Request;
use NewMarkett\Http\Controllers\Controller;
use NewMarkett\PickLists\User\RoleNamePickList;
use NewMarkett\Repositories\User\RoleRepository;

class LoginController extends Controller
{
    /**
     * @var RoleRepository
     */
    private $roleRepository;

    /**
     * @param RoleRepository $roleRepository
     */
    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function getAction()
    {
        if (\Auth::check() && \Auth::user()->role_id == RoleNamePickList::AFFILIATED) {
            return redirect()->route('app.my-network');
        }

        return view('app.auth.login');
    }

    public function postAction(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'password' => 'required',
        ]);

        $role = $this->roleRepository->find(RoleNamePickList::AFFILIATED);

        if (\Auth::attempt([
            'id' => $request->get('id'),
            'password' => $request->get('password'),
            'role_id' => $role->id
        ])
        ) {
            return redirect()->intended('minha-rede');
        }

        $request->session()->flash('alert-danger', 'ID ou senha inválidos.');

        return redirect()->back();
    }
}
