<?php

namespace NewMarkett\Http\Controllers\App\Loot;

use NewMarkett\Entities\User\User;
use NewMarkett\Http\Controllers\Controller;
use NewMarkett\PickLists\Bank\BankNamePickList;
use NewMarkett\PickLists\Financial\TransactionSubTypeNamePickList;
use NewMarkett\Repositories\Bank\BankDataRepository;
use NewMarkett\Repositories\Financial\TransactionRepository;
use NewMarkett\Services\Financial\FinancialPosition\GetFinancialPositionService;

class IndexController extends Controller
{
    /**
     * @var BankDataRepository
     */
    private $bankDataRepository;

    /**
     * @var TransactionRepository
     */
    private $transactionRepository;

    /**
     * @var GetFinancialPositionService
     */
    private $getFinancialPositionService;

    /**
     * @param BankDataRepository $bankDataRepository
     * @param TransactionRepository $transactionRepository
     * @param GetFinancialPositionService $getFinancialPositionService
     */
    public function __construct(
        BankDataRepository $bankDataRepository,
        TransactionRepository $transactionRepository,
        GetFinancialPositionService $getFinancialPositionService
    ) {
        $this->bankDataRepository = $bankDataRepository;
        $this->transactionRepository = $transactionRepository;
        $this->getFinancialPositionService = $getFinancialPositionService;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function getAction()
    {
        /** @var User $user */
        $user = \Auth::user();

        $banks = ['' => 'Selecione'] + BankNamePickList::getData();
        $bankData = $this->bankDataRepository->findByField('user_id', $user->id)->first();
        $lootRequests = $this->transactionRepository->scopeQuery(function($query) {
            return $query->orderBy('created_at', 'desc');
        })->findWhere([
            'user_id' => $user->id,
            'transaction_subtype_id' => TransactionSubTypeNamePickList::LOOT
        ]);

        $financialPosition = $this->getFinancialPositionService->ofUser($user);

        return view(
            'app.loot.index',
            [
                'banks' => $banks,
                'bankData' => $bankData,
                'lootRequests' => $lootRequests,
                'financialPosition' => $financialPosition
            ]
        );
    }
}
