<?php

namespace NewMarkett\Http\Controllers\App\Loot;

use NewMarkett\Http\Controllers\Controller;
use NewMarkett\Http\Requests\App\Loot\LootRequest;
use NewMarkett\PickLists\Financial\TransactionScopeNamePickList;
use NewMarkett\PickLists\Financial\TransactionStatusNamePickList;
use NewMarkett\PickLists\Financial\TransactionSubTypeNamePickList;
use NewMarkett\Repositories\Bank\BankDataRepository;
use NewMarkett\Repositories\Financial\TransactionRepository;
use NewMarkett\Services\Financial\FinancialPosition\GetFinancialPositionService;

class LootRequestController extends Controller
{
    /**
     * @var TransactionRepository
     */
    private $transactionRepository;

    /**
     * @var BankDataRepository
     */
    private $bankDataRepository;

    /**
     * @var GetFinancialPositionService
     */
    private $getFinancialPositionService;

    /**
     * @param TransactionRepository $transactionRepository
     * @param BankDataRepository $bankDataRepository
     * @param GetFinancialPositionService $getFinancialPositionService
     */
    public function __construct(
        TransactionRepository $transactionRepository,
        BankDataRepository $bankDataRepository,
        GetFinancialPositionService $getFinancialPositionService
    ) {
        $this->transactionRepository = $transactionRepository;
        $this->bankDataRepository = $bankDataRepository;
        $this->getFinancialPositionService = $getFinancialPositionService;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function postAction(LootRequest $request)
    {
        $user = \Auth::user();
        $dataRequest = $request->all();

        if ( ! \Hash::check($request->get('password'), $user->password)) {
            $request->session()->flash('alert-danger', 'A senha está incorreta');

            return redirect()->route('app.loot');
        }

        $bankData = $this->bankDataRepository->findByField('user_id', $user->id)->first();

        if ( ! $bankData) {
            $request->session()->flash('alert-danger', 'Não é possível fazer uma solicitação de saque sem cadastrar seus dados bancários.');

            return redirect()->route('app.loot');
        }

        $financialPosition = $this->getFinancialPositionService->ofUser($user);

        if ($dataRequest['value'] > $financialPosition->getTotalBalanceTransactionPaidOrWaitingPayment()) {
            $request->session()->flash('alert-danger', 'O valor solicitado é maior que o saldo disponível para saque.');

            return redirect()->route('app.loot');
        }

        $dataRequest['user_id'] = $user->id;
        $dataRequest['transaction_subtype_id'] = TransactionSubTypeNamePickList::LOOT;
        $dataRequest['transaction_status_id'] = TransactionStatusNamePickList::WAITING_PAYMENT;
        $dataRequest['transaction_scope_id'] = TransactionScopeNamePickList::SCOPE_INDIVIDUAL;
        $dataRequest['description'] = 'Solicitação de saque';

        $this->transactionRepository->create($dataRequest);

        $request->session()->flash('alert-success', 'Solicitação de saque realizada com sucesso, em breve estaremos depositando seu valor.');

        return redirect()->route('app.loot');
    }
}
