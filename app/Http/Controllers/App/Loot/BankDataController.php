<?php

namespace NewMarkett\Http\Controllers\App\Loot;

use NewMarkett\Http\Controllers\Controller;
use NewMarkett\Http\Requests\App\Loot\BankDataRequest;
use NewMarkett\Repositories\Bank\BankDataRepository;

class BankDataController extends Controller
{
    /**
     * @var BankDataRepository
     */
    private $bankDataRepository;

    /**
     * @param BankDataRepository $bankDataRepository
     */
    public function __construct(BankDataRepository $bankDataRepository)
    {
        $this->bankDataRepository = $bankDataRepository;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function postAction(BankDataRequest $request)
    {
        $user = \Auth::user();

        $dataRequest = $request->all();
        $dataRequest['user_id'] = $user->id;

        $bankData = $this->bankDataRepository->findByField('user_id', $user->id)->first();

        if ( ! $bankData) {
            $this->bankDataRepository->create($dataRequest);
        } else {
            $this->bankDataRepository->update($dataRequest, $bankData->id);
        }

        $request->session()->flash('alert-success', 'Dados bancários atualizados com sucesso.');

        return redirect()->route('app.loot');
    }
}
