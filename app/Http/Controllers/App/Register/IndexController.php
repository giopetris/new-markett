<?php

namespace NewMarkett\Http\Controllers\App\Register;

use Illuminate\Http\Request;
use NewMarkett\Entities\User\Plan;
use NewMarkett\Http\Controllers\Controller;
use NewMarkett\Http\Requests\App\Register\IndexRequest;
use NewMarkett\PickLists\User\RoleNamePickList;
use NewMarkett\Repositories\User\PlanRepository;
use NewMarkett\Repositories\User\UserRepository;
use NewMarkett\Services\Financial\Transaction\ThrowTransactionsWhenUserRegisterService;
use NewMarkett\Specifications\User\UserIsInPreRegisterSpecification;

class IndexController extends Controller
{
    /**
     * @var PlanRepository
     */
    private $planRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var ThrowTransactionsWhenUserRegisterService
     */
    private $throwTransactionsWhenUserRegisterService;

    /**
     * @var UserIsInPreRegisterSpecification
     */
    private $userIsInPreRegisterSpecification;

    /**
     * @param PlanRepository $planRepository
     * @param UserRepository $userRepository
     * @param ThrowTransactionsWhenUserRegisterService $throwTransactionsWhenUserRegisterService
     * @param UserIsInPreRegisterSpecification $userIsInPreRegisterSpecification
     */
    public function __construct(
        PlanRepository $planRepository,
        UserRepository $userRepository,
        ThrowTransactionsWhenUserRegisterService $throwTransactionsWhenUserRegisterService,
        UserIsInPreRegisterSpecification $userIsInPreRegisterSpecification
    ) {
        $this->planRepository = $planRepository;
        $this->userRepository = $userRepository;
        $this->throwTransactionsWhenUserRegisterService = $throwTransactionsWhenUserRegisterService;
        $this->userIsInPreRegisterSpecification = $userIsInPreRegisterSpecification;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function getAction(Request $request)
    {
        $preUser = null;
        $preUserId = $request->get('userId');
        if ($preUserId) {
            $preUser = $this->userRepository->find($preUserId);

            if ( ! $this->userIsInPreRegisterSpecification->isSatisfiedBy($preUser)) {
                return redirect()->route('app.auth.login');
            }
        }

        $allPlans = $this->planRepository->all();

        $planOptions = ['' => 'Selecione o plano'];

        /* @var Plan $plan */
        foreach ($allPlans as $plan) {
            $planOptions[$plan->id] = sprintf('%s (%s anuais)', $plan->name, $plan->present()->value);
        }

        return view(
            'app.register.index',
            [
                'planOptions' => $planOptions,
                'preUser' => $preUser
            ]
        );
    }

    /**
     * @param IndexRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postAction(IndexRequest $request)
    {
        $data = $request->all();
        $preDataUserId = $request->get('id');

        if ($preDataUserId) {
            $preUser = $this->userRepository->find($preDataUserId);

            if ( ! $this->userIsInPreRegisterSpecification->isSatisfiedBy($preUser)) {
                $request->session()->flash('alert-error', 'O usuário não está em pré-registro');
                return redirect()->route('app.register');
            }

            $user = $this->userRepository->update($data, $preDataUserId);
        } else {
            $user = $this->userRepository->create($data);
        }

        $user->role_id = RoleNamePickList::AFFILIATED;
        $user->plan_id = $request->get('plan_id');
        $user->save();

        \Auth::login($user);

        $this->throwTransactionsWhenUserRegisterService->to($user);

        return redirect()->route('app.my-network');
    }
}
