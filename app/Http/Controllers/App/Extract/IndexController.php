<?php

namespace NewMarkett\Http\Controllers\App\Extract;

use NewMarkett\Http\Controllers\Controller;
use NewMarkett\PickLists\Financial\TransactionScopeNamePickList;
use NewMarkett\Repositories\Financial\TransactionRepository;

class IndexController extends Controller
{
    /**
     * @var TransactionRepository
     */
    private $transactionRepository;

    /**
     * @param TransactionRepository $transactionRepository
     */
    public function __construct(TransactionRepository $transactionRepository)
    {
        $this->transactionRepository = $transactionRepository;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function getAction()
    {
        $user = \Auth::user();

        $transactions = $this->transactionRepository->scopeQuery(function($query){
            return $query->orderBy('created_at','desc');
        })->findWhere([
            'user_id' => $user->id,
            'transaction_scope_id' => TransactionScopeNamePickList::SCOPE_INDIVIDUAL
        ]);

        return view(
            'app.extract.index',
            ['transactions' => $transactions]
        );
    }
}
