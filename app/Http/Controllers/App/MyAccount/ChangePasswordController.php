<?php

namespace NewMarkett\Http\Controllers\App\MyAccount;

use Illuminate\Http\Request;
use NewMarkett\Http\Controllers\Controller;

class ChangePasswordController extends Controller
{
    public function getAction()
    {
        return view(
            'app.my-account.change-password'
        );
    }

    public function postAction(Request $request)
    {
        $this->validate($request, [
            'current_password' => 'required',
            'new_password' => 'required|confirmed|min:6|max:30',
            'new_password_confirmation' => 'required',
        ]);

        $user = \Auth::user();

        if ( ! \Hash::check($request->get('current_password'), $user->password)) {
            $request->session()->flash('alert-danger', 'A senha atual está incorreta');

            return redirect()->route('app.my-account.change-password');
        }

        $user->password = $request->get('new_password');
        $user->save();

        $request->session()->flash('alert-success', 'Dados alterados com sucesso.');

        return redirect()->route('app.my-account.change-password');
    }
}
