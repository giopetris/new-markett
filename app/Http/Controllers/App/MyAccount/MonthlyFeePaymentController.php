<?php

namespace NewMarkett\Http\Controllers\App\MyAccount;

use NewMarkett\Entities\User\User;
use NewMarkett\Http\Controllers\Controller;
use NewMarkett\Specifications\User\UserIsCompliantWithMonthlyFeeSpecification;
use NewMarkett\Specifications\User\UserIsFullRegistrationSpecification;
use NewMarkett\Specifications\User\UserShouldPayQuotaSpecification;

class MonthlyFeePaymentController extends Controller
{
    /**
     * @var UserIsCompliantWithMonthlyFeeSpecification
     */
    private $userIsCompliantWithMonthlyFeeSpecification;

    /**
     * @param UserIsCompliantWithMonthlyFeeSpecification $userIsCompliantWithMonthlyFeeSpecification
     */
    public function __construct(UserIsCompliantWithMonthlyFeeSpecification $userIsCompliantWithMonthlyFeeSpecification)
    {
        $this->userIsCompliantWithMonthlyFeeSpecification = $userIsCompliantWithMonthlyFeeSpecification;
    }

    public function getAction()
    {
        /** @var User $user */
        $user = \Auth::user();

        if ($this->userIsCompliantWithMonthlyFeeSpecification->isSatisfiedBy($user)) {
            return redirect()->route('app.my-network');
        }

        return view(
            'app.my-account.monthly-fee-payment',
            [
                'user' => $user,
            ]
        );
    }
}
