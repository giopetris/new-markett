<?php

namespace NewMarkett\Http\Controllers\App\MyAccount;

use Illuminate\Http\Request;
use NewMarkett\Entities\Address\Address;
use NewMarkett\Entities\User\User;
use NewMarkett\Http\Controllers\Controller;
use NewMarkett\Http\Requests\App\MyAccount\IndexRequest as MyAccountRequest;
use NewMarkett\Specifications\User\UserCanChangeCPFSpecification;
use NewMarkett\Specifications\User\UserIsFullRegistrationSpecification;

class IndexController extends Controller
{
    /**
     * @var UserCanChangeCPFSpecification
     */
    private $userCanChangeCPFSpecification;

    /**
     * @var UserIsFullRegistrationSpecification
     */
    private $userIsFullRegistrationSpecification;

    /**
     * @param UserCanChangeCPFSpecification $userCanChangeCPFSpecification
     * @param UserIsFullRegistrationSpecification $userIsFullRegistrationSpecification
     */
    public function __construct(
        UserCanChangeCPFSpecification $userCanChangeCPFSpecification,
        UserIsFullRegistrationSpecification $userIsFullRegistrationSpecification
    ) {
        $this->userCanChangeCPFSpecification = $userCanChangeCPFSpecification;
        $this->userIsFullRegistrationSpecification = $userIsFullRegistrationSpecification;
    }

    public function getAction(Request $request)
    {
        /** @var User $user */
        $user = \Auth::user();
        $user->load('sponsor');
        $user->load('address');

        if ( ! $this->userIsFullRegistrationSpecification->isSatisfiedBy($user)) {
            $request->session()->flash('alert-warning', 'Preencha seus dados abaixo para completar seu cadastro');
        }

        return view(
            'app.my-account.index',
            [
                'user' => $user,
                'userCanChangePassword' => $this->userCanChangeCPFSpecification->isSatisfiedBy($user)
            ]
        );
    }

    public function postAction(MyAccountRequest $request)
    {
        /** @var User $user */
        $user = \Auth::user();

        $dataRequest = $request->all();

        $user->update($dataRequest);

        if ($user->address) {
            $user->address->update($dataRequest['address']);
        } else {
            $address = Address::create($dataRequest['address']);
            $user->address()->associate($address);
            $user->save();
        }

        $request->session()->flash('alert-success', 'Dados alterados com sucesso.');

        return redirect()->route('app.my-account');
    }
}
