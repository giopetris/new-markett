<?php

namespace NewMarkett\Http\Controllers\App\MyAccount;

use NewMarkett\Entities\User\User;
use NewMarkett\Http\Controllers\Controller;
use NewMarkett\Specifications\User\UserIsFullRegistrationSpecification;
use NewMarkett\Specifications\User\UserShouldPayQuotaSpecification;

class QuotaPaymentController extends Controller
{
    /**
     * @var UserIsFullRegistrationSpecification
     */
    private $userIsFullRegistrationSpecification;

    /**
     * @var UserShouldPayQuotaSpecification
     */
    private $userShouldPayQuotaSpecification;

    /**
     * @param UserIsFullRegistrationSpecification $userIsFullRegistrationSpecification
     * @param UserShouldPayQuotaSpecification $userShouldPayQuotaSpecification
     */
    public function __construct(
        UserIsFullRegistrationSpecification $userIsFullRegistrationSpecification,
        UserShouldPayQuotaSpecification $userShouldPayQuotaSpecification
    )
    {
        $this->userIsFullRegistrationSpecification = $userIsFullRegistrationSpecification;
        $this->userShouldPayQuotaSpecification = $userShouldPayQuotaSpecification;
    }

    public function getAction()
    {
        /** @var User $user */
        $user = \Auth::user();

        if ( ! $this->userShouldPayQuotaSpecification->isSatisfiedBy($user)) {
            return redirect()->route('app.my-network');
        }

        return view(
            'app.my-account.quota-payment',
            [
                'user' => $user,
                'userIsFullRegistration' => $this->userIsFullRegistrationSpecification->isSatisfiedBy($user)
            ]
        );
    }
}
