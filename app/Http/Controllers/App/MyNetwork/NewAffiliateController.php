<?php

namespace NewMarkett\Http\Controllers\App\MyNetwork;

use NewMarkett\Http\Controllers\Controller;
use NewMarkett\Http\Requests\App\MyNetwork\NewAffiliateRequest;
use NewMarkett\PickLists\User\RoleNamePickList;
use NewMarkett\Repositories\User\UserRepository;

class NewAffiliateController extends Controller
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function getAction()
    {
        return view(
            'app.my-network.new-affiliate'
        );
    }

    public function postAction(NewAffiliateRequest $request)
    {
        $sponsor = \Auth::user();

        $data = $request->all();
        $data['sponsor_id'] = $sponsor->id;

        $user = $this->userRepository->create($data);

        $registerUrl = route('app.register') . '?userId=' . $user->id;

        \Mail::send(
            'mail.app.my-network.register-invitation',
            ['sponsor' => $sponsor, 'user' => $user, 'registerUrl' => $registerUrl],
            function($m) use ($user) {
                $m
                    ->to($user->email, $user->name)
                    ->subject('Convite New Markett');
            }
        );

        return redirect()->route('app.my-network');
    }
}
