<?php

namespace NewMarkett\Http\Controllers\App\MyNetwork;

use NewMarkett\Entities\User\User;
use NewMarkett\Http\Controllers\Controller;
use NewMarkett\Repositories\User\UserRepository;
use NewMarkett\Services\User\GetStatusService;

class ListController extends Controller
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var GetStatusService
     */
    private $getStatusService;

    /**
     * @param UserRepository $userRepository
     * @param GetStatusService $getStatusService
     */
    public function __construct(UserRepository $userRepository, GetStatusService $getStatusService)
    {
        $this->userRepository = $userRepository;
        $this->getStatusService = $getStatusService;
    }

    public function getAction()
    {
        /** @var User $loggedUser */
        $loggedUser = \Auth::user();

        $users = $this->userRepository->scopeQuery(function($query){
            return $query->orderBy('id','desc');
        })->findBySponsor($loggedUser);

        return view(
            'app.my-network.list',
            [
                'users' => $users,
                'getStatusUserService' => $this->getStatusService
            ]
        );
    }
}
