<?php

namespace NewMarkett\Http\Controllers\Institutional\Home;

use Illuminate\Http\Request;
use NewMarkett\Http\Controllers\Controller;

class ContactController extends Controller
{
    public function postAction(Request $request)
    {
        $data = $request->all();

        \Mail::send(
            'mail.institutional.home.contact',
            ['data' => $data],
            function($m) use ($data) {
                $m
                    ->to('contato@newmarkett.com.br', 'Contato New Markett')
                    ->subject('New Markett - Formulário de contato');
            }
        );

        $request->session()->flash('alert-success', 'Mensagem enviada com sucesso.');
        $route = route('institutional.home') . '/#contact';

        return redirect($route);
    }
}
