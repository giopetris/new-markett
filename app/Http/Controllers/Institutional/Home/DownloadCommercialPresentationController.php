<?php

namespace NewMarkett\Http\Controllers\Institutional\Home;

use NewMarkett\Http\Controllers\Controller;

class DownloadCommercialPresentationController extends Controller
{
    public function getAction()
    {
        $path = storage_path() . '/app/institutional/apresentacao_comercial.pdf';
        return response()->download($path);
    }
}
