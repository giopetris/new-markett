<?php

namespace NewMarkett\Http\Controllers\Institutional\Home;

use NewMarkett\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function getAction()
    {
        return view('institutional.home.index');
    }
}
