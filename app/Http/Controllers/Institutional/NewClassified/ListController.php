<?php

namespace NewMarkett\Http\Controllers\Institutional\NewClassified;

use NewMarkett\Http\Controllers\Controller;
use NewMarkett\PickLists\Classified\ClassifiedStatusNamePickList;
use NewMarkett\Repositories\Classified\ClassifiedRepository;

class ListController extends Controller
{
    /**
     * @var ClassifiedRepository
     */
    private $classifiedRepository;

    /**
     * @param ClassifiedRepository $classifiedRepository
     */
    public function __construct(ClassifiedRepository $classifiedRepository)
    {
        $this->classifiedRepository = $classifiedRepository;
    }

    public function getAction()
    {
        $classifieds = $this->classifiedRepository->findByField('classified_status_id', ClassifiedStatusNamePickList::ACTIVE);

        return view(
            'institutional.new-classified.list',
            [
                'classifieds' => $classifieds
            ]
        );
    }
}
