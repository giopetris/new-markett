<?php

namespace NewMarkett\Http\Controllers\Institutional\NewClassified;

use NewMarkett\Http\Controllers\Controller;
use NewMarkett\Repositories\Classified\ClassifiedRepository;

class ViewController extends Controller
{
    /**
     * @var ClassifiedRepository
     */
    private $classifiedRepository;

    /**
     * @param ClassifiedRepository $classifiedRepository
     */
    public function __construct(ClassifiedRepository $classifiedRepository)
    {
        $this->classifiedRepository = $classifiedRepository;
    }

    public function getAction($id)
    {
        $classified = $this->classifiedRepository->find($id);

        return view(
            'institutional.new-classified.view',
            [
                'classified' => $classified
            ]
        );
    }
}
