<?php

namespace NewMarkett\Http\Controllers\Admin\Financial\BillToReceive;

use Illuminate\Http\Request;
use NewMarkett\Http\Controllers\Controller;
use NewMarkett\PickLists\Financial\TransactionStatusNamePickList;
use NewMarkett\Repositories\Financial\TransactionRepository;
use NewMarkett\Repositories\Financial\TransactionStatusRepository;
use NewMarkett\Http\Requests\Admin\Financial\BillToReceive\EditRequest as BillToReceiveEditRequest;
use NewMarkett\Repositories\User\UserRepository;
use NewMarkett\Services\Financial\Transaction\UpdateBillToReceiveService;

class EditController extends Controller
{
    /**
     * @var TransactionRepository
     */
    private $transactionRepository;

    /**
     * @var TransactionStatusRepository
     */
    private $transactionStatusRepository;

    /**
     * @var UpdateBillToReceiveService
     */
    private $updateBillToReceiveService;

    /**
     * @param TransactionRepository $transactionRepository
     * @param TransactionStatusRepository $transactionStatusRepository
     * @param UpdateBillToReceiveService $updateBillToReceiveService
     */
    public function __construct(
        TransactionRepository $transactionRepository,
        TransactionStatusRepository $transactionStatusRepository,
        UpdateBillToReceiveService $updateBillToReceiveService
    ) {
        $this->transactionRepository = $transactionRepository;
        $this->transactionStatusRepository = $transactionStatusRepository;
        $this->updateBillToReceiveService = $updateBillToReceiveService;
    }

    public function getAction($id)
    {
        $transaction = $this->transactionRepository->find($id);

        $transactionStatusList = $this->transactionStatusRepository->all()->lists('name', 'id');

        return view(
            'admin.financial.bill-to-receive.edit',
            [
                'transaction' => $transaction,
                'transactionStatusList' => $transactionStatusList
            ]
        );
    }

    public function postAction(BillToReceiveEditRequest $request, $id)
    {
        $requestData = $request->all();
        $attributesToUpdate = [
            'paid_at' => $requestData['paid_at'],
            'transaction_status_id' => $requestData['transaction_status_id']
        ];

        $transaction = $this->transactionRepository->find($id);

        $this->updateBillToReceiveService->update($attributesToUpdate, $transaction);

        $successMessage = 'Conta a receber atualizada com sucesso';

        if ($requestData['transaction_status_id'] == TransactionStatusNamePickList::PAID) {
            $user = $transaction->user;

            \Mail::send(
                'mail.app.user.register-actived',
                ['user' => $user, 'appUrl' => route('app.my-network')],
                function($m) use ($user) {
                    $m
                        ->to($user->email, $user->name)
                        ->subject('New Markett - Cadastro ativado');
                }
            );

            $successMessage.= ', o cadastro do usuário foi ativado';
        }

        if ($requestData['transaction_status_id'] == TransactionStatusNamePickList::CANCELED) {
            $successMessage.= ', o cadastro do usuário foi inativado';
        }

        $request->session()->flash('alert-success', $successMessage);

        return redirect()->route('admin.financial.bill-to-receive');
    }
}
