<?php

namespace NewMarkett\Http\Controllers\Admin\Financial\BillToReceive;

use NewMarkett\Http\Controllers\Controller;
use NewMarkett\PickLists\Financial\TransactionScopeNamePickList;
use NewMarkett\Repositories\Financial\TransactionRepository;

class ListController extends Controller
{
    /**
     * @var TransactionRepository
     */
    private $transactionRepository;

    /**
     * @param TransactionRepository $transactionRepository
     */
    public function __construct(TransactionRepository $transactionRepository)
    {
        $this->transactionRepository = $transactionRepository;
    }

    public function getAction()
    {
        $transactions = $this->transactionRepository->scopeQuery(function($query) {
            return $query->orderBy('created_at', 'DESC');
        })->findWhere(['transaction_scope_id' => TransactionScopeNamePickList::SCOPE_GLOBAL]);

        return view(
            'admin.financial.bill-to-receive.list',
            [
                'transactions' => $transactions
            ]
        );
    }
}
