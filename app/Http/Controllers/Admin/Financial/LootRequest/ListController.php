<?php

namespace NewMarkett\Http\Controllers\Admin\Financial\LootRequest;

use NewMarkett\Http\Controllers\Controller;
use NewMarkett\PickLists\Financial\TransactionSubTypeNamePickList;
use NewMarkett\Repositories\Financial\TransactionRepository;

class ListController extends Controller
{
    /**
     * @var TransactionRepository
     */
    private $transactionRepository;

    /**
     * @param TransactionRepository $transactionRepository
     */
    public function __construct(TransactionRepository $transactionRepository)
    {
        $this->transactionRepository = $transactionRepository;
    }

    public function getAction()
    {
        $lootRequests = $this->transactionRepository->scopeQuery(function($query) {
            return $query->orderBy('created_at', 'DESC');
        })
            ->findWhere(['transaction_subtype_id' => TransactionSubTypeNamePickList::LOOT])
            ->all();

        return view(
            'admin.financial.loot-request.list',
            [
                'lootRequests' => $lootRequests
            ]
        );
    }
}
