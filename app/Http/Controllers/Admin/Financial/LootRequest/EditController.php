<?php

namespace NewMarkett\Http\Controllers\Admin\Financial\LootRequest;

use Illuminate\Http\Request;
use NewMarkett\Http\Controllers\Controller;
use NewMarkett\Http\Requests\Admin\Financial\LootRequest\EditRequest;
use NewMarkett\Repositories\Financial\TransactionRepository;
use NewMarkett\Repositories\Financial\TransactionStatusRepository;

class EditController extends Controller
{
    /**
     * @var TransactionRepository
     */
    private $transactionRepository;

    /**
     * @var TransactionStatusRepository
     */
    private $transactionStatusRepository;

    /**
     * @param TransactionRepository $transactionRepository
     * @param TransactionStatusRepository $transactionStatusRepository
     */
    public function __construct(TransactionRepository $transactionRepository, TransactionStatusRepository $transactionStatusRepository)
    {
        $this->transactionRepository = $transactionRepository;
        $this->transactionStatusRepository = $transactionStatusRepository;
    }

    public function getAction($id)
    {
        $lootRequest = $this->transactionRepository->find($id);

        $lootStatusList = $this->transactionStatusRepository->all()->lists('name', 'id');

        return view(
            'admin.financial.loot-request.edit',
            [
                'lootRequest' => $lootRequest,
                'lootStatusList' => $lootStatusList
            ]
        );
    }

    public function postAction(EditRequest $request, $id)
    {
        $requestData = $request->all();

        $this->transactionRepository->update([
            'transaction_status_id' => $requestData['transaction_status_id'],
            'paid_at' => $requestData['paid_at']
        ], $id);

        $request->session()->flash('alert-success', 'Status atualizado com sucesso');

        return redirect()->route('admin.financial.loot-request');
    }
}
