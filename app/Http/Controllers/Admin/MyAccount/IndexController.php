<?php

namespace NewMarkett\Http\Controllers\Admin\MyAccount;

use NewMarkett\Entities\User\User;
use NewMarkett\Http\Controllers\Controller;
use NewMarkett\Http\Requests\Admin\MyAccount\IndexRequest as MyAccountRequest;

class IndexController extends Controller
{
    public function getAction()
    {
        $user = \Auth::user();

        return view(
            'admin.my-account.index',
            [
                'user' => $user
            ]
        );
    }

    public function postAction(MyAccountRequest $request)
    {
        /** @var User $user */
        $user = \Auth::user();

        $dataRequest = $request->all();

        $user->update($dataRequest);

        $request->session()->flash('alert-success', 'Dados alterados com sucesso.');

        return redirect()->route('admin.my-account');
    }
}
