<?php

namespace NewMarkett\Http\Controllers\Admin\Auth;

use NewMarkett\Http\Controllers\Controller;

class LogoutController extends Controller
{
    public function getAction()
    {
        \Auth::logout();

        return redirect()->route('admin.auth.login');
    }
}
