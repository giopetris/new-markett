<?php

namespace NewMarkett\Http\Controllers\Admin\Affiliated;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Collections\CellCollection;
use Maatwebsite\Excel\Excel;
use NewMarkett\Http\Controllers\Controller;
use NewMarkett\PickLists\User\RoleNamePickList;
use NewMarkett\Repositories\User\UserRepository;
use NewMarkett\Services\User\GetStatusService;

class ListController extends Controller
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var GetStatusService
     */
    private $getStatusService;

    /**
     * @var Excel
     */
    private $excel;

    /**
     * @param UserRepository $userRepository
     * @param GetStatusService $getStatusService
     * @param Excel $excel
     */
    public function __construct(
        UserRepository $userRepository,
        GetStatusService $getStatusService,
        Excel $excel
    ) {
        $this->userRepository = $userRepository;
        $this->getStatusService = $getStatusService;
        $this->excel = $excel;
    }

    public function getAction(Request $request)
    {
        $affiliates = $this->userRepository->findAffiliates($request->all());

        $affiliatesData = [];

        foreach ($affiliates as $affiliate) {
            $sponsor = $affiliate->sponsor;
            $address = $affiliate->address;
            $planText = '';
            if ($plan = $affiliate->plan) {
                $planText = $plan->name . ' (' . $plan->present()->percentageComissionGlobalValue() . ')';
            }
            $cityText = '';
            if ($address && $city = $address->city) {
                $cityText = $city->nome . ' - ' . $city->uf;
            }

            $affiliateData = [];
            $affiliateData['ID'] = $affiliate->id;
            $affiliateData['Patrocinador'] = $sponsor->name;
            $affiliateData[] = 'ID: ' . $sponsor->id;
            $affiliateData[] = $sponsor->cpf ? 'CPF: ' . $sponsor->present()->cpf : '';
            $affiliateData['Afiliado'] = $affiliate->name;
            $affiliateData[] = 'ID: ' . $affiliate->id;
            $affiliateData['E-mail'] = $affiliate->email;
            $affiliateData['Telefones'] = $affiliate->present()->phones();
            $affiliateData['Plano'] = $planText;
            $affiliateData['Cidade'] = $cityText;
            $affiliateData['Data afiliação'] = $affiliate->present()->createdAt;
            $affiliateData['Status'] = $this->getStatusService->getStatusNameOf($affiliate);

            $affiliatesData[] = $affiliateData;

        }

        if ($request->get('action') == 'Download Excel') {
            $this->excel->create('Relatório afiliados', function($excel) use ($affiliatesData) {
                /** @var Excel $excel */
                $excel->sheet('Sheetname', function($sheet) use ($affiliatesData) {
                    $countLines = count($affiliatesData) + 1;

                    $sheet->fromArray($affiliatesData);
                    $sheet->mergeCells('B1:D1');
                    $sheet->mergeCells('E1:F1');

                    $sheet->cells('A1:L1', function($cells) {
                        $cells->setFontWeight('bold');
                    });

                    $sheet->cells('A1:L' . $countLines, function($cells) {
                        $cells->setAlignment('center');
                    });

                    for ($i = 1; $i <= $countLines; $i++) {
                        $cell = ['A%d', 'G%d', 'H%d', 'I%d', 'J%d', 'K%d', 'L%d'];
                        $cells = ['B%d:D%d', 'E%d:F%d'];

                        foreach ($cell as $c) {
                            $sheet->cell(sprintf($c, $i), function($cell) {
                                $cell->setBorder('medium', 'medium', 'medium', 'medium');
                            });
                        }

                        foreach ($cells as $c) {
                            $sheet->cells(sprintf($c, $i, $i), function($cells) {
                                $cells->setBorder('medium', 'medium', 'medium', 'medium');
                            });
                        }

                    }
                });
            })->download();
        }

        $request->flash();

        return view(
            'admin.affiliated.list',
            [
                'affiliates' => $affiliates,
                'getStatusUserService' => $this->getStatusService
            ]
        );
    }
}
