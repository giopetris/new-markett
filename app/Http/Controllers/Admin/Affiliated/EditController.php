<?php

namespace NewMarkett\Http\Controllers\Admin\Affiliated;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use NewMarkett\Http\Controllers\Controller;
use NewMarkett\Http\Requests\Admin\Affiliated\EditRequest;
use NewMarkett\PickLists\Financial\TransactionScopeNamePickList;
use NewMarkett\PickLists\Financial\TransactionStatusNamePickList;
use NewMarkett\PickLists\Financial\TransactionSubTypeNamePickList;
use NewMarkett\Repositories\Financial\TransactionRepository;
use NewMarkett\Repositories\User\PlanRepository;
use NewMarkett\Repositories\User\UserRepository;

class EditController extends Controller
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var PlanRepository
     */
    private $planRepository;

    /**
     * @var TransactionRepository
     */
    private $transactionRepository;

    /**
     * @param UserRepository $userRepository
     * @param PlanRepository $planRepository
     * @param TransactionRepository $transactionRepository
     */
    public function __construct(
        UserRepository $userRepository,
        PlanRepository $planRepository,
        TransactionRepository $transactionRepository
    ) {
        $this->userRepository = $userRepository;
        $this->planRepository = $planRepository;
        $this->transactionRepository = $transactionRepository;
    }

    public function getAction($id)
    {
        $affiliated = $this->userRepository->find($id);
        $affiliated->load('address');

        /** @var Collection $sponsors */
        $sponsors = $this->userRepository->findWhereNotIn('id', [$id]);
        $sponsorsList = [];

        foreach ($sponsors as $sponsor) {
            $sponsorsList[$sponsor->id] = $sponsor->id . ' - ' . $sponsor->name;
        }

        $planList = $this->planRepository->all()->lists('name', 'id');

        return view(
            'admin.affiliated.edit',
            [
                'affiliated' => $affiliated,
                'sponsorsList' => $sponsorsList,
                'planList' => $planList
            ]
        );
    }

    public function postAction(EditRequest $request, $userId)
    {
        $date = new Carbon();
        $requestData = $request->all();

        $this->userRepository->update(
            [
                'sponsor_id' => $requestData['sponsor_id'],
                'plan_id' => $requestData['plan_id'],
            ],
            $userId
        );

        if ($requestData['value_received_plan']) {
            $this->transactionRepository->create([
                'user_id' => $userId,
                'transaction_subtype_id' => TransactionSubTypeNamePickList::PLAN_CHANGE,
                'transaction_status_id' => TransactionStatusNamePickList::PAID,
                'transaction_scope_id' => TransactionScopeNamePickList::SCOPE_GLOBAL,
                'description' => 'Mudança de plano',
                'value' => $requestData['value_received_plan'],
                'paid_at' => $date->toDateString()
            ]);
        }

        $request->session()->flash('alert-success', 'Dados atualizados com sucesso');

        return redirect()->route('admin.affiliated');
    }
}
