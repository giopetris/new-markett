<?php

namespace NewMarkett\Http\Controllers\Admin\Classified;

use NewMarkett\Http\Controllers\Controller;
use NewMarkett\PickLists\User\RoleNamePickList;
use NewMarkett\Repositories\Classified\ClassifiedRepository;
use NewMarkett\Repositories\User\UserRepository;

class ListController extends Controller
{
    /**
     * @var ClassifiedRepository
     */
    private $classifiedRepository;

    /**
     * @param ClassifiedRepository $classifiedRepository
     */
    public function __construct(ClassifiedRepository $classifiedRepository)
    {
        $this->classifiedRepository = $classifiedRepository;
    }

    public function getAction()
    {
        $classifieds = $this->classifiedRepository->scopeQuery(function($query) {
            return $query->orderBy('updated_at', 'desc');
        })->all();

        return view(
            'admin.classified.list',
            [
                'classifieds' => $classifieds
            ]
        );
    }
}
