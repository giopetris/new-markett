<?php

namespace NewMarkett\Http\Controllers\Admin\Classified;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use NewMarkett\Http\Controllers\Controller;
use NewMarkett\Repositories\Classified\ClassifiedRepository;
use NewMarkett\Repositories\Classified\ClassifiedStatusRepository;

class EditController extends Controller
{
    /**
     * @var ClassifiedRepository
     */
    private $classifiedRepository;

    /**
     * @var ClassifiedStatusRepository
     */
    private $classifiedStatusRepository;

    /**
     * @param ClassifiedRepository $classifiedRepository
     * @param ClassifiedStatusRepository $classifiedStatusRepository
     */
    public function __construct(ClassifiedRepository $classifiedRepository, ClassifiedStatusRepository $classifiedStatusRepository)
    {
        $this->classifiedRepository = $classifiedRepository;
        $this->classifiedStatusRepository = $classifiedStatusRepository;
    }

    public function getAction($id)
    {
        $classified = $this->classifiedRepository->find($id);
        $classified->load('address');

        $statusList = $this->classifiedStatusRepository->all()->lists('name', 'id');

        return view(
            'admin.classified.edit',
            [
                'classified' => $classified,
                'statusList' => $statusList
            ]
        );
    }

    public function postAction(Request $request, $id)
    {
        $this->classifiedRepository->update(['classified_status_id' => $request->get('classified_status_id')], $id);

        $request->session()->flash('alert-success', 'Classificado atualizado com sucesso');

        return redirect()->route('admin.classified');
    }
}
