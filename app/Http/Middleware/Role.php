<?php

namespace NewMarkett\Http\Middleware;

use NewMarkett\Repositories\User\RoleRepository;

class Role
{
    private $roleRepository;

    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    /**
     * {@inheritDoc}
     */
    public function handle($request, \Closure $next, $roleId)
    {
        $role = $this->roleRepository->find($roleId);

        if (! $request->user()->hasRole($role)) {
            die('Acesso negado');
            abort(403);
        }

        return $next($request);
    }
}
