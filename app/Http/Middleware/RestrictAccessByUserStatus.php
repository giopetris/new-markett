<?php

namespace NewMarkett\Http\Middleware;

use Carbon\Carbon;
use NewMarkett\Entities\User\User;
use NewMarkett\Specifications\User\UserIsCompliantWithMonthlyFeeSpecification;
use NewMarkett\Specifications\User\UserShouldPayQuotaSpecification;

class RestrictAccessByUserStatus
{
    /**
     * @var UserShouldPayQuotaSpecification
     */
    private $userShouldPayQuotaSpecification;

    /**
     * @var UserIsCompliantWithMonthlyFeeSpecification
     */
    private $userIsCompliantWithMonthlyFeeSpecification;

    /**
     * @param UserShouldPayQuotaSpecification $userShouldPayQuotaSpecification
     * @param UserIsCompliantWithMonthlyFeeSpecification $userIsCompliantWithMonthlyFeeSpecification
     */
    public function __construct(
        UserShouldPayQuotaSpecification $userShouldPayQuotaSpecification,
        UserIsCompliantWithMonthlyFeeSpecification $userIsCompliantWithMonthlyFeeSpecification
    ) {
        $this->userShouldPayQuotaSpecification = $userShouldPayQuotaSpecification;
        $this->userIsCompliantWithMonthlyFeeSpecification = $userIsCompliantWithMonthlyFeeSpecification;
    }

    /**
     * {@inheritDoc}
     */
    public function handle($request, \Closure $next)
    {
        /** @var User $user */
        $user = $request->user();

        if ($this->userShouldPayQuotaSpecification->isSatisfiedBy($user)) {
            return redirect()->route('app.my-account.quota-payment');
        }

        if ( ! $this->userIsCompliantWithMonthlyFeeSpecification->isSatisfiedBy($user)) {
            return redirect()->route('app.my-account.monthly-fee-payment');
        }

        return $next($request);
    }
}
