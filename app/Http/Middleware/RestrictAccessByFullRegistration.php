<?php

namespace NewMarkett\Http\Middleware;

use NewMarkett\Entities\User\User;
use NewMarkett\Specifications\User\UserIsFullRegistrationSpecification;

class RestrictAccessByFullRegistration
{
    /**
     * @var UserIsFullRegistrationSpecification
     */
    private $userIsFullRegistrationSpecification;

    /**
     * @param UserIsFullRegistrationSpecification $userIsFullRegistrationSpecification
     */
    public function __construct(UserIsFullRegistrationSpecification $userIsFullRegistrationSpecification)
    {
        $this->userIsFullRegistrationSpecification = $userIsFullRegistrationSpecification;
    }

    /**
     * {@inheritDoc}
     */
    public function handle($request, \Closure $next)
    {
        /** @var User $user */
        $user = $request->user();

        if ( ! $this->userIsFullRegistrationSpecification->isSatisfiedBy($user)) {
            return redirect()->route('app.my-account');
        }

        return $next($request);
    }
}
