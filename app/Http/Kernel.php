<?php

namespace NewMarkett\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \NewMarkett\Http\Middleware\EncryptCookies::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        \NewMarkett\Http\Middleware\VerifyCsrfToken::class,
        \NewMarkett\Http\Middleware\GlobalMiddleware::class
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \NewMarkett\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'guest' => \NewMarkett\Http\Middleware\RedirectIfAuthenticated::class,
        'role' => \NewMarkett\Http\Middleware\Role::class,
        'restrict_access_by_user_status' => \NewMarkett\Http\Middleware\RestrictAccessByUserStatus::class,
        'restrict_access_by_full_registration' => \NewMarkett\Http\Middleware\RestrictAccessByFullRegistration::class,
    ];
}
