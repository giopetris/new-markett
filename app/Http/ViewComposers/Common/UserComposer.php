<?php

namespace NewMarkett\Http\ViewComposers\Common;

use Illuminate\Contracts\View\View;

class UserComposer
{
    public function compose(View $view)
    {
        if (\Auth::check()) {
            $view->with('user', \Auth::user());
        }
    }
}
