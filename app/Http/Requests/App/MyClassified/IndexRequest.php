<?php

namespace NewMarkett\Http\Requests\App\MyClassified;

use NewMarkett\DataTransformers\Address\Cep\CepToStringDataTransformer;
use NewMarkett\DataTransformers\Phone\PhoneToStringDataTransformer;
use NewMarkett\Http\Requests\Request;

class IndexRequest extends Request
{
    /**
     * @return array
     */
    public function messages()
    {
        return [
            'phone.required_if' => 'Informe o telefone ou o celular',
            'logo.required_if' => 'Anexe a logo',
            'site.url' => 'O formato do site é inválido. Se atente em colocar o http ou https na URL',
            'link_facebook.url' => 'O formato do link é inválido. Se atente em colocar o http ou https na URL'
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function authorize()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            'logo' => 'required_if:id,|max:1000',
            'title' => 'required|max:55',
            'address.cep' => 'required',
            'address.street' => 'required',
            'address.state' => 'required',
            'address.city_id' => 'required',
            'phone' => 'required_if:cellular,',
            'site' => 'url',
            'link_facebook' => 'url'
        ];
    }

    public function all()
    {
        $request = parent::all();

        $cepToStringDataTransformer = new CepToStringDataTransformer();
        $phoneToStringDataTransformer = new PhoneToStringDataTransformer();

        $request['address']['number'] = $request['address']['number'] == '' ? null : $request['address']['number'];
        $request['address']['cep'] = $request['address']['cep'] == '' ? null : $cepToStringDataTransformer->transform($request['address']['cep']);
        $request['phone'] = $request['phone'] == '' ? null : $phoneToStringDataTransformer->transform($request['phone']);
        $request['cellular'] = $request['cellular'] == '' ? null : $phoneToStringDataTransformer->transform($request['cellular']);

        return $request;
    }
}
