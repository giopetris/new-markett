<?php

namespace NewMarkett\Http\Requests\App\MyAccount;

use NewMarkett\DataTransformers\Address\Cep\CepToStringDataTransformer;
use NewMarkett\DataTransformers\PersonalData\Cpf\CpfToStringDataTransformer;
use NewMarkett\DataTransformers\Phone\PhoneToStringDataTransformer;
use NewMarkett\Http\Requests\Request;

class IndexRequest extends Request
{
    /**
     * {@inheritdoc}
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'name.min_words' => 'Informe o nome completo',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            'name' => 'required|max:55|min_words:2',
            'cpf' => 'required',
            'email' => 'required|email|max:55',
            'cellular' => 'required',
            'address.cep' => 'required',
            'address.street' => 'required',
            'address.number' => 'required',
            'address.district' => 'required',
            'address.state' => 'required',
            'address.city_id' => 'required',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function all()
    {
        $request = parent::all();

        $cpfToStringDataTransformer = new CpfToStringDataTransformer();
        $phoneToStringDataTransformer = new PhoneToStringDataTransformer();
        $cepToStringDataTransformer = new CepToStringDataTransformer();

        $request['cpf'] = $request['cpf'] == '' ? null : $cpfToStringDataTransformer->transform($request['cpf']);
        $request['phone'] = $request['phone'] == '' ? null : $phoneToStringDataTransformer->transform($request['phone']);
        $request['cellular'] = $request['cellular'] == '' ? null : $phoneToStringDataTransformer->transform($request['cellular']);
        $request['address']['city_id'] = $request['address']['city_id'] == '' ? null : $request['address']['city_id'];
        $request['address']['number'] = $request['address']['number'] == '' ? null : $request['address']['number'];
        $request['address']['cep'] = $request['address']['cep'] == '' ? null : $cepToStringDataTransformer->transform($request['address']['cep']);

        return $request;
    }
}
