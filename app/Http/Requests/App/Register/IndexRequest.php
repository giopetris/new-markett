<?php

namespace NewMarkett\Http\Requests\App\Register;

use NewMarkett\DataTransformers\PersonalData\Cpf\CpfToStringDataTransformer;
use NewMarkett\DataTransformers\Phone\PhoneToStringDataTransformer;
use NewMarkett\Http\Requests\Request;

class IndexRequest extends Request
{
    /**
     * @return array
     */
    public function messages()
    {
        return [
            'sponsor_id.exists' => 'Não há nenhum usuário cadastrado com esse ID',
            'name.min_words' => 'Informe o nome completo'
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function authorize()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            'sponsor_id' => 'required|exists:user,id',
            'name' => 'required|max:55|min_words:2',
            'cpf' => 'required',
            'cellular' => 'required',
            'plan_id' => 'required|exists:plan,id',
            'email' => 'required|email|max:55',
            'password' => 'required|confirmed|min:6|max:30',
            'password_confirmation' => 'required',
            'terms_use' => 'required',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function all()
    {
        $request = parent::all();

        $cpfToStringDataTransformer = new CpfToStringDataTransformer();
        $phoneToStringDataTransaformer = new PhoneToStringDataTransformer();

        $request['cpf'] = $request['cpf'] == '' ? null : $cpfToStringDataTransformer->transform($request['cpf']);
        $request['phone'] = $request['phone'] == '' ? '' : $phoneToStringDataTransaformer->transform($request['phone']);
        $request['cellular'] = $request['cellular'] == '' ? '' : $phoneToStringDataTransaformer->transform($request['cellular']);

        return $request;
    }
}
