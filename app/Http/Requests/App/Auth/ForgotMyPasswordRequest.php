<?php

namespace NewMarkett\Http\Requests\App\Auth;

use NewMarkett\Http\Requests\Request;

class ForgotMyPasswordRequest extends Request
{
    /**
     * @return array
     */
    public function messages()
    {
        return [
            'id.exists' => 'Não há nenhum usuário cadastrado com esse ID',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function authorize()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            'id' => 'required|exists:user,id',
        ];
    }
}
