<?php

namespace NewMarkett\Http\Requests\App\Auth;

use NewMarkett\Http\Requests\Request;

class ResetPasswordRequest extends Request
{
    /**
     * {@inheritdoc}
     */
    public function authorize()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            'password' => 'required|confirmed|min:6|max:30',
            'password_confirmation' => 'required'
        ];
    }
}
