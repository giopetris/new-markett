<?php

namespace NewMarkett\Http\Requests\App\Loot;

use NewMarkett\DataTransformers\Money\MoneyToFloatDataTransformer;
use NewMarkett\Http\Requests\Request;

class LootRequest extends Request
{
    /**
     * {@inheritdoc}
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'value.min' => 'O valor mínimo para saque é de R$ 100,00',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            'value' => 'required|numeric|min:100',
            'password' => 'required',
        ];
    }

    public function all()
    {
        $request = parent::all();

        $moneyToFloatDataTransformer = new MoneyToFloatDataTransformer();

        $request['value'] = $request['value_mask'] == '' ? null : $moneyToFloatDataTransformer->transform($request['value_mask']);

        return $request;
    }
}
