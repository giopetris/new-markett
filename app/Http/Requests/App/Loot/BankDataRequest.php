<?php

namespace NewMarkett\Http\Requests\App\Loot;

use NewMarkett\DataTransformers\PersonalData\Cpf\CpfToStringDataTransformer;
use NewMarkett\Http\Requests\Request;

class BankDataRequest extends Request
{
    /**
     * {@inheritdoc}
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'holder_name.required_if' => 'Campo obrigatório',
            'holder_cpf.required_if' => 'Campo obrigatório'
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            'bank_id' => 'required',
            'account_type_id' => 'required',
            'agency' => 'required',
            'account_number' => 'required',
            'holder_name' => 'required_if:not_account_holder,1',
            'holder_cpf' => 'required_if:not_account_holder,1',
        ];
    }

    public function all()
    {
        $request = parent::all();

        $cpfToStringDataTransformer = new CpfToStringDataTransformer();

        $request['not_account_holder'] = isset($request['not_account_holder']);

        if ($request['not_account_holder'] == false) {
            $request['holder_name'] = '';
            $request['holder_cpf'] = '';
        } else {
            $request['holder_cpf'] = $request['holder_cpf'] == '' ? null : $cpfToStringDataTransformer->transform($request['holder_cpf']);
        }

        return $request;
    }
}
