<?php

namespace NewMarkett\Http\Requests\App\MyNetwork;

use NewMarkett\Http\Requests\Request;

class NewAffiliateRequest extends Request
{
    /**
     * {@inheritdoc}
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'name.min_words' => 'Informe o nome completo',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            'name' => 'required|max:55|min_words:2',
            'email' => 'required|email|max:55',
        ];
    }
}
