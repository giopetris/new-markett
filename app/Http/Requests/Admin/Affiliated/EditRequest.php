<?php

namespace NewMarkett\Http\Requests\Admin\Affiliated;

use NewMarkett\DataTransformers\Money\MoneyToFloatDataTransformer;
use NewMarkett\Http\Requests\Request;

class EditRequest extends Request
{
    /**
     * {@inheritdoc}
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function all()
    {
        $request = parent::all();

        $moneyToFloatDataTransformer = new MoneyToFloatDataTransformer();

        $request['value_received_plan'] = $request['value_received_plan'] == '' ?
            null : $moneyToFloatDataTransformer->transform($request['value_received_plan']);

        return $request;
    }
}
