<?php

namespace NewMarkett\Http\Requests\Admin\MyAccount;

use NewMarkett\DataTransformers\Phone\PhoneToStringDataTransformer;
use NewMarkett\Http\Requests\Request;

class IndexRequest extends Request
{
    /**
     * {@inheritdoc}
     */
    public function authorize()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            'name' => 'required|max:55',
            'email' => 'required|email|max:55',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function all()
    {
        $request = parent::all();

        $phoneToStringDataTransformer = new PhoneToStringDataTransformer();

        $request['phone'] = $request['phone'] == '' ? null : $phoneToStringDataTransformer->transform($request['phone']);
        $request['cellular'] = $request['cellular'] == '' ? null : $phoneToStringDataTransformer->transform($request['cellular']);

        return $request;
    }
}
