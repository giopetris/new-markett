<?php

namespace NewMarkett\Http\Requests\Admin\Financial\BillToReceive;

use NewMarkett\DataTransformers\Date\StringToDateDataTransformer;
use NewMarkett\Http\Requests\Request;
use NewMarkett\PickLists\Financial\TransactionStatusNamePickList;

class EditRequest extends Request
{
    /**
     * {@inheritdoc}
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'paid_at.required_if' => 'Campo obrigatório',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            'transaction_status_id' => 'required',
            'paid_at' => 'required_if:transaction_status_id,' . TransactionStatusNamePickList::PAID,
        ];
    }

    public function all()
    {
        $request = parent::all();

        $request['paid_at'] = $request['transaction_status_id'] == TransactionStatusNamePickList::PAID ?
            $request['paid_at'] : null;

        if ($request['paid_at']) {
            $stringToDateDataTransformer = new StringToDateDataTransformer();

            $request['paid_at'] = $stringToDateDataTransformer->transform($request['paid_at']);
        }

        return $request;
    }
}
