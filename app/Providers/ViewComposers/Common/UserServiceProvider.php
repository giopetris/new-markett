<?php

namespace NewMarkett\Providers\ViewComposers\Common;

use Illuminate\Support\ServiceProvider;
use NewMarkett\Http\ViewComposers\Common\UserComposer;

class UserServiceProvider extends ServiceProvider
{
    public function boot()
    {
        view()->composer(
            ['app.*', 'admin.*'],
            UserComposer::class
        );
    }

    public function register()
    {

    }
}
