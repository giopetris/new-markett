<?php

namespace NewMarkett\Repositories\Classified;

use NewMarkett\Entities\Classified\ClassifiedStatus;
use NewMarkett\Repositories\Base\BaseRepository;

class ClassifiedStatusRepository extends BaseRepository
{
    public function model()
    {
        return ClassifiedStatus::class;
    }
}
