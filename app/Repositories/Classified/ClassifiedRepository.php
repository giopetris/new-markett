<?php

namespace NewMarkett\Repositories\Classified;

use NewMarkett\Entities\Classified\Classified;
use NewMarkett\Repositories\Base\BaseRepository;

class ClassifiedRepository extends BaseRepository
{
    public function model()
    {
        return Classified::class;
    }
}
