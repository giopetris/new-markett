<?php

namespace NewMarkett\Repositories\Address;

use NewMarkett\Repositories\Base\BaseRepository;
use NewMarkett\Entities\Address\Address;

class AddressRepository extends BaseRepository
{
    public function model()
    {
        return Address::class;
    }
}
