<?php

namespace NewMarkett\Repositories\Base;

use Prettus\Repository\Eloquent\BaseRepository as EloquentBaseRepository;

abstract class BaseRepository extends EloquentBaseRepository
{
    public function deleteAll()
    {
        \DB::table($this->model->getTable())->delete();
    }
}
