<?php

namespace NewMarkett\Repositories\Bank;

use NewMarkett\Entities\Bank\BankData;
use NewMarkett\Repositories\Base\BaseRepository;

class BankDataRepository extends BaseRepository
{
    public function model()
    {
        return BankData::class;
    }
}
