<?php

namespace NewMarkett\Repositories\Bank;

use NewMarkett\Repositories\Base\BaseRepository;
use NewMarkett\Entities\Bank\AccountType;

class AccountTypeRepository extends BaseRepository
{
    public function model()
    {
        return AccountType::class;
    }
}
