<?php

namespace NewMarkett\Repositories\Bank;

use NewMarkett\Repositories\Base\BaseRepository;
use NewMarkett\Entities\Bank\Bank;

class BankRepository extends BaseRepository
{
    public function model()
    {
        return Bank::class;
    }
}
