<?php

namespace NewMarkett\Repositories\Financial;

use NewMarkett\Entities\Financial\TransactionSubType;
use NewMarkett\Repositories\Base\BaseRepository;

class TransactionSubTypeRepository extends BaseRepository
{
    public function model()
    {
        return TransactionSubType::class;
    }
}
