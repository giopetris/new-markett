<?php

namespace NewMarkett\Repositories\Financial;

use NewMarkett\Entities\Financial\PaymentForm;
use NewMarkett\Repositories\Base\BaseRepository;

class PaymentFormRepository extends BaseRepository
{
    public function model()
    {
        return PaymentForm::class;
    }
}
