<?php

namespace NewMarkett\Repositories\Financial;

use NewMarkett\Repositories\Base\BaseRepository;
use NewMarkett\Entities\Financial\TransactionScope;

class TransactionScopeRepository extends BaseRepository
{
    public function model()
    {
        return TransactionScope::class;
    }
}
