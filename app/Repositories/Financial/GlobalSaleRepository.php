<?php

namespace NewMarkett\Repositories\Financial;

use Carbon\Carbon;
use NewMarkett\Entities\Financial\GlobalSale;
use NewMarkett\Repositories\Base\BaseRepository;

class GlobalSaleRepository extends BaseRepository
{
    public function model()
    {
        return GlobalSale::class;
    }

    public function findGlobalSalesToDivide()
    {
        $date = new Carbon();

        return $this->findWhere([
            'divided_at' => null,
            ['date', '<', $date->toDateString()]
        ]);
    }
}
