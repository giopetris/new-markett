<?php

namespace NewMarkett\Repositories\Financial;

use NewMarkett\Entities\Financial\TransactionType;
use NewMarkett\Repositories\Base\BaseRepository;

class TransactionTypeRepository extends BaseRepository
{
    public function model()
    {
        return TransactionType::class;
    }
}
