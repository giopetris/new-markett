<?php

namespace NewMarkett\Repositories\Financial;

use NewMarkett\Entities\Financial\TransactionStatus;
use NewMarkett\Repositories\Base\BaseRepository;

class TransactionStatusRepository extends BaseRepository
{
    public function model()
    {
        return TransactionStatus::class;
    }
}
