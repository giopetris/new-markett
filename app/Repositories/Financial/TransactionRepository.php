<?php

namespace NewMarkett\Repositories\Financial;

use NewMarkett\Entities\Financial\Transaction;
use NewMarkett\Entities\Financial\TransactionScope;
use NewMarkett\Entities\Financial\TransactionStatus;
use NewMarkett\Entities\Financial\TransactionType;
use NewMarkett\Entities\User\User;
use NewMarkett\PickLists\Financial\TransactionSubTypeNamePickList;
use NewMarkett\Repositories\Base\BaseRepository;

class TransactionRepository extends BaseRepository
{
    public function model()
    {
        return Transaction::class;
    }

    /**
     * @param TransactionType $transactionType
     * @param array $transactionStatusId
     * @param User $user
     * @param TransactionScope $transactionScope
     * @param array|null $paymentForms
     * @return float|int
     */
    public function getTotalValueByTransactionTypeTransactionStatusUserAndScope(
        TransactionType $transactionType,
        array $transactionStatusId,
        User $user,
        TransactionScope $transactionScope,
        array $paymentForms = null
    ) {
        $builder = \DB::table($this->model->getTable())
            ->join('transaction_sub_type', 'transaction.transaction_subtype_id', '=', 'transaction_sub_type.id')
            ->where('transaction_sub_type.transaction_type_id', '=', $transactionType->id)
            ->where('transaction.user_id', '=', $user->id)
            ->whereIn('transaction.transaction_status_id', $transactionStatusId)
            ->where('transaction.transaction_scope_id', $transactionScope->id)
            ->whereNull('transaction.deleted_at');

        if ($paymentForms !== null) {
            $builder = $builder->whereIn('transaction.payment_form_id', $paymentForms);
        }

        return $builder->sum('transaction.value');
    }

    public function getLastMonthlyFeePaymentOf(User $user)
    {
        $result = \DB::table($this->model->getTable())
            ->select('id')
            ->where('transaction.transaction_subtype_id', '=', TransactionSubTypeNamePickList::MONTHLY_FEE_PAYMENT)
            ->where('transaction.user_id', '=', $user->id)
            ->orderBy('transaction.created_at', 'DESC')
            ->limit(1)
            ->first();

        if ( ! $result) {
            return [];
        }

        return $this->find($result->id);
    }
}
