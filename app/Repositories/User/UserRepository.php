<?php

namespace NewMarkett\Repositories\User;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use NewMarkett\Entities\User\Plan;
use NewMarkett\Entities\User\User;
use NewMarkett\PickLists\User\RoleNamePickList;
use NewMarkett\PickLists\User\UserStatusNamePickList;
use NewMarkett\Repositories\Base\BaseRepository;
use Illuminate\Container\Container as Application;
use NewMarkett\Services\User\GetStatusService;
use NewMarkett\Specifications\User\UserCanReceivePaymentsSpecification;

class UserRepository extends BaseRepository
{
    /**
     * @var UserCanReceivePaymentsSpecification
     */
    private $userCanReceivePaymentsSpecification;

    /**
     * @var GetStatusService
     */
    private $getStatusService;

    /**
     * @param Application $app
     * @param UserCanReceivePaymentsSpecification $userCanReceivePaymentsSpecification
     * @param GetStatusService $getStatusService
     */
    public function __construct(
        Application $app,
        UserCanReceivePaymentsSpecification $userCanReceivePaymentsSpecification,
        GetStatusService $getStatusService
    ) {
        parent::__construct($app);
        $this->userCanReceivePaymentsSpecification = $userCanReceivePaymentsSpecification;
        $this->getStatusService = $getStatusService;
    }

    /**
     * @return mixed
     */
    public function model()
    {
        return User::class;
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function findBySponsor(User $user)
    {
        return $this->findByField('sponsor_id', $user->id);
    }

    /**
     * @param Plan $plan
     * @return array
     */
    public function findAffiliatesValidToParticipateOfShareGlobalBelongingToPlan(Plan $plan)
    {
        $affiliates = $this->findWhere(['plan_id' => $plan->id])->all();
        $userCanReceivePaymentsSpecification = $this->userCanReceivePaymentsSpecification;

        return array_filter(
            $affiliates,
            function($affiliate) use ($userCanReceivePaymentsSpecification) {
                return $userCanReceivePaymentsSpecification->isSatisfiedBy($affiliate);
            }
        );
    }

    /**
     * @return Collection
     */
    public function findAffiliatesValidToParticipateOfShareGlobal()
    {
        /** @var Collection $affiliates */
        $affiliates = $this->all();
        $userCanReceivePaymentsSpecification = $this->userCanReceivePaymentsSpecification;

        return $affiliates->filter(
            function($affiliate) use ($userCanReceivePaymentsSpecification) {
                return $userCanReceivePaymentsSpecification->isSatisfiedBy($affiliate);
            }
        );
    }

    /**
     * @return array
     */
    public function findActiveAffiliates()
    {
        $date = new Carbon();

        /** @var Collection $result */
        $result = $this->findWhere([
            'role_id' => RoleNamePickList::AFFILIATED,
            ['plan_expiration_date', '>=', $date->toDateString()],
            'is_inactive' => false
        ]);

        $getStatusService = $this->getStatusService;

        return $result->filter(function($affiliate) use ($getStatusService) {
            return $getStatusService->getStatusIdOf($affiliate) == UserStatusNamePickList::ACTIVE;
        });
    }

    public function findAffiliates($filter)
    {
        /** @var Collection $affiliates */
        $affiliates = $this->scopeQuery(function($query) {
            return $query->orderBy('id', 'desc');
        })->findByField('role_id', RoleNamePickList::AFFILIATED);

        if (isset($filter['status_id']) && $filter['status_id'] != 0) {
            $statusId = $filter['status_id'];

            $getStatusService = $this->getStatusService;
            $affiliates = $affiliates->filter(function($affiliate) use ($getStatusService, $statusId) {
                return $getStatusService->getStatusIdOf($affiliate) == $statusId;
            });
        }

        return $affiliates;
    }

    /**
     * @return mixed
     */
    public function findAffiliatesWithExpiredPlan()
    {
        $date = new Carbon();

        return $this->findWhere([
            ['plan_expiration_date', '!=', '0000-00-00'],
            ['plan_expiration_date', '<', $date->toDateString()],
            'role_id' => RoleNamePickList::AFFILIATED,
            'is_inactive' => false
        ]);
    }
}
