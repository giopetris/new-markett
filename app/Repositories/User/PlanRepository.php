<?php

namespace NewMarkett\Repositories\User;

use NewMarkett\Entities\User\Plan;
use NewMarkett\Repositories\Base\BaseRepository;

class PlanRepository extends BaseRepository
{
    public function model()
    {
        return Plan::class;
    }
}
