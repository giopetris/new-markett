<?php

namespace NewMarkett\Repositories\User;

use NewMarkett\Entities\User\Role;
use NewMarkett\Repositories\Base\BaseRepository;

class RoleRepository extends BaseRepository
{
    public function model()
    {
        return Role::class;
    }
}
