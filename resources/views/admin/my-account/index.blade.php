@extends('admin.layout.base.index')

@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1>Minha conta</h1>
                </div>
            </div>
        </div>

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        @include('admin.common.flash-message')
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-green-sharp bold uppercase">
                                        Meus dados
                                    </span>
                                </div>
                            </div>

                            <div class="portlet-body">
                                {!! Form::model($user, ['class' => 'form-horizontal']) !!}

                                <div class="form-group">
                                    {!! Form::label('id', 'Meu ID', ['class' => 'col-md-2 control-label']) !!}
                                    <div class="col-md-10">
                                        {!! Form::text(
                                            'id',
                                            null,
                                            ['class' => 'form-control', 'disabled' => 'disabled']
                                        ) !!}
                                        <span class="text-danger">
                                            {{ $errors->first('id') }}
                                        </span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('name', 'Nome', ['class' => 'col-md-2 control-label']) !!}
                                    <div class="col-md-10">
                                        {!! Form::text('name', null, ['class' => 'form-control']) !!}
                                        <span class="text-danger">
                                            {{ $errors->first('name') }}
                                        </span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('email', 'E-mail', ['class' => 'col-md-2 control-label']) !!}
                                    <div class="col-md-10">
                                        {!! Form::text('email', null, ['class' => 'form-control']) !!}
                                        <span class="text-danger">
                                            {{ $errors->first('email') }}
                                        </span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-offset-2 col-md-10">
                                        <a href="{{ route('admin.my-account.change-password') }}">Alterar senha</a>
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('phone', 'Telefone', ['class' => 'col-md-2 control-label']) !!}
                                    <div class="col-md-10">
                                        {!! Form::text(
                                            'phone',
                                            $user->present()->phone,
                                            ['class' => 'form-control mask-phone-number']
                                        ) !!}
                                        <span class="text-danger">
                                            {{ $errors->first('phone') }}
                                        </span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('cellular', 'Celular', ['class' => 'col-md-2 control-label']) !!}
                                    <div class="col-md-10">
                                        {!! Form::text(
                                            'cellular',
                                            $user->present()->cellular,
                                            ['class' => 'form-control mask-phone-number']
                                        ) !!}
                                        <span class="text-danger">
                                            {{ $errors->first('cellular') }}
                                        </span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-offset-2 col-md-10">
                                        {!! Form::submit('Salvar', ['class' => 'btn blue']) !!}
                                    </div>
                                </div>

                                {!! Form::close() !!}
                            </div>
                        </div>
                        <!-- END SAMPLE FORM PORTLET-->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
