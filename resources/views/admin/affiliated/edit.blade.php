<?php
$menuAtivo = 'affiliates';
?>

@extends('admin.layout.base.index')

@section('javascript')
    @parent

    {!! Html::script('assets/js/admin/affiliate/handlePlanField.js') !!}
    {!! Html::script('assets/lib/artesaos/cidades/js/scripts.js') !!}

    <script type="text/javascript">
        $(function() {
            $('#uf').ufs({
                onChange: function(uf){
                    $('#cidade').cidades({uf: uf});
                }
            });
        });
    </script>
@endsection

@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1>Editar afiliado</h1>
                </div>
            </div>
        </div>

        <div class="page-content">
            <div class="container">
                <div class="row ">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-green-sharp bold uppercase">
                                        Dados do afiliado
                                    </span>
                                </div>

                                <div class="tools">
                                    <a href="{{ URL::previous() }}">Voltar</a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                {!! Form::model($affiliated, ['class' => 'form-horizontal']) !!}
                                    <div class="form-group">
                                        {!! Form::label('plan_id', 'Plano', ['class' => 'col-md-2 control-label']) !!}
                                        <div class="col-md-10">
                                            {!! Form::select(
                                                'plan_id',
                                                $planList,
                                                $affiliated->plan->id,
                                                ['class' => 'form-control', 'id' => 'select-plan']
                                            ) !!}
                                        </div>
                                    </div>

                                    <div class="form-group" id="wrapper-value-received-plan" style="display: none;">
                                        {!! Form::label('value_received_plan', 'Valor recebido', ['class' => 'col-md-2 control-label']) !!}
                                        <div class="col-md-10">
                                            {!! Form::text(
                                                'value_received_plan',
                                                null,
                                                ['class' => 'form-control mask-money']
                                            ) !!}
                                            <span class="help-block">
                                                Informe o valor recebido do usuário para o sistema lançar como uma conta
                                                recebida. Caso não foi cobrado nenhum valor pela mudança do plano,
                                                apenas deixe esse campo em branco.
                                            </span>
                                        </div>
                                    </div>

                                    <?php
                                    $optionsSponsorField = ['class' => 'form-control'];

                                    if ( ! $affiliated->sponsorIsAdmin()) {
                                        $optionsSponsorField['disabled'] = 'disabled';
                                    }
                                    ?>

                                    <div class="form-group">
                                        {!! Form::label('sponsor_id', 'Patrocinador', ['class' => 'col-md-2 control-label']) !!}
                                        <div class="col-md-10">
                                            {!! Form::select(
                                                'sponsor_id',
                                                $sponsorsList,
                                                $affiliated->sponsor->id,
                                                $optionsSponsorField
                                            ) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('id', 'ID do afiliado', ['class' => 'col-md-2 control-label']) !!}
                                        <div class="col-md-10">
                                            {!! Form::text(
                                                'id',
                                                null,
                                                ['class' => 'form-control', 'readonly' => 'readonly']
                                            ) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('name', 'Nome', ['class' => 'col-md-2 control-label']) !!}
                                        <div class="col-md-10">
                                            {!! Form::text(
                                                'name',
                                                null,
                                                ['class' => 'form-control', 'readonly' => 'readonly']
                                            ) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('cpf', 'CPF', ['class' => 'col-md-2 control-label']) !!}
                                        <div class="col-md-10">
                                            {!! Form::text(
                                                'cpf',
                                                $affiliated->present()->cpf,
                                                ['class' => 'form-control', 'readonly' => 'readonly']
                                            ) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('email', 'E-mail', ['class' => 'col-md-2 control-label']) !!}
                                        <div class="col-md-10">
                                            {!! Form::text(
                                                'email',
                                                null,
                                                ['class' => 'form-control', 'readonly' => 'readonly']
                                            ) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('phone', 'Telefone', ['class' => 'col-md-2 control-label']) !!}
                                        <div class="col-md-10">
                                            {!! Form::text(
                                                'phone',
                                                $affiliated->present()->phone,
                                                ['class' => 'form-control', 'readonly' => 'readonly']
                                            ) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('cellular', 'Celular', ['class' => 'col-md-2 control-label']) !!}
                                        <div class="col-md-10">
                                            {!! Form::text(
                                                'cellular',
                                                $affiliated->present()->cellular,
                                                ['class' => 'form-control', 'readonly' => 'readonly']
                                            ) !!}
                                        </div>
                                    </div>

                                    <h4>Endereço</h4>
                                    <hr>

                                    <div class="form-group">
                                        {!! Form::label('address[cep]', 'CEP', ['class' => 'col-md-2 control-label']) !!}
                                        <div class="col-md-10">
                                            {!! Form::text(
                                                'address[cep]',
                                                null,
                                                ['class' => 'form-control mask-zip-code', 'readonly' => 'readonly']
                                            ) !!}
                                            <span class="text-danger">
                                                {{ $errors->first('address[cep]') }}
                                            </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('address[street]', 'Endereço', ['class' => 'col-md-2 control-label']) !!}
                                        <div class="col-md-10">
                                            {!! Form::text(
                                                'address[street]',
                                                null,
                                                ['class' => 'form-control', 'readonly' => 'readonly']
                                            ) !!}
                                            <span class="text-danger">
                                                {{ $errors->first('address[street]') }}
                                            </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('address[number]', 'Número', ['class' => 'col-md-2 control-label']) !!}
                                        <div class="col-md-10">
                                            {!! Form::text(
                                                'address[number]',
                                                null,
                                                ['class' => 'form-control', 'readonly' => 'readonly']
                                            ) !!}
                                            <span class="text-danger">
                                                {{ $errors->first('address[number]') }}
                                            </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('address[district]', 'Bairro', ['class' => 'col-md-2 control-label']) !!}
                                        <div class="col-md-10">
                                            {!! Form::text(
                                                'address[district]',
                                                null,
                                                ['class' => 'form-control', 'readonly' => 'readonly']
                                            ) !!}
                                            <span class="text-danger">
                                                {{ $errors->first('address[district]') }}
                                            </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('address[complement]', 'Complemento', ['class' => 'col-md-2 control-label']) !!}
                                        <div class="col-md-10">
                                            {!! Form::text(
                                                'address[complement]',
                                                null,
                                                ['class' => 'form-control', 'readonly' => 'readonly']
                                            ) !!}
                                            <span class="text-danger">
                                                {{ $errors->first('address[complement]') }}
                                            </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('address[state]', 'Estado', ['class' => 'col-md-2 control-label']) !!}
                                        <div class="col-md-10">
                                            <?php
                                                $stateDefaultValue = old('address.state');
                                                if ($stateDefaultValue === null && $affiliated && $affiliated->address && $affiliated->address->city) {
                                                    $stateDefaultValue = $affiliated->address->city->uf;
                                                }
                                            ?>
                                            {!! Form::select(
                                                null,
                                                [],
                                                null,
                                                [
                                                    'id' => 'uf',
                                                    'default' => $stateDefaultValue,
                                                    'class' => 'form-control',
                                                    'disabled' => 'disabled'
                                                ]
                                            ) !!}
                                            <span class="text-danger">
                                                {{ $errors->first('address[state]') }}
                                            </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('address[city_id]', 'Cidade:', ['class' => 'col-md-2 control-label']) !!}
                                        <div class="col-md-10">
                                            <?php
                                                $cityDefaultValue = old('address.city_id');
                                                if ($cityDefaultValue === null && $affiliated && $affiliated->address && $affiliated->address->city) {
                                                    $cityDefaultValue = $affiliated->address->city->id;
                                                }
                                            ?>
                                            {!! Form::select(
                                                'address[city_id]',
                                                [],
                                                null,
                                                [
                                                    'id' => 'cidade',
                                                    'default' => $cityDefaultValue,
                                                    'class' => 'form-control',
                                                    'disabled' => 'disabled'
                                                ]
                                            ) !!}
                                            <span class="text-danger">
                                                {{ $errors->first('address[city_id]') }}
                                            </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-offset-2 col-md-10">
                                            {!! Form::submit('Salvar', ['class' => 'btn blue']) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                        <!-- END SAMPLE FORM PORTLET-->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
