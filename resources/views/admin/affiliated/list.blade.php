<?php
$menuAtivo = 'affiliates';
?>

@extends('admin.layout.base.index')

@section('stylesheet')
    @parent

    {!! Html::style('assets/theme/metronic/global/plugins/select2/select2.css') !!}
    {!! Html::style('assets/theme/metronic/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css') !!}
@endsection

@section('javascript')
    @parent

    {!! Html::script('assets/theme/metronic/global/plugins/select2/select2.min.js') !!}
    {!! Html::script('assets/theme/metronic/global/plugins/datatables/media/js/jquery.dataTables.min.js') !!}
    {!! Html::script('assets/theme/metronic/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') !!}


    {!! Html::script('assets/js/admin/affiliate/datatable.js') !!}
    <script>
        $(function() {
            TableAffiliate.init();
        });
    </script>
@endsection

@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1>Afiliados</h1>
                </div>
            </div>
        </div>

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        @include('admin.common.flash-message')
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-green-sharp bold uppercase">
                                        Filtro
                                    </span>
                                </div>
                            </div>

                            <div class="portlet-body">
                                {!! Form::open(['class' => '', 'method' => 'GET', 'route' => 'admin.affiliated']) !!}

                                <div class="row">
                                    <div class="col-md-5">
                                        {!! Form::label('status_id', 'Status') !!}
                                        {!! Form::select(
                                            'status_id',
                                            ['Todos'] + \NewMarkett\PickLists\User\UserStatusNamePickList::getData(),
                                            null,
                                            ['class' => 'form-control']
                                        ) !!}
                                    </div>

                                    <div class="col-md-1">
                                        {!! Form::submit(
                                            'Filtrar',
                                            [
                                                'class' => 'btn blue',
                                                'style' => 'margin-top: 23px',
                                                'name' => 'action',
                                                'value' => 'filter'
                                            ]
                                        ) !!}
                                    </div>

                                    <div class="col-md-1 pull-right">
                                        {!! Form::submit(
                                            'Download Excel',
                                            [
                                                'class' => 'btn pull-right',
                                                'style' => 'margin-top: 23px',
                                                'name' => 'action',
                                                'value' => 'download-excel'
                                            ]
                                        ) !!}
                                    </div>
                                </div>

                                {!! Form::close() !!}
                            </div>
                        </div>
                        <!-- END SAMPLE FORM PORTLET-->
                    </div>
                </div>

                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet light">
                            <div class="portlet-body">
                                <table class="table table-striped table-bordered table-hover" id="table-affiliate">
                                    <thead>
                                    <tr>
                                        <th>
                                            ID
                                        </th>
                                        <th style="min-width: 130px;">
                                            Patrocinador
                                        </th>
                                        <th style="min-width: 130px;">
                                            Afiliado
                                        </th>
                                        <th>
                                            E-mail
                                        </th>
                                        <th style="min-width: 100px">
                                            Telefones
                                        </th>
                                        <th style="width: 70px;">
                                            Plano
                                        </th>
                                        <th>
                                            Cidade
                                        </th>
                                        <th>
                                            Data afiliação
                                        </th>
                                        <th>
                                            Status
                                        </th>
                                        <th>
                                            Ações
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($affiliates as $affiliate)
                                        <?php
                                        $sponsor = $affiliate->sponsor
                                        ?>
                                        <tr>
                                            <td>{{ $affiliate->id }}</td>
                                            <td>
                                                {{ $sponsor->name }} <br />
                                                ID: {{ $sponsor->id }} <br />
                                                @if($sponsor->cpf)
                                                    CPF: {{ $sponsor->present()->cpf }}
                                                @endif
                                            </td>
                                            <td>
                                                {{ $affiliate->name }} <br />
                                                ID: {{ $affiliate->id }} <br />
                                                @if($affiliate->cpf)
                                                    CPF: {{ $affiliate->present()->cpf }}
                                                @endif
                                            </td>
                                            <td>{{ $affiliate->email }}</td>
                                            <td>{!! $affiliate->present()->phones(true) !!}</td>
                                            <td>{{ $affiliate->plan->name }} <br /> {{ '(' . $affiliate->plan->present()->percentageComissionGlobalValue . ')' }}</td>
                                            <td>{{ $affiliate->address && $affiliate->address->city ? $affiliate->address->city->nome . ' - ' .$affiliate->address->city->uf : '' }}</td>
                                            <td>{{ $affiliate->present()->createdAt }}</td>
                                            <td>{{ $getStatusUserService->getStatusNameOf($affiliate) }}</td>
                                            <td><a href="{{ route('admin.affiliated.edit', ['id' => $affiliate->id]) }}">Editar</a></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
    </div>
@endsection
