<?php
$menuAtivo = 'classified';
?>

@extends('admin.layout.base.index')

@section('stylesheet')
    @parent

    {!! Html::style('assets/theme/metronic/global/plugins/select2/select2.css') !!}
    {!! Html::style('assets/theme/metronic/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css') !!}
@endsection

@section('javascript')
    @parent

    {!! Html::script('assets/theme/metronic/global/plugins/select2/select2.min.js') !!}
    {!! Html::script('assets/theme/metronic/global/plugins/datatables/media/js/jquery.dataTables.min.js') !!}
    {!! Html::script('assets/theme/metronic/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') !!}


    {!! Html::script('assets/js/admin/classified/datatable.js') !!}
    <script>
        $(function() {
            TableClassified.init();
        });
    </script>
@endsection

@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1>Classificados</h1>
                </div>
            </div>
        </div>

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        @include('admin.common.flash-message')
                    </div>
                </div>
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body">
                                <table class="table table-striped table-bordered table-hover" id="table-classified">
                                    <thead>
                                    <tr>
                                        <th>
                                            Data última modificação
                                        </th>
                                        <th>
                                            Afiliado
                                        </th>
                                        <th>
                                            Título
                                        </th>
                                        <th>
                                            Status
                                        </th>
                                        <th>
                                            Ações
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($classifieds as $classified)
                                        <?php $affiliated = $classified->user ?>
                                        <tr>
                                            <td>{{ $classified->present()->updatedAt }}</td>
                                            <td>
                                                {{ $affiliated->name }} <br />
                                                ID: {{ $affiliated->id }} <br />
                                                CPF: {{ $affiliated->present()->cpf }}
                                            </td>
                                            <td>{{ $classified->title }}</td>
                                            <td>{{ $classified->classifiedStatus->name }}</td>
                                            <td>
                                                <a href="{{ route('admin.classified.edit', ['id' => $classified->id]) }}">
                                                    Editar
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
    </div>
@endsection
