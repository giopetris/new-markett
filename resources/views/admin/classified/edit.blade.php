<?php
$menuAtivo = 'classified';
?>

@extends('admin.layout.base.index')

@section('javascript')
    @parent

    {!! Html::script('assets/lib/artesaos/cidades/js/scripts.js') !!}

    <script type="text/javascript">
        $(function() {
            $('#uf').ufs({
                onChange: function(uf){
                    $('#cidade').cidades({uf: uf});
                }
            });
        });
    </script>
@endsection

@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1>Editar classificado</h1>
                </div>
            </div>
        </div>

        <div class="page-content">
            <div class="container">
                <div class="row ">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-green-sharp bold uppercase">
                                        Dados do classificado
                                    </span>
                                </div>

                                <div class="tools">
                                    <a href="{{ URL::previous() }}">Voltar</a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                {!! Form::model($classified, ['class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}

                                    {!! Form::hidden('id') !!}

                                    <div class="form-group">
                                        {!! Form::label('classified_status_id', 'Status *', ['class' => 'col-md-2 control-label']) !!}
                                        <div class="col-md-10">
                                            {!! Form::select(
                                                'classified_status_id',
                                                $statusList,
                                                null,
                                                [
                                                    'class' => 'form-control'
                                                ]
                                            ) !!}
                                            <span class="text-danger">
                                                {{ $errors->first('classified_status_id') }}
                                            </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('logo', 'Logo', ['class' => 'col-md-2 control-label']) !!}
                                        <div class="col-md-10">
                                            {!! Form::image(
                                                $classified->getFullLogoPath(),
                                                null,
                                                [
                                                    'width' => '225px',
                                                    'height' => '95px',
                                                    'class' => 'pull-left',
                                                    'style' => 'margin-right: 10px'
                                                ]
                                            ) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('title', 'Título', ['class' => 'col-md-2 control-label']) !!}
                                        <div class="col-md-10">
                                            {!! Form::text(
                                                'title',
                                                null,
                                                [
                                                    'class' => 'form-control',
                                                    'readonly' => 'readonly'
                                                ]
                                            ) !!}
                                            <span class="text-danger">
                                                    {{ $errors->first('title') }}
                                                </span>
                                        </div>
                                    </div>

                                    <h4>Endereço</h4>
                                    <hr>

                                    <div class="form-group">
                                        {!! Form::label('address[cep]', 'CEP', ['class' => 'col-md-2 control-label']) !!}
                                        <div class="col-md-10">
                                            {!! Form::text(
                                                'address[cep]',
                                                null,
                                                [
                                                    'class' => 'form-control mask-zip-code',
                                                    'readonly' => 'readonly'
                                                ]
                                            ) !!}
                                            <span class="text-danger">
                                                    {{ $errors->first('address.cep') }}
                                                </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('address[street]', 'Rua', ['class' => 'col-md-2 control-label']) !!}
                                        <div class="col-md-10">
                                            {!! Form::text(
                                                'address[street]',
                                                null,
                                                ['class' => 'form-control', 'readonly' => 'readonly']
                                            ) !!}
                                            <span class="text-danger">
                                                    {{ $errors->first('address.street') }}
                                                </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('address[number]', 'Número', ['class' => 'col-md-2 control-label']) !!}
                                        <div class="col-md-10">
                                            {!! Form::text(
                                                'address[number]',
                                                null,
                                                ['class' => 'form-control', 'readonly' => 'readonly']
                                            ) !!}
                                            <span class="text-danger">
                                                    {{ $errors->first('address.number') }}
                                                </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('address[district]', 'Bairro', ['class' => 'col-md-2 control-label']) !!}
                                        <div class="col-md-10">
                                            {!! Form::text(
                                                'address[district]',
                                                null,
                                                ['class' => 'form-control', 'readonly' => 'readonly']
                                            ) !!}
                                            <span class="text-danger">
                                                    {{ $errors->first('address.district') }}
                                                </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('address[complement]', 'Complemento', ['class' => 'col-md-2 control-label']) !!}
                                        <div class="col-md-10">
                                            {!! Form::text(
                                                'address[complement]',
                                                null,
                                                ['class' => 'form-control', 'readonly' => 'readonly']
                                            ) !!}
                                            <span class="text-danger">
                                                    {{ $errors->first('address.complement') }}
                                                </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('address[state]', 'Estado', ['class' => 'col-md-2 control-label']) !!}
                                        <div class="col-md-10">
                                            <?php
                                            $stateDefaultValue = old('address.state');
                                            if ($stateDefaultValue === null && $classified && $classified->address->city) {
                                                $stateDefaultValue = $classified->address->city->uf;
                                            }
                                            ?>
                                            {!! Form::select(
                                                'address[state]',
                                                [],
                                                null,
                                                [
                                                    'id' => 'uf',
                                                    'default' => $stateDefaultValue,
                                                    'class' => 'form-control',
                                                    'disabled' => 'disabled'
                                                ]
                                            ) !!}
                                            <span class="text-danger">
                                                    {{ $errors->first('address.state') }}
                                                </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('address[city_id]', 'Cidade', ['class' => 'col-md-2 control-label']) !!}
                                        <div class="col-md-10">
                                            <?php
                                            $cityDefaultValue = old('address.city_id');
                                            if ($cityDefaultValue === null && $classified && $classified->address->city) {
                                                $cityDefaultValue = $classified->address->city->id;
                                            }
                                            ?>
                                            {!! Form::select(
                                                'address[city_id]',
                                                [],
                                                null,
                                                [
                                                    'id' => 'cidade',
                                                    'default' => $cityDefaultValue,
                                                    'class' => 'form-control',
                                                    'disabled' => 'disabled'
                                                ]
                                            ) !!}
                                            <span class="text-danger">
                                                    {{ $errors->first('address.city_id') }}
                                                </span>
                                        </div>
                                    </div>

                                    <h4>Contato</h4>
                                    <hr>

                                    <div class="form-group">
                                        {!! Form::label('phone', 'Telefone', ['class' => 'col-md-2 control-label']) !!}
                                        <div class="col-md-10">
                                            {!! Form::text(
                                                'phone',
                                                null,
                                                ['class' => 'form-control mask-phone-number', 'readonly' => 'readonly']
                                            ) !!}
                                            <span class="text-danger">
                                                    {{ $errors->first('phone') }}
                                                </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('cellular', 'Celular', ['class' => 'col-md-2 control-label']) !!}
                                        <div class="col-md-10">
                                            {!! Form::text(
                                                'cellular',
                                                null,
                                                ['class' => 'form-control mask-phone-number', 'readonly' => 'readonly']
                                            ) !!}
                                            <span class="text-danger">
                                                    {{ $errors->first('cellular') }}
                                                </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('site', 'Site', ['class' => 'col-md-2 control-label']) !!}
                                        <div class="col-md-10">
                                            {!! Form::text(
                                                'site',
                                                null,
                                                ['class' => 'form-control', 'readonly' => 'readonly']
                                            ) !!}
                                            <span class="text-danger">
                                                    {{ $errors->first('site') }}
                                                </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('email', 'E-mail', ['class' => 'col-md-2 control-label']) !!}
                                        <div class="col-md-10">
                                            {!! Form::text(
                                                'email',
                                                null,
                                                ['class' => 'form-control', 'readonly' => 'readonly']
                                            ) !!}
                                            <span class="text-danger">
                                                    {{ $errors->first('email') }}
                                                </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('link_facebook', 'Link do Facebook', ['class' => 'col-md-2 control-label']) !!}
                                        <div class="col-md-10">
                                            {!! Form::text(
                                                'link_facebook',
                                                null,
                                                ['class' => 'form-control', 'readonly' => 'readonly']
                                            ) !!}
                                            <span class="text-danger">
                                                    {{ $errors->first('link_facebook') }}
                                                </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-offset-2 col-md-10">
                                            {!! Form::submit('Salvar', ['class' => 'btn blue']) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                        <!-- END SAMPLE FORM PORTLET-->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
