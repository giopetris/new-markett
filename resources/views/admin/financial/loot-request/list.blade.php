<?php
$menuAtivo = 'financial';
?>

@extends('admin.layout.base.index')

@section('stylesheet')
    @parent

    {!! Html::style('assets/theme/metronic/global/plugins/select2/select2.css') !!}
    {!! Html::style('assets/theme/metronic/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css') !!}
@endsection

@section('javascript')
    @parent

    {!! Html::script('assets/theme/metronic/global/plugins/select2/select2.min.js') !!}
    {!! Html::script('assets/theme/metronic/global/plugins/datatables/media/js/jquery.dataTables.min.js') !!}
    {!! Html::script('assets/theme/metronic/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') !!}


    {!! Html::script('assets/js/admin/loot-request/datatable.js') !!}
    <script>
        $(function() {
            TableLootRequest.init();
        });
    </script>
@endsection

@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1>Solicitações de saque</h1>
                </div>
            </div>
        </div>

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        @include('admin.common.flash-message')
                    </div>
                </div>
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet light">
                            <div class="portlet-body">
                                <table class="table table-striped table-bordered table-hover" id="table-loot-request">
                                    <thead>
                                    <tr>
                                        <th>
                                            Data
                                        </th>
                                        <th>
                                            Afiliado
                                        </th>
                                        <th>
                                            Valor
                                        </th>
                                        <th>
                                            Status
                                        </th>
                                        <th>
                                            Pago em
                                        </th>
                                        <th>
                                            Ações
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($lootRequests as $lootRequest)
                                        <?php $user = $lootRequest->user; ?>
                                        <tr>
                                            <td>{{ $lootRequest->present()->createdAt }}</td>
                                            <td>
                                                {{ $user->name }} <br />
                                                ID: {{ $user->id }} <br />
                                                @if($user->cpf)
                                                    CPF: {{ $user->present()->cpf }}
                                                @endif
                                            </td>
                                            <td>{{ $lootRequest->present()->value }}</td>
                                            <td>{{ $lootRequest->transactionStatus->name }}</td>
                                            <td>{{ $lootRequest->present()->paidAt ? $lootRequest->present()->paidAt : '-' }}</td>
                                            <td>
                                                <a href="{{ route('admin.financial.loot-request.edit', ['id' => $lootRequest->id]) }}">
                                                    Editar
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
    </div>
@endsection
