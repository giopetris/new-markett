<?php
$menuAtivo = 'financial';
?>

@extends('admin.layout.base.index')

@section('javascript')
    @parent
    {!! Html::script('assets/theme/metronic/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') !!}
    {!! Html::script('assets/theme/metronic/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.pt-BR.min.js') !!}
    {!! Html::script('assets/theme/metronic/global/scripts/components-pickers.js') !!}

    <script>
        jQuery(document).ready(function() {
            ComponentsPickers.init();
        });

        var transactionStatusPaid = '{{ \NewMarkett\PickLists\Financial\TransactionStatusNamePickList::PAID }}';
    </script>

    {!! Html::script('assets/js/common/financial/handleVisibilityPaidAtField.js') !!}
@endsection

@section('stylesheet')
    @parent

    {!! Html::style('assets/theme/metronic/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') !!}
@endsection

@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1>Editar solicitação de saque</h1>
                </div>
            </div>
        </div>

        <div class="page-content">
            <div class="container">
                <div class="row ">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="row" style="text-align: center;">
                                    <div class="col-md-4" style="border-right: solid 1px #ddd;">
                                        <div class="caption" style="margin-bottom: 5px; ">
                                            <span class="caption-subject font-green-sharp bold uppercase">
                                                Dados do cliente
                                            </span>
                                        </div>

                                        <?php
                                            $user = $lootRequest->user;
                                            $address = $user->address;
                                            $bankData = $user->bankData;
                                        ?>

                                        <p>
                                            <strong>ID:</strong> {{ $user->id }} <br>
                                            <strong>Nome:</strong> {{ $user->name }} <br>
                                            <strong>CPF:</strong> {{ $user->present()->cpf }} <br>
                                            <strong>E-mail:</strong> {{ $user->email }} <br>

                                            @if ($user->phone || $user->cellular)
                                                <strong>Telefones:</strong> {{ $user->present()->phones }} <br>
                                            @endif
                                        </p>
                                    </div>

                                    <div class="col-md-4" style="border-right: solid 1px #ddd;">
                                        <div class="caption" style="margin-bottom: 5px;">
                                            <span class="caption-subject font-green-sharp bold uppercase">
                                                Endereço
                                            </span>
                                        </div>

                                        <p>
                                            @if ($address)
                                                <strong>CEP:</strong> {{ $address->present()->cep }} <br>
                                                <strong>Rua:</strong> {{ $address->street }} <br>
                                                <strong>Número:</strong> {{ $address->number }} <br>
                                                <strong>Bairro:</strong> {{ $address->district }} <br>
                                                <strong>Complemento:</strong> {{ $address->complement }} <br>
                                                <strong>Cidade:</strong> {{ $address->city->nome . ', ' . $address->city->uf }} <br>
                                            @endif
                                        </p>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="caption" style="margin-bottom: 5px;">
                                            <span class="caption-subject font-green-sharp bold uppercase">
                                                Dados bancários
                                            </span>
                                        </div>

                                        <p>
                                            @if ($bankData)
                                                <strong>Banco:</strong> {{ $bankData->bank->name }} <br>
                                                <strong>Tipo:</strong> {{ $bankData->accountType->name }} <br>
                                                <strong>Agência:</strong> {{ $bankData->agency }} <br>
                                                <strong>Conta:</strong> {{ $bankData->account_number }} <br>
                                            @endif
                                        </p>
                                    </div>
                                </div>


                                <div class="tools">
                                    <a href="{{ URL::previous() }}">Voltar</a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                {!! Form::model($lootRequest, ['class' => 'form-horizontal']) !!}
                                    <div class="form-group">
                                        {!! Form::label('value', 'Valor solicitado', ['class' => 'col-md-2 control-label']) !!}
                                        <div class="col-md-10">
                                            {!! Form::text(
                                                'value',
                                                $lootRequest->present()->value,
                                                ['class' => 'form-control', 'readonly' => 'readonly']
                                            ) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('transaction_status_id', 'Status', ['class' => 'col-md-2 control-label']) !!}
                                        <div class="col-md-10">
                                            {!! Form::select(
                                                'transaction_status_id',
                                                $lootStatusList,
                                                null,
                                                [
                                                    'class' => 'form-control',
                                                ]
                                            ) !!}
                                            <span class="text-danger">
                                                {{ $errors->first('transaction_status_id') }}
                                            </span>
                                        </div>
                                    </div>

                                    <div class="form-group form-group-paid-at" style="display: none;">
                                        {!! Form::label('paid_at', 'Pago em', ['class' => 'col-md-2 control-label']) !!}
                                        <div class="col-md-10">
                                            <?php
                                            $paidAt = $lootRequest->present()->paidAt;

                                            if ( ! $paidAt) {
                                                $now = new DateTime();
                                                $paidAt = $now->format('d/m/Y');
                                            }
                                            ?>

                                            {!! Form::text(
                                                'paid_at',
                                                $paidAt,
                                                ['class' => 'form-control date-picker',]
                                            ) !!}

                                            <span class="text-danger">
                                                {{ $errors->first('paid_at') }}
                                            </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-offset-2 col-md-10">
                                            {!! Form::submit('Salvar', ['class' => 'btn blue']) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                        <!-- END SAMPLE FORM PORTLET-->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
