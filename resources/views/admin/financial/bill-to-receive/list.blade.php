<?php
$menuAtivo = 'financial';
?>

@extends('admin.layout.base.index')

@section('stylesheet')
    @parent

    {!! Html::style('assets/theme/metronic/global/plugins/select2/select2.css') !!}
    {!! Html::style('assets/theme/metronic/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css') !!}
@endsection

@section('javascript')
    @parent

    {!! Html::script('assets/theme/metronic/global/plugins/select2/select2.min.js') !!}
    {!! Html::script('assets/theme/metronic/global/plugins/datatables/media/js/jquery.dataTables.min.js') !!}
    {!! Html::script('assets/theme/metronic/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') !!}


    {!! Html::script('assets/js/admin/bill-to-receive/datatable.js') !!}
    <script>
        $(function() {
            TableBillToReceive.init();
        });
    </script>
@endsection

@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1>Contas a receber</h1>
                </div>
            </div>
        </div>

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        @include('admin.common.flash-message')
                    </div>
                </div>
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet light">
                            <div class="portlet-body">
                                <table class="table table-striped table-bordered table-hover" id="table-bill-to-receive">
                                    <thead>
                                    <tr>
                                        <th>
                                            Data de lançamento
                                        </th>
                                        <th>
                                            Afiliado
                                        </th>
                                        <th>
                                            Descrição
                                        </th>
                                        <th>
                                            Valor
                                        </th>
                                        <th>
                                            Status
                                        </th>
                                        <th>
                                            Recebido em
                                        </th>
                                        <th>
                                            Ações
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($transactions as $transaction)
                                        <?php $user = $transaction->user; ?>
                                        <tr>
                                            <td>{{ $transaction->present()->createdAt }}</td>
                                            <td>
                                                {{ $user->name }} <br />
                                                ID: {{ $user->id }} <br />
                                                @if($user->cpf)
                                                    CPF: {{ $user->present()->cpf }}
                                                @endif
                                            </td>
                                            <td>{{ $transaction->description }}</td>
                                            <td>{{ $transaction->present()->value }}</td>
                                            <td>{{ $transaction->transactionStatus->name }}</td>
                                            <td>{{ $transaction->present()->paidAt ? $transaction->present()->paidAt : '-' }}</td>
                                            <td>
                                                <a href="{{ route('admin.financial.bill-to-receive.edit', ['id' => $transaction->id]) }}">
                                                    Editar
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
    </div>
@endsection
