<?php
use NewMarkett\PickLists\Financial\TransactionStatusNamePickList;

$menuAtivo = 'financial';
?>

@extends('admin.layout.base.index')

@section('javascript')
    @parent

    {!! Html::script('assets/theme/metronic/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') !!}
    {!! Html::script('assets/theme/metronic/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.pt-BR.min.js') !!}
    {!! Html::script('assets/theme/metronic/global/scripts/components-pickers.js') !!}

    <script>
        jQuery(document).ready(function() {
            ComponentsPickers.init();
        });

        var transactionStatusPaid = '{{ \NewMarkett\PickLists\Financial\TransactionStatusNamePickList::PAID }}';
    </script>

    {!! Html::script('assets/js/common/financial/handleVisibilityPaidAtField.js') !!}
@endsection

@section('stylesheet')
    @parent

    {!! Html::style('assets/theme/metronic/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') !!}
@endsection

@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1>Editar conta a receber</h1>
                </div>
            </div>
        </div>

        <div class="page-content">
            <div class="container">
                <div class="row ">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="row" style="text-align: center;">
                                    <div class="col-md-4" style="border-right: solid 1px #ddd;">
                                        <div class="caption" style="margin-bottom: 5px; ">
                                            <span class="caption-subject font-green-sharp bold uppercase">
                                                Dados do afiliado
                                            </span>
                                        </div>

                                        <?php
                                            $user = $transaction->user;
                                            $address = $user->address;
                                        ?>

                                        <p>
                                            <strong>ID:</strong> {{ $user->id }} <br>
                                            <strong>Nome:</strong> {{ $user->name }} <br>
                                            <strong>CPF:</strong> {{ $user->present()->cpf }} <br>
                                            <strong>E-mail:</strong> {{ $user->email }} <br>

                                            @if ($user->phone || $user->cellular)
                                                <strong>Telefones:</strong> {{ $user->present()->phones }} <br>
                                            @endif
                                        </p>
                                    </div>

                                    @if ($address)

                                        <div class="col-md-4" style="border-right: solid 1px #ddd;">
                                            <div class="caption" style="margin-bottom: 5px;">
                                                <span class="caption-subject font-green-sharp bold uppercase">
                                                    Endereço
                                                </span>
                                            </div>

                                            <p>
                                                <strong>CEP:</strong> {{ $address->present()->cep }} <br>
                                                <strong>Rua:</strong> {{ $address->street }} <br>
                                                <strong>Número:</strong> {{ $address->number }} <br>
                                                <strong>Bairro:</strong> {{ $address->district }} <br>
                                                <strong>Complemento:</strong> {{ $address->complement }} <br>
                                                @if ($address->city)
                                                    <strong>Cidade:</strong> {{ $address->city->nome . ', ' . $address->city->uf }} <br>
                                                @endif
                                            </p>
                                        </div>
                                    @endif
                                </div>


                                <div class="tools">
                                    <a href="{{ URL::previous() }}">Voltar</a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                {!! Form::model($transaction, ['class' => 'form-horizontal']) !!}
                                    <div class="form-group">
                                        {!! Form::label('created_at', 'Data de lançamento', ['class' => 'col-md-2 control-label']) !!}
                                        <div class="col-md-10">
                                            {!! Form::text(
                                                'created_at',
                                                $transaction->present()->createdAt,
                                                ['class' => 'form-control', 'readonly' => 'readonly']
                                            ) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('description', 'Descrição', ['class' => 'col-md-2 control-label']) !!}
                                        <div class="col-md-10">
                                            {!! Form::text(
                                                'description',
                                                $transaction->description,
                                                ['class' => 'form-control', 'readonly' => 'readonly']
                                            ) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('value', 'Valor', ['class' => 'col-md-2 control-label']) !!}
                                        <div class="col-md-10">
                                            {!! Form::text(
                                                'value',
                                                $transaction->present()->value,
                                                ['class' => 'form-control', 'readonly' => 'readonly']
                                            ) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('transaction_status_id', 'Status', ['class' => 'col-md-2 control-label']) !!}
                                        <div class="col-md-10">
                                            <?php
                                                $transactionIdPaid = $transaction->transaction_status_id == TransactionStatusNamePickList::PAID;

                                                $transactionStatusIdOptions = [
                                                    'class' => 'form-control',
                                                ];

                                                if ($transactionIdPaid) {
                                                    $transactionStatusIdOptions['readonly'] = 'readonly';
                                                }
                                            ?>

                                            {!! Form::select(
                                                'transaction_status_id',
                                                $transactionStatusList,
                                                null,
                                                $transactionStatusIdOptions
                                            ) !!}
                                            <span class="text-danger">
                                                {{ $errors->first('transaction_status_id') }}
                                            </span>
                                        </div>
                                    </div>

                                    <?php
                                    $paidAt = $transaction->present()->paidAt;

                                    if ( ! $paidAt) {
                                        $now = new DateTime();
                                        $paidAt = $now->format('d/m/Y');
                                    }
                                    ?>

                                    <div class="form-group form-group-paid-at" style="display: none;">
                                        {!! Form::label('paid_at', 'Recebido em', ['class' => 'col-md-2 control-label']) !!}
                                        <div class="col-md-10">
                                            <?php
                                                $paidAtOptions = [
                                                        'class' => 'form-control date-picker',
                                                ];

                                                if ($transactionIdPaid) {
                                                    $paidAtOptions['readonly'] = 'readonly';
                                                }
                                            ?>
                                            {!! Form::text(
                                                'paid_at',
                                                $paidAt,
                                                $paidAtOptions
                                            ) !!}

                                            <span class="help-block">
                                                <strong>Atenção!</strong> Depois que o status for salvo como pago você
                                                não poderá mais alterá-lo. <br />
                                                Pois, caso o pagamento seja da taxa de adesão ou renovação anual
                                                o sistema irá calcular e disponibilizar o valor aos usuários para
                                                recebimento da comissão direta e divisão global. <br />
                                                Caso o pagamento seja referente a taxa mensal o sistema irá fazer o
                                                lançamento das comissões mensais.
                                            </span>
                                            <span class="text-danger">
                                                {{ $errors->first('paid_at') }}
                                            </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-offset-2 col-md-10">
                                            @if($transactionIdPaid)
                                                <a href="{{ URL::previous() }}" class="btn blue">Ok</a>
                                            @else
                                                {!! Form::submit('Salvar', ['class' => 'btn blue']) !!}
                                            @endif
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                        <!-- END SAMPLE FORM PORTLET-->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
