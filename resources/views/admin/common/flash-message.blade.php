@foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))
        <div class="alert alert-{{ $msg }}">
            <button class="close" data-close="alert"></button>
            <span>
                {{ Session::get(sprintf('alert-%s', $msg)) }}
            </span>
        </div>
    @endif
@endforeach
