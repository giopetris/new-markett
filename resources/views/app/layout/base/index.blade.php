<!DOCTYPE html>
<!--[if IE 8]>
<html lang="pt-BR" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="pt-BR" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="pt-BR" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8">
    <title>New Markett</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="" name="description">
    <meta content="" name="author">

    @section('stylesheet')
        {!! Html::style('http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all') !!}
        {!! Html::style('assets/theme/metronic/global/plugins/font-awesome/css/font-awesome.min.css') !!}
        {!! Html::style('assets/theme/metronic/global/plugins/simple-line-icons/simple-line-icons.min.css') !!}
        {!! Html::style('assets/theme/metronic/global/plugins/bootstrap/css/bootstrap.min.css') !!}
        {!! Html::style('assets/theme/metronic/global/plugins/uniform/css/uniform.default.css') !!}

        {!! Html::style('assets/theme/metronic/global/css/components-rounded.css') !!}
        {!! Html::style('assets/theme/metronic/global/css/plugins.css') !!}
        {!! Html::style('assets/theme/metronic/admin/layout3/css/layout.css') !!}
        {!! Html::style('assets/theme/metronic/admin/layout3/css/themes/default.css') !!}
    @show

    <link rel="shortcut icon" href="favicon.ico">
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-menu-fixed" class to set the mega menu fixed  -->
<!-- DOC: Apply "page-header-top-fixed" class to set the top menu fixed  -->
<body>
<!-- BEGIN HEADER -->
<div class="page-header">
    <!-- BEGIN HEADER TOP -->
    <div class="page-header-top">
        <div class="container">
            <!-- BEGIN LOGO -->
            <div class="page-logo">
                <a href="#" class='no-text-decoration'>
                    <h1 class='no-text-decoration'>New Markett</h1>
                </a>
            </div>
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a href="javascript:;" class="menu-toggler"></a>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">
                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <li class="dropdown dropdown-user dropdown-dark">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                           data-close-others="true">
                            {{--<img alt="" class="img-circle" src="{{ asset('assets/theme/metronic/admin/layout3/img/avatar9.jpg') }}">--}}
                            <i class="icon-user" style="margin-right: 6px; font-size: 24px"></i>
                            <span class="username username-hide-mobile"><strong>{{ $user->name }}</strong> {{ ' - Meu ID ' . $user->id  }}</span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <li>
                                <a href="{{ route('app.my-account') }}">
                                    <i class="icon-user"></i> Minha conta </a>
                            </li>
                            <li class="divider">
                            </li>
                            <li>
                                <a href="{{ route('app.auth.logout') }}">
                                    <i class="icon-key"></i> Logout
                                </a>
                            </li>
                        </ul>
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
    </div>
    <!-- END HEADER TOP -->
    <!-- BEGIN HEADER MENU -->
    <div class="page-header-menu">
        <div class="container">
            <!-- BEGIN MEGA MENU -->
            <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
            <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
            <div class="hor-menu ">
                <ul class="nav navbar-nav">
                    <li class="<?= isset($menuAtivo) && $menuAtivo == 'my-network' ? 'active' : '' ?>">
                        <a href="{{ route('app.my-network') }}">Minha rede</a>
                    </li>
                    <li class="<?= isset($menuAtivo) && $menuAtivo == 'extract' ? 'active' : '' ?>">
                        <a href="{{ route('app.extract') }}">Extrato</a>
                    </li>
                    <li class="<?= isset($menuAtivo) && $menuAtivo == 'loot' ? 'active' : '' ?>">
                        <a href="{{ route('app.loot') }}">Saque</a>
                    </li>
                    <li class="<?= isset($menuAtivo) && $menuAtivo == 'my-classified' ? 'active' : '' ?>">
                        <a href="{{ route('app.my-classified') }}">Meu classificado</a>
                    </li>
                </ul>
            </div>
            <!-- END MEGA MENU -->
        </div>
    </div>
    <!-- END HEADER MENU -->
</div>
<!-- END HEADER -->

@yield('content')

        <!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="container">
        {{ date('Y') }} &copy; New Markett
    </div>
</div>
<div class="scroll-to-top">
    <i class="icon-arrow-up"></i>
</div>

@section('javascript')
    <script type="text/javascript">
        var url = '{{ env('APP_URL') }}'
    </script>

    <!--[if lt IE 9]>
    {!! Html::script('assets/theme/metronic/global/plugins/respond.min.js') !!}
    {!! Html::script('assets/theme/metronic/global/plugins/excanvas.min.js') !!}
    <![endif]-->
    {!! Html::script('assets/theme/metronic/global/plugins/jquery.min.js') !!}
    {!! Html::script('assets/theme/metronic/global/plugins/jquery-migrate.min.js') !!}
    {!! Html::script('assets/theme/metronic/global/plugins/jquery-ui/jquery-ui.min.js') !!}
    {!! Html::script('assets/theme/metronic/global/plugins/bootstrap/js/bootstrap.min.js') !!}
    {!! Html::script('assets/theme/metronic/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') !!}
    {!! Html::script('assets/theme/metronic/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') !!}
    {!! Html::script('assets/theme/metronic/global/plugins/jquery.blockui.min.js') !!}
    {!! Html::script('assets/theme/metronic/global/plugins/uniform/jquery.uniform.min.js') !!}
    {!! Html::script('assets/theme/metronic/global/plugins/jquery.cokie.min.js') !!}

    {!! Html::script('assets/theme/metronic/global/scripts/metronic.js') !!}
    {!! Html::script('assets/theme/metronic/admin/layout/scripts/layout.js') !!}

    {!! Html::script('assets/lib/jquery-mask-money/jquery.maskMoney.min.js') !!}
    {!! Html::script('assets/lib/jquery-masked-input/jquery.maskedInput.min.js') !!}
    {!! Html::script('assets/js/common/mask.js') !!}
@show

<script>
    jQuery(document).ready(function () {
        Metronic.init(); // init metronic core componets
        Layout.init(); // init layout
    });
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
