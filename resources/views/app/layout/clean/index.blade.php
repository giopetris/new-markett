<!DOCTYPE html>
<!--[if IE 8]> <html lang="pt-BR" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="pt-BR" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="pt-BR">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>New Markett</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="Giovanni Petris" name="author"/>

    @section('stylesheet')
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        {!! Html::style('http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all') !!}
        {!! Html::style('assets/theme/metronic/global/plugins/font-awesome/css/font-awesome.min.css') !!}
        {!! Html::style('assets/theme/metronic/global/plugins/simple-line-icons/simple-line-icons.min.css') !!}
        {!! Html::style('assets/theme/metronic/global/plugins/bootstrap/css/bootstrap.min.css') !!}
        {!! Html::style('assets/theme/metronic/global/plugins/uniform/css/uniform.default.css') !!}
                <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        {!! Html::style('assets/theme/metronic/admin/pages/css/login2.css') !!}
                <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME STYLES -->
        {!! Html::style('assets/theme/metronic/global/css/components-rounded.css') !!}
        {!! Html::style('assets/theme/metronic/global/css/plugins.css') !!}
        {!! Html::style('assets/theme/metronic/admin/layout/css/layout.css') !!}
        {!! Html::style('assets/theme/metronic/admin/layout/css/themes/default.css') !!}
        {!! Html::style('assets/theme/metronic/admin/layout/css/custom.css') !!}
        <!-- END THEME STYLES -->
    @show

    <link rel="shortcut icon" href="favicon.ico" />
</head>

<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>

@yield('content')

@section('javascript')
    <!-- END LOGIN -->
    <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
    <!-- BEGIN CORE PLUGINS -->
    <!--[if lt IE 9]>
    {!! Html::script('assets/theme/metronic/global/plugins/respond.min.js') !!}
    {!! Html::script('assets/theme/metronic/global/plugins/excanvas.min.js') !!}
    <![endif]-->
    {!! Html::script('assets/theme/metronic/global/plugins/jquery.min.js') !!}
    {!! Html::script('assets/theme/metronic/global/plugins/jquery-migrate.min.js') !!}
    {!! Html::script('assets/theme/metronic/global/plugins/bootstrap/js/bootstrap.min.js') !!}
    {!! Html::script('assets/theme/metronic/global/plugins/jquery.blockui.min.js') !!}
    {!! Html::script('assets/theme/metronic/global/plugins/uniform/jquery.uniform.min.js') !!}
    {!! Html::script('assets/theme/metronic/global/plugins/jquery.cokie.min.js') !!}
            <!-- END CORE PLUGINS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    {!! Html::script('assets/theme/metronic/global/plugins/jquery-validation/js/jquery.validate.min.js') !!}
            <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    {!! Html::script('assets/theme/metronic/global/scripts/metronic.js') !!}
    {!! Html::script('assets/theme/metronic/admin/layout/scripts/layout.js') !!}
        <!-- END PAGE LEVEL SCRIPTS -->
    <script>
        jQuery(document).ready(function() {
            Metronic.init(); // init metronic core components
            Layout.init(); // init current layout
        });
    </script>
    <!-- END JAVASCRIPTS -->

    {!! Html::script('assets/lib/jquery-mask-money/jquery.maskMoney.min.js') !!}
    {!! Html::script('assets/lib/jquery-masked-input/jquery.maskedInput.min.js') !!}
    {!! Html::script('assets/js/common/mask.js') !!}
@show

</body>
<!-- END BODY -->
</html>
