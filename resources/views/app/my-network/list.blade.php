<?php
$menuAtivo = 'my-network';
?>

@extends('app.layout.base.index')

@section('stylesheet')
    @parent

    {!! Html::style('assets/theme/metronic/global/plugins/select2/select2.css') !!}
    {!! Html::style('assets/theme/metronic/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css') !!}
@endsection

@section('javascript')
    @parent

    {!! Html::script('assets/theme/metronic/global/plugins/select2/select2.min.js') !!}
    {!! Html::script('assets/theme/metronic/global/plugins/datatables/media/js/jquery.dataTables.min.js') !!}
    {!! Html::script('assets/theme/metronic/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') !!}

    {!! Html::script('assets/js/app/my-network/datatable.js') !!}

    <script>
        $(function() {
            TableMyNetwork.init();
        });
    </script>
@endsection

@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1>Minha rede</h1>
                </div>

                <a href="{{ route('app.my-network.new-affiliate') }}" class="btn btn-primary pull-right" style="margin-top: 14px;">
                    Novo afiliado
                </a>
            </div>
        </div>

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        @include('app.common.flash-message')
                    </div>
                </div>
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet light">
                            <div class="portlet-body">
                                <table class="table table-striped table-bordered table-hover" id="table-my-network">
                                    <thead>
                                    <tr>
                                        <th>
                                            ID
                                        </th>
                                        <th>
                                            Nome
                                        </th>
                                        <th>
                                            E-mail
                                        </th>
                                        <th>
                                            Telefones
                                        </th>
                                        <th>
                                            Data de afiliação
                                        </th>
                                        <th>
                                            Status
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($users as $user)
                                            <tr>
                                                <td>{{ $user->id }}</td>
                                                <td>{{ $user->name }}</td>
                                                <td>{{ $user->email }}</td>
                                                <td>{{ $user->present()->phones }}</td>
                                                <td>{{ $user->present()->createdAt }}</td>
                                                <td>{{ $getStatusUserService->getStatusNameOf($user) }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
    </div>
@endsection
