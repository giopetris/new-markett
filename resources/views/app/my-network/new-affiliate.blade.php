<?php
$menuAtivo = 'my-network';
?>

@extends('app.layout.base.index')

@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1>Minha rede</h1>
                </div>
            </div>
        </div>

        <div class="page-content">
            <div class="container">
                <div class="row ">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-green-sharp bold uppercase">
                                        Novo afiliado
                                    </span>
                                </div>

                                <div class="tools">
                                    <a href="{{ URL::previous() }}">Voltar</a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                {!! Form::open(['class' => 'form-horizontal']) !!}
                                    <div class="form-group">
                                        {!! Form::label('name', 'Nome', ['class' => 'col-md-1 control-label']) !!}
                                        <div class="col-md-11">
                                            {!! Form::text(
                                                'name',
                                                null,
                                                ['class' => 'form-control']
                                            ) !!}

                                            <span class="text-danger">
                                                {{ $errors->first('name') }}
                                            </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('email', 'E-mail', ['class' => 'col-md-1 control-label']) !!}
                                        <div class="col-md-11">
                                            {!! Form::text(
                                                'email',
                                                null,
                                                ['class' => 'form-control']
                                            ) !!}

                                            <span class="text-danger">
                                                {{ $errors->first('email') }}
                                            </span>
                                            <div class="help-block">
                                                Ao convidar um novo afiliado será enviado um e-mail ao mesmo para criar
                                                a sua conta e fazer parte da sua rede. Quando a cota for paga, você
                                                receberá 10% do valor.
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-offset-1 col-md-11">
                                            {!! Form::submit('Convidar', ['class' => 'btn blue']) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                        <!-- END SAMPLE FORM PORTLET-->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
