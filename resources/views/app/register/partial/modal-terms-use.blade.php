<div class="modal fade bs-modal-lg" id="modal-terms-use" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Proposta de afiliação</h4>
            </div>
            <div class="modal-body">
                1. O AFILIADO da NewMarkett tem o direito de oferecer os serviços da empresa para revenda,
                assim como indicar pessoas em conformidade com estes Termos e Condições.

                <br /><br />

                2. O recebimento de bônus está vinculado ao requisitos de qualificação de acordo com a
                política de vendas vigente publicada em nosso site www.newmarkett.com.br.

                <br /><br />

                3. A apresentação do Plano de Venda bem como de seus produtos deve ser feita em
                conformidade com os materiais disponibilizados em nosso site.

                <br /><br />

                4. A natureza jurídica da relação existente entre o AFILIADO e a NewMarkett é de venda direta
                e, desta forma, não pode ser considerada como tendente a promover entre as partes
                associação em qualquer negócio conjunto ou ainda, qualificar o AFILIADO, para todos os fins,
                inclusive fiscais, na condição de sócio, agente, franqueado, representante de vendas,
                mandatário, de consumo ou empregado da NewMarkett sem que se possa interpretar o
                presente contrato como origem ou causa de qualquer uma destas relações.

                <br /><br />

                5. O AFILIADO é exclusivamente responsável pelo pagamento de todas as despesas incorridas
                de sua atividade de AFILIADO, incluindo, mas não limitado às despesas de viagem,
                alimentação, acomodação, secretariais, de escritório, telefonemas e de qualquer outra
                despesa.

                <br /><br />

                6. O AFILIADO declara que leu, compreendeu e aceitou todos os termos dessa Proposta de
                AFILIADO. Esta Proposta e a política de vendas poderá ser alterada a exclusivo critério da
                NewMarkett, concordando o AFILIADO em aderir a todas as referidas alterações sendo que a
                notificação de alterações será incluída na página de internet da NewMarkett e entrarão em
                vigor no prazo de 30 dias a contar de sua publicação. A continuidade do AFILIADO no negócio
                ou a aceitação dos bônus constituirão aceitação a todas e quaisquer alterações.

                <br /><br />

                7. O prazo deste Contrato é de um ano. O cancelamento, rescisão ou não renovação anual do
                negócio NewMarkett implica na perda definitiva de todos os direitos do AFILIADO, incluindo,
                mas não se limitando a revenda dos produtos da NewMarkett e elegibilidade para o
                recebimento de bonificações resultantes das atividades de sua organização independente de
                revenda. Na hipótese de cancelamento, rescisão ou ausência de renovação, o AFILIADO
                renuncia a todos os direitos que possui, incluindo, mas não limitado aos direitos de propriedade
                relativos a seu grupo de vendas e a quaisquer outras bonificações resultantes das revendas e
                de outras atividades de seu grupo. A NewMarkett se reserva o direito de rescindir esta
                Proposta através de uma notificação com 30 dias de antecedência por qualquer motivo,
                incluindo, mas não se limitando a encerramento de suas operações comerciais ou término da
                distribuição de seus produtos através de canais de venda direta. O AFILIADO poderá cancelar
                o presente Contrato a qualquer tempo, e por qualquer motivo, através de uma notificação, por
                e-mail, à NewMarkett.

                <br /><br />

                8. É vedado ao AFILIADO a cessão de direitos ou delegação de suas atribuições nos termos
                desta Proposta, sem o prévio consentimento por escrito da NewMarkett. Qualquer tentativa de
                transferência ou cessão da Proposta sem o expresso consentimento por escrito da NewMarkett
                a tornará nula, a critério da NewMarkett e poderá resultar na rescisão da mesma.

                <br /><br />

                9. O descumprimento dos termos desta Proposta implica, a critério da NewMarkett, em
                aplicação de ação disciplinar. O descumprimento, infração ou violação desta Proposta implicam
                na perda de direito de recebimento de quaisquer bônus futuros, tenham ou não as revendas
                relativas a tais bônus sido concluídas.

                <br /><br />

                10. O AFILIADO reconhece que é um prestador de serviços independente e que é
                exclusivamente responsável por quaisquer reclamações, perdas, danos ou responsabilidades
                relativas a atos e omissões decorrentes de sua conduta, nos termos desta Proposta. O
                AFILIADO obriga-se a indenizar e isentar a NewMarkett de todas e quaisquer reclamações ou
                responsabilidades que lhe forem imputadas respondendo inclusive, porém não se limitando a,
                reclamações decorrentes de propaganda falsa ou enganosa ou de informações deturpadas
                prestadas no que se refere aos produtos oferecidos ou pelo não cumprimento das leis
                aplicáveis que regem as suas atividades.

                <br /><br />

                11. Esta Proposta, em seu formato atual e conforme alterado pela NewMarkett, a seu critério,
                constitui o acordo integral entre a NewMarkett e o AFILIADO. Quaisquer promessas,
                declarações, ofertas ou outras comunicações que não estejam expressamente previstas na
                Proposta não estão em vigor ou efeito.

                <br /><br />

                12. Nenhuma das disposições desta Proposta poderá ser renunciada ou modificada em
                decorrência da falta de insistência no cumprimento da mesma ou de alguma outra forma, que
                não seja expressa e devidamente assinada pela parte que seria beneficiada por esta
                disposição. Eventual ato de tolerância em relação ao descumprimento de prazos, valores e
                demais obrigações pactuadas neste instrumento não constituem novação e não implicam na
                alteração do ajuste ora firmado sendo certo que qualquer alteração, exclusão ou inclusão de
                cláusula para esta Proposta deverá ser realizada mediante termo aditivo específico.

                <br /><br />

                13. Se qualquer disposição da Proposta for considerada inválida ou inexeqüível, a referida
                disposição deverá ser alterada somente na extensão necessária para torná-la exeqüível, e o
                remanescente da Proposta permanecerá em pleno vigor e efeito.

                <br /><br />

                14. O AFILIADO autoriza a NewMarkett a utilizar seu nome, fotografia, histórico pessoal e/ou
                similares em materiais de propaganda ou promocionais e renuncia a todas as reivindicações
                por remuneração para tal uso.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn blue" data-dismiss="modal">OK</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
