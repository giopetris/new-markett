@extends('app.layout.clean.index')

@section('javascript')
    @parent

    {!! Html::script('assets/js/app/register/handleSponsorField.js') !!}
    {!! Html::script('assets/lib/jquery-jreject/js/jquery.reject.js') !!}
    {!! Html::script('assets/js/common/init-jreject.js') !!}
@endsection

@section('stylesheet')
    @parent

    {!! Html::style('assets/lib/jquery-jreject/css/jquery.reject.css') !!}
@endsection

@section('content')
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGO -->
<div class="logo" style="margin-top: 20px">
    <a href="{{ route('institutional.home') }}" class="no-text-decoration">
        <h1 class="no-text-decoration">New Markett</h1>
    </a>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">

    {!! Form::open(['class' => 'login-form', 'autocomplete' => 'off']) !!}
    {!! Form::hidden('id', $preUser ? $preUser->id : null) !!}

    @include('app.common.flash-message')

    <div class="form-title">
        <span class="form-title">Cadastre-se</span>
    </div>

    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        {!! Form::label(
            'sponsor_id',
            'ID do meu patrocinador *',
            ['class' => 'control-label']
        ) !!}

        <?php
            $sponsorOptions = [
                'class' => 'form-control form-control-solid placeholder-no-fix',
                'id' => 'input-sponsor-id'
            ];

            if ($preUser) {
                $sponsorOptions['readonly'] = 'readonly';
            }
        ?>

        {!! Form::text(
            'sponsor_id',
            $preUser ? $preUser->sponsor_id : null,
            $sponsorOptions
        ) !!}

        <span>
            {{ $errors->first('sponsor_id') }}
        </span>

        @if( ! $preUser)
            <label class="check" style="display: block; margin-top: 5px;">
                <input type="checkbox" id="checkbox-has-sponsor"/>Não tenho patrocinador
            </label>
        @endif
    </div>

    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        {!! Form::label(
            'name',
            'Nome *',
            ['class' => 'control-label']
        ) !!}

        {!! Form::text(
            'name',
            $preUser ? $preUser->name : null,
            [
                'class' => 'form-control form-control-solid placeholder-no-fix',
            ]
        ) !!}
        <span>
            {{ $errors->first('name') }}
        </span>
    </div>

    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        {!! Form::label(
            'cpf',
            'CPF *',
            ['class' => 'control-label']
        ) !!}
        {!! Form::text(
            'cpf',
            null,
            [
                'class' => 'form-control form-control-solid placeholder-no-fix mask-cpf',
            ]
        ) !!}
        <span>
            {{ $errors->first('cpf') }}
        </span>
    </div>

    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        {!! Form::label(
            'phone',
            'Telefone',
            ['class' => 'control-label']
        ) !!}
        {!! Form::text(
            'phone',
            null,
            [
                'class' => 'form-control form-control-solid placeholder-no-fix mask-phone-number',
            ]
        ) !!}
        <span>
            {{ $errors->first('phone') }}
        </span>
    </div>

    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        {!! Form::label(
            'cellular',
            'Celular *',
            ['class' => 'control-label']
        ) !!}
        {!! Form::text(
            'cellular',
            null,
            [
                'class' => 'form-control form-control-solid placeholder-no-fix mask-phone-number',
            ]
        ) !!}
        <span>
            {{ $errors->first('cellular') }}
        </span>
    </div>

    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        {!! Form::label(
            'plan_id',
            'Plano *',
            ['class' => 'control-label']
        ) !!}
        {!! Form::select(
            'plan_id',
            $planOptions,
            null,
            [
                'class' => 'form-control form-control-solid placeholder-no-fix',
            ]
        ) !!}
        <span>
            {{ $errors->first('plan_id') }}
        </span>
    </div>

    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        {!! Form::label(
            'email',
            'E-mail *',
            ['class' => 'control-label']
        ) !!}

        <?php
            $email = null;
            if ($preUser) {
                $email = $preUser->email;
            }
        ?>

        {!! Form::text(
            'email',
            $email,
            [
                'class' => 'form-control form-control-solid placeholder-no-fix',
            ]
        ) !!}
        <span>
            {{ $errors->first('email') }}
        </span>
    </div>

    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        {!! Form::label(
            'password',
            'Senha *',
            ['class' => 'control-label']
        ) !!}
        {!! Form::password(
            'password',
            [
                'class' => 'form-control form-control-solid placeholder-no-fix',
            ]
        ) !!}
        <span>
            {{ $errors->first('password') }}
        </span>
    </div>

    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        {!! Form::label(
            'password_confirmation',
            'Confirmar senha *',
            ['class' => 'control-label']
        ) !!}
        {!! Form::password(
            'password_confirmation',
            [
                'class' => 'form-control form-control-solid placeholder-no-fix',
            ]
        ) !!}
        <span>
            {{ $errors->first('password_confirmation') }}
        </span>

        <label class="check" style="display: block; margin-top: 5px;">
            <input type="checkbox" name="terms_use" />
            Declaro que li e aceito os <a data-toggle="modal" href="#modal-terms-use">termos de uso</a>
        </label>

        <span>
            {{ $errors->first('terms_use') }}
        </span>
    </div>

    <div class="form-actions">
        {!! Form::submit('Cadastrar', ['class' => 'btn btn-primary btn-block uppercase"']) !!}
    </div>

    {!! Form::close() !!}
</div>

@include('app.register.partial.modal-terms-use')

@endsection
