@extends('app.layout.clean.index')

@section('javascript')
    @parent

    {!! Html::script('assets/lib/jquery-jreject/js/jquery.reject.js') !!}
    {!! Html::script('assets/js/common/init-jreject.js') !!}
@endsection

@section('stylesheet')
    @parent

    {!! Html::style('assets/lib/jquery-jreject/css/jquery.reject.css') !!}
@endsection

@section('content')
        <!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGO -->
<div class="logo">
    <a href="{{ route('institutional.home') }}" class="no-text-decoration">
        <h1 class="no-text-decoration">New Markett</h1>
    </a>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">

    {!! Form::open(['class' => 'login-form', 'autocomplete' => 'off']) !!}

    @include('app.common.flash-message')

    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        {!! Form::label(
            'id',
            'Meu ID',
            ['class' => 'control-label visible-ie8 visible-ie9']
        ) !!}
        {!! Form::text(
            'id',
            null,
            [
                'class' => 'form-control form-control-solid placeholder-no-fix',
                'placeholder' => 'Meu ID'
            ]
        ) !!}
        <span>
            {{ $errors->first('id') }}
        </span>
    </div>

    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        {!! Form::label(
            'password',
            'Senha',
            ['class' => 'control-label visible-ie8 visible-ie9']
        ) !!}
        {!! Form::password(
            'password',
            [
                'class' => 'form-control form-control-solid placeholder-no-fix',
                'placeholder' => 'Senha'
            ]
        ) !!}
        <span>
            {{ $errors->first('password') }}
        </span>
    </div>

    <div class="form-group">
        <a href="{{ route('app.auth.forgot-my-password') }}">Esqueci minha senha</a>
    </div>

    <div class="form-actions">
        {!! Form::submit('Entrar', ['class' => 'btn btn-primary btn-block uppercase"']) !!}
    </div>

    {!! Form::close() !!}
</div>

@endsection
