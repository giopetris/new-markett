@extends('app.layout.clean.index')

@section('content')
        <!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGO -->
<div class="logo">
        <h1 class="no-text-decoration" style="color: white;">E-mail enviado</h1>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">

    @include('app.common.flash-message')

    <p class="hint">
        Enviamos um e-mail para {{ $email }} com as instruções e o link para você trocar a senha. <br /><br />
        Caso você não receba o e-mail em alguns minutos verifique sua caixa de spam ou
        envie-nos seu e-mail novamente.
    </p>

    <a href="{{ route('institutional.home') }}" class="btn btn-primary btn-block uppercase" style="margin-top: 50px;">
        Ok, voltar para a tela inicial
    </a>
    
</div>

@endsection
