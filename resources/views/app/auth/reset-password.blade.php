@extends('app.layout.clean.index')

@section('content')
        <!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGO -->
<div class="logo">
    <a href="{{ route('institutional.home') }}" class="no-text-decoration">
        <h1 class="no-text-decoration">New Markett - Trocar senha</h1>
    </a>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">

    {!! Form::open(['class' => 'login-form']) !!}

    @include('app.common.flash-message')

    <p class="hint">Olá, {{ $user->name }}! Crie uma nova senha para a sua conta.</p>

    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        {!! Form::label(
            'password',
            'Senha',
            ['class' => 'control-label visible-ie8 visible-ie9']
        ) !!}
        {!! Form::password(
            'password',
            [
                'class' => 'form-control form-control-solid placeholder-no-fix',
                'placeholder' => 'Senha'
            ]
        ) !!}
        <span>
            {{ $errors->first('password') }}
        </span>
    </div>

    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        {!! Form::label(
            'password_confirmation',
            'Confirmar senha',
            ['class' => 'control-label visible-ie8 visible-ie9']
        ) !!}
        {!! Form::password(
            'password_confirmation',
            [
                'class' => 'form-control form-control-solid placeholder-no-fix',
                'placeholder' => 'Confirmar senha'
            ]
        ) !!}
        <span>
            {{ $errors->first('password_confirmation') }}
        </span>
    </div>

    <div class="form-actions">
        {!! Form::submit('Salvar', ['class' => 'btn btn-primary btn-block uppercase"']) !!}
    </div>

    {!! Form::close() !!}
</div>

@endsection
