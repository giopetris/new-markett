@extends('app.layout.clean.index')

@section('content')
        <!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGO -->
<div class="logo">
    <a href="{{ route('institutional.home') }}" class="no-text-decoration">
        <h1 class="no-text-decoration">New Markett</h1>
    </a>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">

    {!! Form::open(['class' => 'login-form']) !!}

    @include('app.common.flash-message')

    <p class="hint">Informe seu ID para receber um e-mail com as instruções e o link para criar uma nova senha.</p>

    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        {!! Form::label(
            'id',
            'Meu ID',
            ['class' => 'control-label visible-ie8 visible-ie9']
        ) !!}
        {!! Form::text(
            'id',
            null,
            [
                'class' => 'form-control form-control-solid placeholder-no-fix',
                'placeholder' => 'Meu ID'
            ]
        ) !!}
        <span>
            {{ $errors->first('id') }}
        </span>
    </div>

    <div class="form-actions">
        {!! Form::submit('Continuar', ['class' => 'btn btn-primary btn-block uppercase"']) !!}
    </div>

    {!! Form::close() !!}
</div>

@endsection
