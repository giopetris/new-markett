<?php
$menuAtivo = 'loot';
?>

@extends('app.layout.base.index')

@section('javascript')
    @parent

    {!! Html::script('assets/js/app/loot/handleHolderFields.js') !!}
@endsection

@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1>Saque</h1>
                </div>
            </div>
        </div>

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        @include('app.common.flash-message')
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-green-sharp bold uppercase">
                                        Posição do dia (resumo)
                                    </span>
                                </div>
                            </div>

                            <div class="portlet-body">
                                <p>(+) Total recebido
                                    <span class="pull-right">
                                        {{ $financialPosition->present()->getTotalEntriesPaid() }}
                                    </span>
                                </p>
                                <hr>

                                <p>(-) Retiradas realizadas
                                    <span class="pull-right">
                                        {{ $financialPosition->present()->getTotalOutputsPaidOrWaitingPayment() }}
                                    </span>
                                </p>
                                <hr>

                                <p>(=) Saldo disponível para saque
                                    <span class="pull-right">
                                        {{ $financialPosition->present()->getTotalBalanceTransactionPaidOrWaitingPayment() }}
                                    </span>
                                </p>
                            </div>
                        </div>
                        <!-- END SAMPLE FORM PORTLET-->
                    </div>

                    <div class="col-md-6">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-green-sharp bold uppercase">
                                        Solicitação de saque
                                    </span>
                                </div>
                            </div>

                            <div class="portlet-body">
                                {!! Form::open(['class' => 'form-horizontal', 'route' => 'app.loot.loot-request']) !!}

                                    <div class="form-group">
                                        {!! Form::label('value_mask', 'Valor', ['class' => 'col-md-2 control-label']) !!}
                                        <div class="col-md-10">
                                            {!! Form::text(
                                                'value_mask',
                                                null,
                                                ['class' => 'form-control mask-money']
                                            ) !!}
                                            <span class="text-danger">
                                                {{ $errors->first('value') }}
                                            </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('password', 'Senha', ['class' => 'col-md-2 control-label']) !!}
                                        <div class="col-md-10">
                                            {!! Form::password(
                                                'password',
                                                ['class' => 'form-control']
                                            ) !!}
                                            <span class="text-danger">
                                                {{ $errors->first('password') }}
                                            </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-offset-2 col-md-10">
                                            {!! Form::submit('Confirmar', ['class' => 'btn blue']) !!}
                                        </div>
                                    </div>

                                {!! Form::close() !!}
                            </div>
                        </div>
                        <!-- END SAMPLE FORM PORTLET-->
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-green-sharp bold uppercase">
                                        Dados bancários
                                    </span>
                                </div>
                            </div>

                            <div class="portlet-body">

                                <div class="row">
                                    <div class="col-md-6">
                                        {!! Form::model($bankData, ['class' => 'form-horizontal', 'route' => 'app.loot.bank-data']) !!}

                                            <div class="form-group">
                                                {!! Form::label('bank_id', 'Banco', ['class' => 'col-md-3 control-label']) !!}
                                                <div class="col-md-9">
                                                    {!! Form::select(
                                                        'bank_id',
                                                        $banks,
                                                        null,
                                                        ['class' => 'form-control']
                                                    ) !!}
                                                    <span class="text-danger">
                                                        {{ $errors->first('bank_id') }}
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                {!! Form::label('account_type_id', 'Tipo de conta', ['class' => 'col-md-3 control-label']) !!}
                                                <div class="col-md-9">

                                                    <div class="radio-list">
                                                        <label class="radio-inline">
                                                            {!! Form::radio(
                                                            'account_type_id',
                                                            \NewMarkett\PickLists\Bank\AccountTypeNamePickList::CHECKING_ACCOUNT
                                                        ) !!}
                                                            {{ \NewMarkett\PickLists\Bank\AccountTypeNamePickList::get(
                                                                \NewMarkett\PickLists\Bank\AccountTypeNamePickList::CHECKING_ACCOUNT
                                                            ) }}
                                                        </label>

                                                        <label class="radio-inline">
                                                            {!! Form::radio(
                                                            'account_type_id',
                                                            \NewMarkett\PickLists\Bank\AccountTypeNamePickList::SAVINGS_ACCOUNT
                                                        ) !!}
                                                            {{ \NewMarkett\PickLists\Bank\AccountTypeNamePickList::get(
                                                                \NewMarkett\PickLists\Bank\AccountTypeNamePickList::SAVINGS_ACCOUNT
                                                            ) }}
                                                        </label>
                                                    </div>

                                                    <span class="text-danger">
                                                        {{ $errors->first('account_type_id') }}
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                {!! Form::label('agency', 'Agência', ['class' => 'col-md-3 control-label']) !!}
                                                <div class="col-md-9">
                                                    {!! Form::text(
                                                        'agency',
                                                        null,
                                                        ['class' => 'form-control']
                                                    ) !!}
                                                    <span class="text-danger">
                                                        {{ $errors->first('agency') }}
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                {!! Form::label('account_number', 'Conta', ['class' => 'col-md-3 control-label']) !!}
                                                <div class="col-md-9">
                                                    {!! Form::text(
                                                        'account_number',
                                                        null,
                                                        ['class' => 'form-control']
                                                    ) !!}
                                                    <span class="text-danger">
                                                        {{ $errors->first('account_number') }}
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-9 col-md-offset-3">
                                                    <label>
                                                        {!! Form::checkbox('not_account_holder', 1, null, ['id' => 'checkbox-not-account-holder']) !!}
                                                        Não sou o titular da conta
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="form-group show-account-holder">
                                                {!! Form::label('holder_name', 'Nome do titular', ['class' => 'col-md-3 control-label']) !!}
                                                <div class="col-md-9">
                                                    {!! Form::text(
                                                        'holder_name',
                                                        null,
                                                        ['class' => 'form-control']
                                                    ) !!}
                                                    <span class="text-danger">
                                                        {{ $errors->first('holder_name') }}
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="form-group show-account-holder">
                                                {!! Form::label('holder_cpf', 'CPF do titular', ['class' => 'col-md-3 control-label']) !!}
                                                <div class="col-md-9">
                                                    {!! Form::text(
                                                        'holder_cpf',
                                                        null,
                                                        ['class' => 'form-control mask-cpf']
                                                    ) !!}
                                                    <span class="text-danger">
                                                        {{ $errors->first('holder_cpf') }}
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-offset-3 col-md-9">
                                                    {!! Form::submit('Atualizar', ['class' => 'btn blue']) !!}
                                                </div>
                                            </div>

                                            <p style="margin-top: 5px;">
                                                Importante: O preenchimento errado dos campos acima ocasionará atraso
                                                do valor solicitado.
                                            </p>

                                        {!! Form::close() !!}
                                    </div>

                                    <div class="col-md-6">
                                        <div style="border: solid 1px #CACACA; padding: 20px 30px;">
                                            <p>Observações</p>

                                            <p>
                                                1. O valor mínimo para saque é de R$ 100,00.
                                                <br>
                                                2. O pagamento das bonificações é realizado exclusivamente na Conta
                                                Corrente ou Poupança relacionada ao CPF deste cadastro.
                                                <br>
                                                3. Taxa de transferência: R$ 8,00 (a ser debitada do valor solicitado).
                                                <br>
                                                4. Alguns bancos não permitem transferência para contas poupança.
                                                Consulte seu banco antes de solicitar um saque.
                                            </p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- END SAMPLE FORM PORTLET-->
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-green-sharp bold uppercase">
                                        Histórico de solicitações
                                    </span>
                                </div>
                            </div>

                            <div class="portlet-body">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Data</th>
                                            <th>Descrição</th>
                                            <th>Valor</th>
                                            <th>Status</th>
                                            <th>Pago em</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach($lootRequests as $lootRequest)
                                            <tr>
                                                <td>{{ $lootRequest->present()->createdAt }}</td>
                                                <td>Solicitação de saque</td>
                                                <td>{{ $lootRequest->present()->value }}</td>
                                                <td>{{ $lootRequest->transactionStatus->name }}</td>
                                                <td>{{ $lootRequest->present()->paidAt ? $lootRequest->present()->paidAt : '-' }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
