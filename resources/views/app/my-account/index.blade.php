@extends('app.layout.base.index')

@section('javascript')
    @parent

    {!! Html::script('assets/lib/artesaos/cidades/js/scripts.js') !!}

    <script type="text/javascript">
        $(function() {
            $('#uf').ufs({
                onChange: function(uf){
                    $('#cidade').cidades({uf: uf});
                }
            });
        });
    </script>
@endsection

@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1>Minha conta</h1>
                </div>
            </div>
        </div>

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        @include('app.common.flash-message')
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-green-sharp bold uppercase">
                                        Meus dados
                                    </span>
                                </div>
                            </div>

                            <div class="portlet-body">
                                {!! Form::model($user, ['class' => 'form-horizontal']) !!}

                                <?php $plan = $user->plan ?>
                                <div class="form-group">
                                    {!! Form::label('plan', 'Meu plano', ['class' => 'col-md-2 control-label']) !!}
                                    <div class="col-md-10">
                                        {!! Form::text(
                                            'plan',
                                            $plan->name . ' (' . $plan->present()->percentageComissionGlobalValue() . ')',
                                            ['class' => 'form-control', 'disabled' => 'disabled']
                                        ) !!}
                                        <span class="text-danger">
                                            {{ $errors->first('sponsor[name]') }}
                                        </span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('sponsor[name]', 'Meu patrocinador', ['class' => 'col-md-2 control-label']) !!}
                                    <div class="col-md-10">
                                        {!! Form::text(
                                            'sponsor[name]',
                                            null,
                                            ['class' => 'form-control', 'disabled' => 'disabled']
                                        ) !!}
                                        <span class="text-danger">
                                            {{ $errors->first('sponsor[name]') }}
                                        </span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('ids', 'Meu ID', ['class' => 'col-md-2 control-label']) !!}
                                    <div class="col-md-10">
                                        {!! Form::text(
                                            'id',
                                            null,
                                            ['class' => 'form-control', 'disabled' => 'disabled']
                                        ) !!}
                                        <span class="text-danger">
                                            {{ $errors->first('id') }}
                                        </span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('name', 'Nome *', ['class' => 'col-md-2 control-label']) !!}
                                    <div class="col-md-10">
                                        {!! Form::text('name', null, ['class' => 'form-control']) !!}
                                        <span class="text-danger">
                                            {{ $errors->first('name') }}
                                        </span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('cpf', 'CPF *', ['class' => 'col-md-2 control-label']) !!}
                                    <div class="col-md-10">
                                        <?php
                                            $cpfFieldOptions = ['class' => 'form-control mask-cpf'];

                                            if ( ! $userCanChangePassword) {
                                                $cpfFieldOptions['readonly'] = 'readonly';
                                            }
                                        ?>

                                        {!! Form::text(
                                            'cpf',
                                            $user->present()->cpf,
                                            $cpfFieldOptions
                                        ) !!}
                                        <span class="text-danger">
                                            {{ $errors->first('cpf') }}
                                        </span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('email', 'E-mail *', ['class' => 'col-md-2 control-label']) !!}
                                    <div class="col-md-10">
                                        {!! Form::text('email', null, ['class' => 'form-control']) !!}
                                        <span class="text-danger">
                                            {{ $errors->first('email') }}
                                        </span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-offset-2 col-md-10">
                                        <a href="{{ route('app.my-account.change-password') }}">Alterar senha</a>
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('phone', 'Telefone', ['class' => 'col-md-2 control-label']) !!}
                                    <div class="col-md-10">
                                        {!! Form::text(
                                            'phone',
                                            $user->present()->phone,
                                            ['class' => 'form-control mask-phone-number']
                                        ) !!}
                                        <span class="text-danger">
                                            {{ $errors->first('phone') }}
                                        </span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('cellular', 'Celular *', ['class' => 'col-md-2 control-label']) !!}
                                    <div class="col-md-10">
                                        {!! Form::text(
                                            'cellular',
                                            $user->present()->cellular,
                                            ['class' => 'form-control mask-phone-number']
                                        ) !!}
                                        <span class="text-danger">
                                            {{ $errors->first('cellular') }}
                                        </span>
                                    </div>
                                </div>

                                <h4>Meu endereço</h4>
                                <hr>

                                <div class="form-group">
                                    {!! Form::label('address[cep]', 'CEP *', ['class' => 'col-md-2 control-label']) !!}
                                    <div class="col-md-10">
                                        {!! Form::text(
                                            'address[cep]',
                                            null,
                                            ['class' => 'form-control mask-zip-code']
                                        ) !!}
                                        <span class="text-danger">
                                            {{ $errors->first('address.cep') }}
                                        </span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('address[street]', 'Rua *', ['class' => 'col-md-2 control-label']) !!}
                                    <div class="col-md-10">
                                        {!! Form::text(
                                            'address[street]',
                                            null,
                                            ['class' => 'form-control']
                                        ) !!}
                                        <span class="text-danger">
                                            {{ $errors->first('address.street') }}
                                        </span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('address[number]', 'Número *', ['class' => 'col-md-2 control-label']) !!}
                                    <div class="col-md-10">
                                        {!! Form::text(
                                            'address[number]',
                                            null,
                                            ['class' => 'form-control']
                                        ) !!}
                                        <span class="text-danger">
                                            {{ $errors->first('address.number') }}
                                        </span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('address[district]', 'Bairro *', ['class' => 'col-md-2 control-label']) !!}
                                    <div class="col-md-10">
                                        {!! Form::text(
                                            'address[district]',
                                            null,
                                            ['class' => 'form-control']
                                        ) !!}
                                        <span class="text-danger">
                                            {{ $errors->first('address.district') }}
                                        </span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('address[complemet]', 'Complemento', ['class' => 'col-md-2 control-label']) !!}
                                    <div class="col-md-10">
                                        {!! Form::text(
                                            'address[complement]',
                                            null,
                                            ['class' => 'form-control']
                                        ) !!}
                                        <span class="text-danger">
                                            {{ $errors->first('address.complement') }}
                                        </span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('address[state]', 'Estado *', ['class' => 'col-md-2 control-label']) !!}
                                    <div class="col-md-10">
                                        <?php
                                            $stateDefaultValue = old('address.state');
                                            if ($stateDefaultValue === null && $user->address && $user->address->city) {
                                                $stateDefaultValue = $user->address->city->uf;
                                            }
                                        ?>
                                        {!! Form::select(
                                            'address[state]',
                                            [],
                                            null,
                                            [
                                                'id' => 'uf',
                                                'default' => $stateDefaultValue,
                                                'class' => 'form-control'
                                            ]
                                        ) !!}
                                        <span class="text-danger">
                                            {{ $errors->first('address.state') }}
                                        </span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('address[city_id]', 'Cidade *', ['class' => 'col-md-2 control-label']) !!}
                                    <div class="col-md-10">
                                        <?php
                                            $cityDefaultValue = old('address.city_id');
                                            if ($cityDefaultValue === null && $user->address && $user->address->city) {
                                                $cityDefaultValue = $user->address->city->id;
                                            }
                                        ?>
                                        {!! Form::select(
                                            'address[city_id]',
                                            [],
                                            null,
                                            [
                                                'id' => 'cidade',
                                                'default' => $cityDefaultValue,
                                                'class' => 'form-control'
                                            ]
                                        ) !!}
                                        <span class="text-danger">
                                            {{ $errors->first('address.city_id') }}
                                        </span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-offset-2 col-md-10">
                                        {!! Form::submit('Salvar', ['class' => 'btn blue']) !!}
                                    </div>
                                </div>

                                {!! Form::close() !!}
                            </div>
                        </div>
                        <!-- END SAMPLE FORM PORTLET-->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
