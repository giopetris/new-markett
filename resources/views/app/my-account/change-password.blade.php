@extends('app.layout.base.index')

@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1>Alterar senha</h1>
                </div>
            </div>
        </div>

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        @include('app.common.flash-message')
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-body">
                                {!! Form::open(['class' => 'form-horizontal']) !!}

                                <div class="form-group">
                                    {!! Form::label('current_password', 'Senha', ['class' => 'col-md-2 control-label']) !!}
                                    <div class="col-md-10">
                                        {!! Form::password('current_password', ['class' => 'form-control']) !!}
                                        <span class="text-danger">
                                            {{ $errors->first('current_password') }}
                                        </span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('new_password', 'Nova senha', ['class' => 'col-md-2 control-label']) !!}
                                    <div class="col-md-10">
                                        {!! Form::password('new_password', ['class' => 'form-control']) !!}
                                        <span class="text-danger">
                                            {{ $errors->first('new_password') }}
                                        </span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!! Form::label('new_password_confirmation', 'Confirmar senha', ['class' => 'col-md-2 control-label']) !!}
                                    <div class="col-md-10">
                                        {!! Form::password('new_password_confirmation', ['class' => 'form-control']) !!}
                                        <span class="text-danger">
                                            {{ $errors->first('new_password_confirmation') }}
                                        </span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-offset-2 col-md-10">
                                        {!! Form::submit('Salvar', ['class' => 'btn blue']) !!}
                                    </div>
                                </div>

                                {!! Form::close() !!}
                            </div>
                        </div>
                        <!-- END SAMPLE FORM PORTLET-->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
