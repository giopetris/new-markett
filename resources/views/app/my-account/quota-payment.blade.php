<?php
use NewMarkett\PickLists\Financial\BankDataToDepositPickList;
use NewMarkett\PickLists\User\UserStatusNamePickList;
?>

@extends('app.layout.base.index')

@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1>Minha conta</h1>
                </div>
            </div>
        </div>

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        @include('app.common.flash-message')
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-green-sharp bold uppercase">
                                        Pagamento da cota
                                    </span>
                                </div>
                            </div>

                            <div class="portlet-body" style="text-align: center;">
                                @if($user->isInactive())
                                    <h1>Seu cadastro está inativo.</h1>
                                    <br />

                                    <h3>Provavelmente seu pagamento não foi aprovado.</h3>

                                    <h4>
                                        Por favor, contate-nos através do e-mail contato@newmarkett.com.br ou pelos
                                        telefones (47) 9269-7632 | (47) 8439-3656.
                                    </h4>
                                @endif

                                @if( ! $user->planAccessionIsPaid() && ! $user->isInactive())
                                    <h1>Parabéns, agora você já faz parte da rede New Markett!</h1>
                                    <br />

                                    <h3>
                                        O número do seu ID é <strong>{{ $user->id }}</strong>
                                        <br />
                                        Grave esta informação pois você irá acessar o sistema a partir do seu ID.
                                    </h3>
                                @endif

                                @if($user->planIsExpired() && ! $user->isInactive())
                                    <h1>Seu plano expirou, siga as instruções abaixo para reativar seu cadastro!</h1>
                                    <br />
                                @endif

                                @if( ! $user->isInactive())
                                    <br />
                                    <h4>Para ativar seu cadastro e usufruir de todos os nossos benefícios faça um depósito identificado informando o <br>
                                        seu cpf de cadastro (<strong>{{ $user->present()->cpf }}</strong>) no valor de
                                        <strong>{{ $user->plan->present()->value }}</strong> para a conta bancária:
                                    </h4>

                                    <br>

                                    <h4>
                                        Banco: {{ BankDataToDepositPickList::get(BankDataToDepositPickList::BANK) }} <br>
                                        Agência: {{ BankDataToDepositPickList::get(BankDataToDepositPickList::AGENCY) }} <br>
                                        Conta: {{ BankDataToDepositPickList::get(BankDataToDepositPickList::ACCOUNT_NUMBER) }} <br>
                                        Operação: {{ BankDataToDepositPickList::get(BankDataToDepositPickList::OPERATION) }}
                                        ({{ BankDataToDepositPickList::get(BankDataToDepositPickList::ACCOUNT_TYPE) }}) <br>
                                        Nome do Favorecido: {{ BankDataToDepositPickList::get(BankDataToDepositPickList::PAYEE_NAME) }} <br>
                                        CPF: {{ BankDataToDepositPickList::get(BankDataToDepositPickList::PAYEE_CPF) }}
                                    </h4>

                                    <br />

                                    <h4>
                                        Depois de feito o depósito você poderá enviar o comprovante para <br>
                                        financeiro@newmarkett.com.br para agilizar o processo de ativação da sua conta.
                                    </h4>
                                @endif

                                @if(isset($userIsFullRegistration) && ! $userIsFullRegistration)
                                    <br>
                                    <hr />
                                    <h4>
                                        Você também deve completar o seu cadastro na área de
                                        <a href="{{ route('app.my-account') }}">Minha Conta</a>
                                    </h4>
                                @endif

                                <br>
                                <hr />

                                <p>
                                    Observação: Caso você já tenha feito o depósito desconsidere essa mensagem que
                                    em breve seu cadastro já estará ativado.
                                </p>
                            </div>
                        </div>
                        <!-- END SAMPLE FORM PORTLET-->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
