@extends('app.layout.base.index')

@section('content')
    <div class="page-container">
        <div class="page-head">
            <div class="container">
                <div class="page-title">
                    <h1>Minha conta</h1>
                </div>
            </div>
        </div>

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        @include('app.common.flash-message')
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-green-sharp bold uppercase">
                                        Pagamento da taxa mensal
                                    </span>
                                </div>
                            </div>

                            <div class="portlet-body" style="text-align: center;">
                                    <h1>Você está inadimplente com a taxa mensal.</h1>
                                    <br />

                                    <h4>
                                        Verifique o e-mail que enviamos para fazer o pagamento através de boleto bancário.
                                    </h4>
                                    <br />
                                    <h4>
                                        Enquanto seu cadastro estiver inadimplente você estará impossibilitado de fazer
                                        o saque, sua conta não participará da divisão global e não será contabilizada a
                                        comissão por indicação direta.
                                    </h4>
                                    <br />
                                    <h4>
                                        Qualquer dúvida, contate-nos através do e-mail contato@newmarkett.com.br ou pelos
                                        telefones (47) 9269-7632 | (47) 8439-3656.
                                    </h4>
                                <br>
                                <hr />

                                <p>
                                    Observação: Caso você já tenha feito o pagamento desconsidere essa mensagem que
                                    em breve seu cadastro já estará ativado.
                                </p>
                            </div>
                        </div>
                        <!-- END SAMPLE FORM PORTLET-->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
