@extends('institutional.layout.base.index')

@section('content')

<!-- Banner -->
<section id="banner" style="padding: 0;">

    <!--
        ".inner" is set up as an inline-block so it automatically expands
        in both directions to fit whatever's inside it. This means it won't
        automatically wrap lines, so be sure to use line breaks where
        appropriate (<br />).
    -->
    <div class="inner" style="background: none;">

        <header>
        </header>

    </div>
</section>

<!-- Main -->
<article id="main" style="background-image: none;">
    <!-- Two -->
    <section class="wrapper style1 container special">

        @if ($classifieds->count() == 0)
            Nenhum classificado cadastrado
        @endif

        <?php
            $columnCount = 1;
        ?>
        @foreach ($classifieds as $classified)

            @if ($columnCount == 1)
                <div class="row">
            @endif

                <div class="4u 12u(narrower)">

                    <section>
                        <a href="{{ route('institutional.new-classified.view', ['id' => $classified->id]) }}" class="image">
                            {!! Html::image(
                                $classified->getFullLogoPath(),
                                'Logo',
                                ['height' => '190px', 'width' => '375px']
                            ) !!}
                            <img src="" alt="" >
                        </a>
                        <p>
                            <a href="{{ route('institutional.new-classified.view', ['id' => $classified->id]) }}"
                               style="color: rgb(124, 128, 129); border-bottom: none;">
                                <strong>{{ strtoupper($classified->title) }}</strong>
                            </a>
                            <br />
                            {{ ucfirst(strtolower($classified->address->city->nome)) . ' - ' . $classified->address->city->uf }}
                            <br />
                            {{ $classified->present()->phones }}
                        </p>
                    </section>

                </div>

            @if ($columnCount == 3)
                </div>
            @endif

            <?php
                $columnCount++;
                if ($columnCount == 4) {
                    $columnCount = 1;
                }
            ?>

        @endforeach

        @if ($columnCount != 1)
            </div>
        @endif

    </section>
</article>

@endsection
