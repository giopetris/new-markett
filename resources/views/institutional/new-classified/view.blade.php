@extends('institutional.layout.base.index')

@section('content')

<!-- Banner -->
<section id="banner" style="padding: 0;">

    <!--
        ".inner" is set up as an inline-block so it automatically expands
        in both directions to fit whatever's inside it. This means it won't
        automatically wrap lines, so be sure to use line breaks where
        appropriate (<br />).
    -->
    <div class="inner" style="background: none;">

        <header>
        </header>

    </div>
</section>

<!-- Main -->
<article id="main" style="background-image: none;">

    <section class="wrapper style4 container">

        <div class="row 150%">
            <div class="5u 12u(narrower)">

                <!-- Sidebar -->
                <div class="sidebar">
                    {!! Html::image(
                        $classified->getFullLogoPath(),
                        'Logo',
                        ['height' => '190px', 'width' => '375px']
                    ) !!}

                </div>

                @if($classified->link_facebook)
                    <div style="margin-top: 10px;">
                        <a href="{{ $classified->link_facebook }}" class="icon circle fa-facebook" target="_blank"><span class="label">Facebook</span></a>
                    </div>
                @endif

            </div>
            <div class="7u 12u(narrower) important(narrower)" style="padding-left: 30px; line-height: 24px;">

                <!-- Content -->
                <div class="content">
                    <section>
                        <header>
                            <h3>
                                <strong>{{ strtoupper($classified->title) }}</strong>
                            </h3>
                        </header>

                        <p style="font-size: 16px;">
                            <strong>CEP: </strong> {{ $classified->address->present()->cep }} <br />
                            <strong>Rua: </strong> {{ $classified->address->street }} <br />
                            @if($classified->address->number)
                                <strong>Número: </strong> {{ $classified->address->number }} <br />
                            @endif
                            @if($classified->address->district)
                                <strong>Bairro: </strong> {{ $classified->address->district }} <br />
                            @endif
                            @if($classified->address->complement)
                                <strong>Complemento: </strong> {{ $classified->address->complement }} <br />
                            @endif
                            <strong>Cidade: </strong> {{ ucfirst(strtolower($classified->address->city->nome)) . ' - ' . $classified->address->city->uf }} <br />
                        </p>

                        <p style="font-size: 16px;">
                            <strong>Telefone: </strong> {{ $classified->present()->phone }} <br />
                            @if($classified->cellular)
                                <strong>Telefone: </strong> {{ $classified->present()->cellular }} <br />
                            @endif
                            @if($classified->site)
                                <strong>Site: </strong>
                                <a href="{{ $classified->site }}" target="_blank">{{ $classified->site }}</a>
                                <br />
                            @endif
                            @if($classified->email)
                                <strong>E-mail: </strong> {{ $classified->email }} <br />
                            @endif
                        </p>
                    </section>
                </div>

            </div>
        </div>
    </section>

</article>

@endsection
