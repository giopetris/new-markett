<!DOCTYPE HTML>
<html>
<head>
    <title>New Markett</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!--[if lte IE 8]>{!! Html::script('assets/theme/twenty/js/ie/html5shiv.js') !!}<![endif]-->
    {!! Html::style('assets/theme/twenty/css/main.css') !!}
    <!--[if lte IE 8]>{!! Html::script('assets/theme/twenty/css/ie8.css') !!}<![endif]-->
    <!--[if lte IE 9]>{!! Html::script('assets/theme/twenty/css/ie9.css') !!}<![endif]-->
</head>
<body class="index">
<div id="page-wrapper">

    <!-- Header -->
    <header id="header" class="alt">
        <h1 id="logo">
            <a href="{{ route('institutional.home') }}">
                <img src="{{ asset('assets/img/logo.png') }}" alt="" style="width: 190px; margin-top: -9px;">
            </a>
        </h1>
        <nav id="nav">
            <ul>
                <li><a href="{{ route('institutional.home') }}">Quem somos</a></li>
                <li><a href="{{ route('institutional.home') }}#main">Oportunidade</a></li>
                <li><a href="{{ route('institutional.home') }}#contact">Contato</a></li>
                <li><a href="{{ route('app.register') }}">Registre-se</a></li>
                <li><a href="{{ route('institutional.new-classified') }}">New classificados</a></li>
                <li><a href="{{ route('app.auth.login') }}" class="button special">Escritório virtual</a></li>
            </ul>
        </nav>
    </header>

    @yield('content')

    <!-- Footer -->
    <footer id="footer">

        <!--<ul class="icons">
            <li><a href="#" class="icon circle fa-twitter"><span class="label">Twitter</span></a></li>
            <li><a href="#" class="icon circle fa-facebook"><span class="label">Facebook</span></a></li>
            <li><a href="#" class="icon circle fa-google-plus"><span class="label">Google+</span></a></li>
            <li><a href="#" class="icon circle fa-github"><span class="label">Github</span></a></li>
            <li><a href="#" class="icon circle fa-dribbble"><span class="label">Dribbble</span></a></li>
        </ul>-->

        <div class="copyright">
            <p>
                <strong>Endereço: </strong> Rua das Missões, 2214 - Ponta Aguda, Blumenau, SC - CEP: 89051-001 <br> <br>

                contato@newmarkett.com.br <br><br>
                (47) 9269-7632 | (47) 8439-3656
            </p>
        </div>

    </footer>

</div>

<!-- Scripts -->
{!! Html::script('assets/theme/twenty/js/jquery.min.js') !!}
{!! Html::script('assets/theme/twenty/js/jquery.dropotron.min.js') !!}
{!! Html::script('assets/theme/twenty/js/jquery.scrolly.min.js') !!}
{!! Html::script('assets/theme/twenty/js/jquery.scrollgress.min.js') !!}
{!! Html::script('assets/theme/twenty/js/skel.min.js') !!}
{!! Html::script('assets/theme/twenty/js/util.js') !!}
<!--[if lte IE 8]>{!! Html::script('assets/theme/twenty/js/ie/respond.min.js') !!}<![endif]-->
{!! Html::script('assets/theme/twenty/js/main.js') !!}

</body>
</html>
