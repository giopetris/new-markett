@extends('institutional.layout.base.index')

@section('content')

<!-- Banner -->
<section id="banner">

    <!--
        ".inner" is set up as an inline-block so it automatically expands
        in both directions to fit whatever's inside it. This means it won't
        automatically wrap lines, so be sure to use line breaks where
        appropriate (<br />).
    -->
    <div class="inner">

        <header>
            <h2>QUEM SOMOS</h2>
        </header>

        <p>Somos uma empresa voltada para <br/> o crescimento e desenvolvimento do <br/> <strong>empreendedorismo</strong> e <strong>renda familiar</strong>!</p>

        <footer>
            <ul class="buttons vertical">
                <li><a href="#main" class="button fit scrolly">Conte-me mais</a></li>
            </ul>
        </footer>

    </div>

</section>

<!-- Main -->
<article id="main">

    <header class="special container">
        <span class="icon fa-bar-chart-o"></span>
        <h2><strong>OPORTUNIDADE</strong>
            <br />
            De negócio simples, prático e rentável.</h2>

        <p>
            Foi pensado em você que a empresa criou a melhor oportunidade <br/>
            para quem quer <strong>empreender</strong> e ter sua <strong>independência financeira</strong>.
        </p>
    </header>

    <!-- One -->
    <!--<section class="wrapper style2 container special-alt">
        <div class="row 50%">
            <div class="8u 12u(narrower)">

                <header>
                    <h2>Behold the <strong>icons</strong> that visualize what you’re all about. or just take up space. your call bro.</h2>
                </header>
                <p>Sed tristique purus vitae volutpat ultrices. Aliquam eu elit eget arcu comteger ut fermentum lorem. Lorem ipsum dolor sit amet. Sed tristique purus vitae volutpat ultrices. eu elit eget commodo. Sed tristique purus vitae volutpat ultrices. Aliquam eu elit eget arcu commodo.</p>
                <footer>
                    <ul class="buttons">
                        <li><a href="#" class="button">Find Out More</a></li>
                    </ul>
                </footer>

            </div>
            <div class="4u 12u(narrower) important(narrower)">

                <ul class="featured-icons">
                    <li><span class="icon fa-clock-o"><span class="label">Feature 1</span></span></li>
                    <li><span class="icon fa-volume-up"><span class="label">Feature 2</span></span></li>
                    <li><span class="icon fa-laptop"><span class="label">Feature 3</span></span></li>
                    <li><span class="icon fa-inbox"><span class="label">Feature 4</span></span></li>
                    <li><span class="icon fa-lock"><span class="label">Feature 5</span></span></li>
                    <li><span class="icon fa-cog"><span class="label">Feature 6</span></span></li>
                </ul>

            </div>
        </div>
    </section>-->

    <!-- Two -->
    <section class="wrapper style1 container special">
        <div class="row">
            <div class="4u 12u(narrower)">

                <section>
                    <span class="icon featured fa-check"></span>
                    <header>
                        <h3>New classificados</h3>
                    </header>
                    <p>
                        Divulgue seu negócio, sem pagar mensalidade!
                        Afiliando-se ao New Markett você terá direito à um <strong>espaço</strong> para
                        divulgar sua empresa ou qualquer que seja sua atividade.
                    </p>
                </section>

            </div>
            <div class="4u 12u(narrower)">

                <section>
                    <span class="icon featured fa-check"></span>
                    <header>
                        <h3>Parcerias na área da saúde</h3>
                    </header>
                    <p>
                        Ao adquirir o plano Ouro Rubi ou Diamante a New Markett lhe proporciona acesso a uma carteira
                        de parceiros criteriosamente selecionados da área de saúde, com descontos muito atrativos.
                        <br />
                        Consultas médicas a preços especiais entre outros benefícios na área de saúde.
                        <br />
                        Consulte-nos para mais informações.
                    </p>
                </section>

            </div>
            <div class="4u 12u(narrower)">

                <section>
                    <span class="icon featured fa-check"></span>
                    <header>
                        <h3>Bônus indicação direta</h3>
                    </header>
                    <p>Você recebe 10% do valor total que seu indicado adquirir.</p>
                </section>

            </div>
        </div>

        <hr/>
        <h2><strong>Tudo isso e muito mais!</strong></h2>
        <p>
            Clique no ícone abaixo e faça o download de <br/> nossa apresentação comercial para conhecer todas as vantagens.
        </p>
        <br/>
        <br/>
        <a href="{{ route('institutional.download-commercial-presentation') }}" style="border-bottom: none">
            <span class="icon fa-download" style="font-size: 95px"></span>
        </a>
    </section>

    <!-- Three -->
    <!-- <section class="wrapper style3 container special">

        <header class="major">
            <h2>Next look at this <strong>cool stuff</strong></h2>
        </header>

        <div class="row">
            <div class="6u 12u(narrower)">

                <section>
                    <a href="#" class="image featured"><img src="images/pic01.jpg" alt="" /></a>
                    <header>
                        <h3>A Really Fast Train</h3>
                    </header>
                    <p>Sed tristique purus vitae volutpat commodo suscipit amet sed nibh. Proin a ullamcorper sed blandit. Sed tristique purus vitae volutpat commodo suscipit ullamcorper sed blandit lorem ipsum dolore.</p>
                </section>

            </div>
            <div class="6u 12u(narrower)">

                <section>
                    <a href="#" class="image featured"><img src="images/pic02.jpg" alt="" /></a>
                    <header>
                        <h3>An Airport Terminal</h3>
                    </header>
                    <p>Sed tristique purus vitae volutpat commodo suscipit amet sed nibh. Proin a ullamcorper sed blandit. Sed tristique purus vitae volutpat commodo suscipit ullamcorper sed blandit lorem ipsum dolore.</p>
                </section>

            </div>
        </div>
        <div class="row">
            <div class="6u 12u(narrower)">

                <section>
                    <a href="#" class="image featured"><img src="images/pic03.jpg" alt="" /></a>
                    <header>
                        <h3>Hyperspace Travel</h3>
                    </header>
                    <p>Sed tristique purus vitae volutpat commodo suscipit amet sed nibh. Proin a ullamcorper sed blandit. Sed tristique purus vitae volutpat commodo suscipit ullamcorper sed blandit lorem ipsum dolore.</p>
                </section>

            </div>
            <div class="6u 12u(narrower)">

                <section>
                    <a href="#" class="image featured"><img src="images/pic04.jpg" alt="" /></a>
                    <header>
                        <h3>And Another Train</h3>
                    </header>
                    <p>Sed tristique purus vitae volutpat commodo suscipit amet sed nibh. Proin a ullamcorper sed blandit. Sed tristique purus vitae volutpat commodo suscipit ullamcorper sed blandit lorem ipsum dolore.</p>
                </section>

            </div>
        </div>

        <footer class="major">
            <ul class="buttons">
                <li><a href="#" class="button">See More</a></li>
            </ul>
        </footer>

    </section>-->

</article>

<!-- CTA -->
<section id="cta">

    <header>
        <h2>Pronto para conquistar a <br/> <strong>independência financeira</strong>?</h2>
    </header>
    <footer>
        <ul class="buttons">
            <li><a href="{{ route('app.register') }}" class="button">Sim, criar minha conta</a></li>
        </ul>
    </footer>

</section>

<!-- Main -->
<article id="contact">

    <header class="special container">
        <span class="icon fa-envelope"></span>
        <h2>Contato</h2>
        <p>Sinta-se à vontade para entrar em contato conosco pelo formulário abaixo <br/> ou pelos telefones (47) 9269-7632 | (47) 8439-3656</p>
    </header>

    @include('institutional.common.flash-message')

            <!-- One -->
    <section class="wrapper style4 special container 75%">

        <!-- Content -->
        <div class="content">
            {!! Form::open(['route' => 'institutional.contact']) !!}
                <div class="row 50%">
                    <div class="6u 12u(mobile)">
                        <input type="text" name="name" placeholder="Nome" />
                    </div>
                    <div class="6u 12u(mobile)">
                        <input type="text" name="email" placeholder="E-mail" />
                    </div>
                </div>
                <div class="row 50%">
                    <div class="12u">
                        <input type="text" name="subject" placeholder="Assunto" />
                    </div>
                </div>
                <div class="row 50%">
                    <div class="12u">
                        <textarea name="message" placeholder="Mensagem" rows="7"></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="12u">
                        <ul class="buttons">
                            <li><input type="submit" class="special" value="Enviar mensagem" /></li>
                        </ul>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>

    </section>

</article>
@endsection
