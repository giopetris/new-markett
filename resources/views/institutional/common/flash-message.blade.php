@foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))
        <div class="alert alert-{{ $msg }} container 75%" style="text-align: center; background-color: #83D3C9; font-size: 1em; color: #fff; font-weight: bold;">
            <span>
                {{ Session::get(sprintf('alert-%s', $msg)) }}
            </span>
        </div>
    @endif
@endforeach
